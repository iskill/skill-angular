/**
 * Declare SCSS files as modules so we can import them into TS files and use their content
 */
declare module '*.scss'
{
    const content: { [className: string]: string };
    export = content;
}
declare class OffscreenCanvas {
    constructor(width: number, height: number);
    getContext(contextId: '2d' | 'bitmaprenderer', options?: any): CanvasRenderingContext2D | ImageBitmapRenderingContext | null;
    // Add any additional methods or properties you might need
  }
  
