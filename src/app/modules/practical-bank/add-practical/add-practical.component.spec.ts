import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPracticalComponent } from './add-practical.component';

describe('AddPracticalComponent', () => {
  let component: AddPracticalComponent;
  let fixture: ComponentFixture<AddPracticalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddPracticalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPracticalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
