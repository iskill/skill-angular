import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PracticalBankComponent } from './practical-bank.component';

describe('PracticalBankComponent', () => {
  let component: PracticalBankComponent;
  let fixture: ComponentFixture<PracticalBankComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PracticalBankComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PracticalBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
