import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVivaComponent } from './add-viva.component';

describe('AddVivaComponent', () => {
  let component: AddVivaComponent;
  let fixture: ComponentFixture<AddVivaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddVivaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVivaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
