import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { JobRoleService } from 'app/modules/job-role/job-role.service';
import { IJobRole } from 'app/modules/job-role/job-role.types';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-add-viva',
  templateUrl: './add-viva.component.html',
  styleUrls: ['./add-viva.component.scss']
})
export class AddVivaComponent implements OnInit {

  isNew = true;
  practicalForm: FormGroup;
  jobRoles$: Observable<IJobRole[]>;

  constructor(
    private fb: FormBuilder,
    private _changeDetectorRef: ChangeDetectorRef,
    private jobRoleService: JobRoleService
  ) {
    this.practicalForm = fb.group({
      practicals: fb.array([this.newQuestions()])
    });
  }

  ngOnInit(): void {
    console.log('questions ==> ', this.questions());
    this.jobRoles$ = this.jobRoleService.jobRoles$;
    this.jobRoleService.getJobRole().subscribe();
  }

  // Add question
  questions(): FormArray {
    return this.practicalForm.get('practicals') as FormArray;
  }

  newQuestions(): FormGroup {
    return this.fb.group({
      question: ['', [Validators.required]],
      jobRole: ['', [Validators.required]],
      steps: [''],
    });
  }

  removeQuestion(i: number) {
    this.questions().removeAt(i);
  }

  addQuestion() {
    this.questions().push(this.newQuestions());
    this._changeDetectorRef.markForCheck();
  }
  // Add question and waitage end

  addUpdateViva() {
    console.log('addUpdateViva Clicked');
  }

}
