import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddPracticalComponent } from './add-practical/add-practical.component';
import { AddVivaComponent } from './add-viva/add-viva.component';
import { Route, RouterModule } from '@angular/router';
import { PracticalBankComponent } from './practical-bank.component';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatRippleModule } from '@angular/material/core';
import { MatSortModule } from '@angular/material/sort';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDividerModule } from '@angular/material/divider';
import { CKEditorModule } from 'ckeditor4-angular';

const practicalRoute: Route[] = [
  {
    path: '',
    component: PracticalBankComponent
  },
  {
    path: 'add-practical',
    component: AddPracticalComponent
  },
  {
    path: 'update-practical/:id',
    component: AddPracticalComponent
  },
  {
    path: 'add-viva',
    component: AddVivaComponent
  },
  {
    path: 'update-viva/:id',
    component: AddVivaComponent
  }
];

@NgModule({
  declarations: [
    PracticalBankComponent,
    AddPracticalComponent,
    AddVivaComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(practicalRoute),
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatRippleModule,
    MatSortModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatTableModule,
    MatTooltipModule,
    MatDividerModule,
    SharedModule,
    CKEditorModule
  ]
})
export class PracticalBankModule { }
