/* eslint-disable @typescript-eslint/naming-convention */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';
import { ApiResponse } from '../scheme/scheme.types';
import { IRoles, ISubAdmin } from './sub-admin.types';

@Injectable({
  providedIn: 'root'
})
export class SubAdminService {

  private _subAdmins: BehaviorSubject<ISubAdmin[] | null> = new BehaviorSubject(null);
  constructor(private _httpClient: HttpClient) { }

  get subAdmins$(): Observable<ISubAdmin[]> {
    return this._subAdmins.asObservable();
  }

  getRoles(): Observable<IRoles[]> {
    return this._httpClient.get<ApiResponse>(`${environment.apiBaseUrl}roles`).pipe(
      map((response) => {
        if (response.status === 200) {
          return response.data;
        }
      })
    );
  }

  getRolesById(id: string): Observable<IRoles[]> {
    return this._httpClient.get<ApiResponse>(`${environment.apiBaseUrl}sub-admins/${id}`).pipe(
      map((response) => {
        if (response.status === 200) {
          return response.data;
        }
      })
    );
  }

  getSubAdmins(): Observable<ISubAdmin[]> {
    return this._httpClient.get<ApiResponse>(`${environment.apiBaseUrl}sub-admins`).pipe(
      map((response) => {
        if (response.status === 200) {
          this._subAdmins.next(response.data);
          console.log(response.data);
          response.data.map((d: ISubAdmin) => {
            d.sub_admin_roles = Array.prototype.map.call(d.sub_admin_roles, item => item.role.name ).join(', ');
          });
          return response.data;
        }
      })
    );
  }

  getSubAdminById(id: string): Observable<ISubAdmin[]> {
    return this._httpClient.get<ApiResponse>(`${environment.apiBaseUrl}sub-admins/${id}`).pipe(
      map((response) => {
        if (response.status === 200) {
          this._subAdmins.next(response.data);
          return response.data;
        }
      })
    );
  }

  /**
   * Create SubAdmin
   */
  createSubAdmin(addSubAdmin): Observable<ISubAdmin> {
    const postData = {
      first_name: addSubAdmin.first_name,
      last_name: addSubAdmin.last_name,
      email: addSubAdmin.email,
      password: addSubAdmin.password,
      roles: addSubAdmin.role
    };
    return this.subAdmins$.pipe(
      take(1),
      // eslint-disable-next-line @typescript-eslint/naming-convention
      switchMap(SubAdmins => this._httpClient.post<ISubAdmin>(`${environment.apiBaseUrl}sub-admins`, postData).pipe(
          map((newJob) => {
              newJob['data']['sector'] = addSubAdmin['sector'];

              // Update the JobRoles with the new scheme
              this._subAdmins.next([newJob['data'], ...SubAdmins]);

              // Return the new newJob
              return newJob;
          })
      ))
    );
  }

  /**
   * Update SubAdmin
   */
  updateSubAdmin(id: string, subAdmin: ISubAdmin): Observable<ISubAdmin> {
    const updateData = {
      first_name: subAdmin.first_name,
      last_name: subAdmin.last_name,
      email: subAdmin.email,
      password: subAdmin.password,
      roles: subAdmin['role']
    };
    return this._httpClient.put<ISubAdmin>(`${environment.apiBaseUrl}sub-admins/${id}`, updateData);
  }

  /**
   * Delete the SubAdmin
   *
   * @param id
   */
  deleteSubAdmin(id: string): Observable<ApiResponse> {
    return this.subAdmins$.pipe(
      take(1),
      switchMap(jobRoles => this._httpClient.delete<ApiResponse>(`${environment.apiBaseUrl}sub-admins/${id}`).pipe(
        map((newJob) => {
          // Find the index of the deleted JobRole
          const index = jobRoles.findIndex(item => item.id === id);

          // Delete the sector
          jobRoles.splice(index, 1);

          // Update the sectors
          this._subAdmins.next(jobRoles);

          return newJob;
        })
      ))
    );
  }

}
