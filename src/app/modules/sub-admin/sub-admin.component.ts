/* eslint-disable @typescript-eslint/naming-convention */
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDailogComponent } from 'app/shared/component/confirmation-dailog/confirmation-dailog.component';
import { Observable, of, Subscription } from 'rxjs';
import { SubAdminService } from './sub-admin.service';
import { ISubAdmin } from './sub-admin.types';

@Component({
  selector: 'app-sub-admin',
  templateUrl: './sub-admin.component.html',
  styleUrls: ['./sub-admin.component.scss']
})
export class SubAdminComponent implements OnInit, OnDestroy {

  subAdmins$: Observable<ISubAdmin[]>;

  flashMessage: 'success' | 'error' | null = null;
  isLoading: boolean = false;
  subAdminTableColumns: string[] = ['name', 'email', 'role', 'actions'];
  selectedSubAdmin = null;
  subAdminCount: number = 0;
  count: number = 0;
  private _unsubscribeAll: Subscription;

  /**
   * Constructor
   */
  constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private _subAdminService: SubAdminService,
    private dialog: MatDialog
  ) { }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.subAdmins$ = this._subAdminService.subAdmins$;
    this.isLoading = true;
    this._unsubscribeAll = this._subAdminService
      .getSubAdmins()
      .subscribe((res) => {
        this.isLoading = false;
        // Update the counts
        this.subAdminCount = res.length;
        // Mark for check
        this._changeDetectorRef.markForCheck();
      });
  }

  // Open delete confirmation dialog
  openDialog(id) {
    const dialogRef = this.dialog.open(ConfirmationDailogComponent, {
      data: {
        message: 'Are you sure want to delete?',
        buttonText: {
          ok: 'Yes',
          cancel: 'No'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.deleteselectedSubAdmin(id);
        const a = document.createElement('a');
        a.click();
        a.remove();
      }
    });
  }
  // Open delete confirmation dialog end

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.unsubscribe();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Close the details
   */
  closeDetails(): void {
    this.selectedSubAdmin = null;
  }

  filterSearch(event) {
    const value = event.target.value.toLowerCase();
    this._subAdminService.subAdmins$.subscribe((res) => {
      this.subAdmins$ = of(res.filter(item =>
        (item.first_name + item.last_name)
          .toLocaleLowerCase()
          .includes(value)));
    });
  }

  /**
   * Delete the selected product using the form mock-api
   */
  deleteselectedSubAdmin(job): void {
    this._subAdminService.deleteSubAdmin(job.id).subscribe((res) => {
      this._changeDetectorRef.markForCheck();
    });
  }

  /**
   * Show flash message
   */
  showFlashMessage(type: 'success' | 'error'): void {
    // Show the message
    this.flashMessage = type;

    // Mark for check
    this._changeDetectorRef.markForCheck();

    // Hide it after 1.5 seconds
    setTimeout(() => {
      this.flashMessage = null;
      this.selectedSubAdmin = {};
      // Mark for check
      this._changeDetectorRef.markForCheck();
    }, 1500);
  }

  /**
   * Track by function for ngFor loops
   *
   * @param index
   * @param item
   */
  trackByFn(index: number, item: any): any {
    return item.id || index;
  }

}
