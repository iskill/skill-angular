/* eslint-disable @typescript-eslint/naming-convention */
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SubAdminService } from '../sub-admin.service';
import { IRoles } from '../sub-admin.types';

@Component({
  selector: 'app-add-sub-admin',
  templateUrl: './add-sub-admin.component.html',
  styleUrls: ['./add-sub-admin.component.scss']
})
export class AddSubAdminComponent implements OnInit {

  roles: IRoles[];
  id: string;
  selectedSubAdminForm: FormGroup;
  subAdminCount: number = 0;
  selectedSubAdmin = null;
  isLoading: boolean = false;
  isNew: boolean = true;

  constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _subAdminService: SubAdminService,
    private _activatedRoute: ActivatedRoute,
    private _route: Router
  ) { }

  ngOnInit(): void {
    this.selectedSubAdminForm = this._formBuilder.group({
      first_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      email: ['', [Validators.required,Validators.pattern('^[a-z0-9._%+-]+@[a-z.-]+.[a-z]{2,4}$')]],
      password: [''],
      confirmPassword: [''],
      role: ['', [Validators.required]],
    }, { validator: mustMatch('password', 'confirmPassword') });
    // Get Id from url if exist
    this.id = this._activatedRoute.snapshot.paramMap.get('id');
    if (this.id) {
      this.isNew = false;
      this._subAdminService.getSubAdminById(this.id).subscribe((response) => {
        const roles = [];
        response['sub_admin_roles'].map((role) => {
          roles.push(role.role.id);
        });
        this.selectedSubAdminForm.patchValue({
          first_name: response['first_name'],
          last_name: response['last_name'],
          email: response['email'],
          password: response['password'],
          confirmPassword: response['password'],
          role: roles,
        });
      });
    }
    this.isLoading = true;
    this._subAdminService.getRoles().subscribe((res) => {
      this.isLoading = false;
      this.roles = res;
    });
  }

  get f() {
    return this.selectedSubAdminForm.controls;
  }

  addUpdateselectedSubAdmin() {
    if (this.selectedSubAdminForm.valid) {
   

    if (this.isNew) {
        this._subAdminService
            .createSubAdmin(this.selectedSubAdminForm.value)
            .subscribe((res) => {
              this._changeDetectorRef.markForCheck();
              if (res) {
                this._route.navigate(['sub-admin']);
              }
            });
    } else {
      this._subAdminService
        .updateSubAdmin(
          this.id,
          this.selectedSubAdminForm.value
        )
        .subscribe((res) => {
          this._changeDetectorRef.markForCheck();
          if (res) {
            this._route.navigate(['sub-admin']);
          }
        });
    }
  }
}
}
// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
export function mustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
          // return if another validator has already found an error on the matchingControl
          return;
        }

        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
          matchingControl.setErrors({ mustMatch: true });
        } else {
          matchingControl.setErrors(null);
        }
    };
}
