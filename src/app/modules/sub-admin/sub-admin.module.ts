import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubAdminComponent } from './sub-admin.component';
import { RouterModule, Route } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatRippleModule } from '@angular/material/core';
import { MatSortModule } from '@angular/material/sort';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { AddSubAdminComponent } from './add-sub-admin/add-sub-admin.component';
import { MatDividerModule } from '@angular/material/divider';

const subAdminRoute: Route[] = [
  {
    path: '',
    component: SubAdminComponent
  },
  {
    path: 'add-sub-admin',
    component: AddSubAdminComponent
  },
  {
    path: 'update-sub-admin/:id',
    component: AddSubAdminComponent
  }
];

@NgModule({
  declarations: [
    SubAdminComponent,
    AddSubAdminComponent
  ],
  imports: [
    RouterModule.forChild(subAdminRoute),
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatRippleModule,
    MatSortModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatTableModule,
    MatTooltipModule,
    MatDividerModule,
    SharedModule
  ]
})
export class SubAdminModule { }
