/* eslint-disable @typescript-eslint/naming-convention */
export interface ISubAdmin{
    id: string;
    first_name: string;
    last_name: string;
    email: string;
    password: string;
    roles: [];
    sub_admin_roles: {
        role: {
            name: string;
        };
    };
}
export interface IRoles{
    id: string;
    role_id: string;
    name: string;
}
