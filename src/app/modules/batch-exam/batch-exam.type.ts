/* eslint-disable @typescript-eslint/naming-convention */
export interface IBatchExam {
    id: string;
    sector_id: string;
    scheme_id: string;
    job_role_id: string;
    exam_rule_id: string;
    tp_name: string;
    tc_name: string;
    coordinator_name: string;
    coordinator_contact_no: string;
    center_name: string;
    city: string;
    state: string;
    name: string;
    type: string;
    start_date: string;
    end_date: string;
    locales: [];
    geolocation_lat: string;
    geolocation_long: string;
    no_of_question: string;
    time: string;
    total_marks: number;
    assessor_id: string;
    annexure_m_url: string;
    assessor_feedback_form_url: string;
    assessor_declaration_form_url: string;
    center_incharge_declaration_from_url: string;
    candidate_feedback_form_url: string;
    center_feedback_form_url: string;
    annexure_n_url: string;
    no_equipment_declaration_url: string;
    no_document_declaration_url: string;
    attendance_sheet_url: string;
    equipment: [];
}
export interface IGetBatch {
    id: string;
    name: string;
    sector_name: string;
    assessor_first_name: string;
    assessor_last_name: string;
    candidate_count: number;
    job_role_name: string;
    practicalSubmitted: number;
    question_count: number;
}

export interface IStates {
    id: string;
    state_name: string;
}
export interface ICities {
    id: string;
    state_id: string;
    city_name: string;
}
export interface IBatchDetails {
    id: string;
    annexure_m_url: string;
    assessor_declaration_form_url: string;
    assessor_feedback_form_url: string;
    assessor_geo_location: string;
    assessor_id: string;
    assessor_img_url: string;
    attendance_declaration_url: string;
    batch_id: string;
    batch_locales: [];
    candidates: ICandidatesDetails[];
    center_feedback_form_url: string;
    center_incharge_declaration_from_url: string;
    center_name: string;
    city: string;
    coordinator_contact_no: string;
    coordinator_name: string;
    createdAt: string;
    date: string;
    equipment: [];
    exam_rule_id: string;
    geolocation: string;
    job_role_id: string;
    lab_video_url: string;
    name: string;
    no_document_declaration_url: string;
    no_equipment_declaration_url: string;
    no_of_question: string;
    scheme_id: string;
    sector_id: string;
    state: string;
    status: string;
    tc_name: string;
    time: string;
    total_marks: number;
    tp_name: string;
    type: string;
    updatedAt: string;
}

export interface ICandidatesDetails {
    adhaar_card_url: string;
    annexure_n_url: string;
    attendance_sheet_url: string;
    batch_id: string;
    candidate_id: string;
    contact_no: string;
    createdAt: string;
    doc_one_url: string;
    doc_two_url: string;
    enrollment_form_url: string;
    enrollment_no: string;
    father_name: string;
    feedback_form_url: string;
    id: string;
    img_url: string;
    isPresent: boolean;
    practical_img_url: string;
    practical_video_url: string;
    updatedAt: string;
    user: IUser[];
    viva_video_url: string;
}
export interface IUser {
    email: string;
    first_name: string;
    last_name: string;
    password: string;
}
export interface ICandidates {
    Candidate_Id: string;
    Candidate_First_Name: string;
    Candidate_Last_Name: string;
    Candidate_Father_Name: string;
    Candidate_Email: string;
    Candidate_Password: string;
    Candidate_Mobile_Number: string;
    Enrollment_Number: string;
}
export interface IBatchExamPost {
    first_name: string;
    last_name: string;
    email: string;
    password: string;
    candidate_id: string;
    father_name: string;
    contact_no: string;
    enrollment_no: string;
}
export interface IUpdateBatch {
    sector_id: string;
    scheme_id: string;
    job_role_id: string;
    exam_rule_id: string;
    tp_name: string;
    tc_name: string;
    coordinator_name: string;
    coordinator_contact_no: string;
    city: string;
    state: string;
    name: string;
    type: string;
    start_date: string;
    end_date: string;
    locales: [];
    geolocation_lat: string;
    geolocation_long: string;
    no_of_question: string;
    time: string;
    total_marks: number;
    assessor_id: string;
}
export interface ILocales {
    id: string;
    name: string;
    createdAt: string;
    updatedAt: string;
}
export interface IexamType {
    id: string;
    name: string;
}
export const examType: IexamType[] = [
    { id: '1', name: 'PRACTICAL' },
    { id: '2', name: 'THEORY' },
    { id: '3', name: 'BOTH' },
];
export const loginType: IexamType[] = [
    { id: '1', name: 'WEB' },
    { id: '2', name: 'APP' },
];
