/* eslint-disable @typescript-eslint/naming-convention */
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AssessorService } from 'app/modules/assessor/assessor.service';
import { ExamRulesService } from 'app/modules/exam-rules/exam-rules.service';
import { JobRoleService } from 'app/modules/job-role/job-role.service';
import { IJobRole } from 'app/modules/job-role/job-role.types';
import { PaperService } from 'app/modules/papers/papers.service';
import { IPapers } from 'app/modules/papers/papers.types';
import { SchemeService } from 'app/modules/scheme/scheme.service';
import { IScheme } from 'app/modules/scheme/scheme.types';
import { SectorService } from 'app/modules/sector/sector.service';
import { ISector } from 'app/modules/sector/sector.type';
import { ToastrService } from 'ngx-toastr';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BatchExamService } from '../batch-exam.service';
import { examType, ILocales, IStates, ICities } from '../batch-exam.type';

@Component({
    selector: 'app-add-batch',
    templateUrl: './add-batch.component.html',
    styleUrls: ['./add-batch.component.scss'],
})
export class AddBatchComponent implements OnInit {
    id: string;
    batchForm: FormGroup;
    isNew: boolean = true;
    submitted = false;
    isLoading: boolean = false;
    autoName: string = '';
    examType = examType;
    startDate: any;
    generateBatchId: boolean = false;
    // City State search
    public inputSearchStateCtrl: FormControl = new FormControl();
    public inputSearchCityCtrl: FormControl = new FormControl();
    public states: ReplaySubject<IStates[]> = new ReplaySubject<IStates[]>(1);
    public cities: ReplaySubject<ICities[]> = new ReplaySubject<ICities[]>(1);
    state: IStates[];
    city: ICities[];
    // City State search end
    // Get Data of services
    schemes: IScheme[];
    sectors: ISector[];
    jobRoles: IJobRole[];
    examRules: [];
    assessors: [] = [];
    language: ILocales[];
    paperSet: IPapers[];

    // For show total
    disableLanguageSelect = [];
    Marks: number = 0;
    count: any[] = [];
    weightage: any[] = [];
    totalMarks: number = 0;
    // For show total emd

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _formBuilder: FormBuilder,
        private _schemeService: SchemeService,
        private _sectorService: SectorService,
        private _jobRoleService: JobRoleService,
        private _examRulesService: ExamRulesService,
        private _assessorService: AssessorService,
        private _batchExamService: BatchExamService,
        private _route: Router,
        private _changeDetectorRef: ChangeDetectorRef,
        private _toastr: ToastrService,
        private _paperService: PaperService,
    ) {
        this.batchForm = _formBuilder.group({
            id: [''],
            batch_id: [''],
            sector_id: ['', [Validators.required]],
            paper_id: [''],
            scheme_id: ['', [Validators.required]],
            job_role_id: ['', [Validators.required]],
            exam_rule_id: ['', [Validators.required]],
            tp_name: ['', [Validators.required]],
            tc_name: ['', [Validators.required]],
            tp_email: ['', [Validators.required, Validators.email]],
            tp_address: ['', [Validators.required]],
            coordinator_name: ['', [Validators.required]],
            coordinator_contact_no: [
                '',
                [Validators.required, Validators.minLength(10)],
            ],
            coordinator_contact_no_2: [
                '',
                [Validators.required, Validators.minLength(10)],
            ],
            city: ['', [Validators.required]],
            state: ['', [Validators.required]],
            name: ['', [Validators.required]],
            type: ['', [Validators.required]],
            start_date: ['', [Validators.required]],
            end_date: ['', [Validators.required]],
            locales: [''],
            // geolocation_lat: [''],
            // geolocation_long: [''],
            no_of_question: this._formBuilder.array([this.newQuestions()]),
            time: ['', [Validators.required]],
            total_marks: [0, [Validators.required]],
            assessor_id: [''],
            // latitude: [''],
            // longitude: [''],
            // allowed_range: [''],
            
            
            annexure_m_url: [
                'https://iskill-development.s3.ap-south-1.amazonaws.com/Practical/Annexure-m-converted.pdf',
                [Validators.required],
            ],
            assessor_feedback_form_url: [
                'https://iskill-development.s3.ap-south-1.amazonaws.com/Practical/Assessor+Feedback+Form.pdf',
                [Validators.required],
            ],
            assessor_declaration_form_url: [
                'https://iskill-development.s3.ap-south-1.amazonaws.com/Practical/Declaration_Assossor.pdf',
                [Validators.required],
            ],
            center_incharge_declaration_from_url: [
                'https://iskill-development.s3.ap-south-1.amazonaws.com/Practical/Declaration+by+Center+in+chage.pdf',
                [Validators.required],
            ],
            candidate_feedback_form_url: [
                'https://iskill-development.s3.ap-south-1.amazonaws.com/Practical/candidate+feedback+form.pdf',
                [Validators.required],
            ],
            center_feedback_form_url: [
                'https://s3.console.aws.amazon.com/s3/object/iskill-development?region=ap-south-1&prefix=Practical/Annexure-N.pdf',
                [Validators.required],
            ],
            annexure_n_url: [
                'https://s3.console.aws.amazon.com/s3/object/iskill-development?region=ap-south-1&prefix=Practical/Annexure-N.pdf',
                [Validators.required],
            ],
            // eslint-disable-next-line max-len
            no_equipment_declaration_url: [
                'https://iskill-development.s3.ap-south-1.amazonaws.com/Practical/Declaration+If+Lab+Equip+are+not+available-converted.pdf',
                [Validators.required],
            ],
            no_document_declaration_url: [
                'https://iskill-development.s3.ap-south-1.amazonaws.com/Practical/Declaration.pdf',
                [Validators.required],
            ],
        });
    }

    ngOnInit(): void {
        this.getServiceData();
        this.id = this._activatedRoute.snapshot.paramMap.get('id');
        if (this.id) {
            this.isNew = false;
            this.isLoading = true;
            this._batchExamService
                .getBatchExamById(this.id)
                .subscribe((response: any) => {
                    this.isLoading = false;
                    const no_of_question = JSON.parse(
                        response.data.paper.no_of_question
                    );
                    for (let i = 1; i < no_of_question.length; i++) {
                        this.addQuestion();
                        this.count.push(no_of_question[i].count);
                        this.weightage.push(no_of_question[i].weightage);
                    }
                    const localesData = [];
                    response.data.batch_locales.map((data) => {
                        if (data.locale?.id) {
                            localesData.push(data.locale?.id);
                        }
                    });
                    if (response.data.batch_id) {
                        this.generateBatchId = true;
                        this.autoName = response.data.batch_id;
                    }
                    this.onSelectState(response.data.state?.id);
                    this.batchForm.patchValue({
                        id: response.data.id,
                        sector_id: response.data.sector_id,
                        scheme_id: response.data.scheme_id,
                        job_role_id: response.data.job_role_id,
                        exam_rule_id: response.data.exam_rule_id,
                        paper_id: response.data.paper_id ? response.data.paper_id : '',
                        tp_name: response.data.tp_name,
                        tc_name: response.data.tc_name,
                        coordinator_name: response.data.coordinator_name,
                        coordinator_contact_no:
                            response.data.coordinator_contact_no,
                        tp_email: response.data.tp_email,
                        tp_address: response.data.tp_address,
                        coordinator_contact_no_2: response.data.coordinator_contact_no_2,
                        city: response.data.city,
                        state: response.data.state?.id,
                        name: response.data.name,
                        type: response.data.type,
                        start_date: new Date(response.data.start_date),
                        end_date: new Date(response.data.end_date),
                        locales: localesData,
                        // geolocation_lat: response.data.geolocation_lat,
                        // geolocation_long: response.data.geolocation_long,
                        no_of_question: no_of_question,
                        time: response.data.time,
                        total_marks: response.data.paper.total_marks,
                        assessor_id: response.data.assessor_id,
                        // latitude: response.data.latitude,
                        // longitude: response.data.longitude,
                        // allowed_range: response.data.allowed_range,
                        // annexure_m_url: response.data.annexure_m_url,
                        // assessor_feedback_form_url:
                        //     response.data.assessor_feedback_form_url,
                        // assessor_declaration_form_url:
                        //     response.data.assessor_declaration_form_url,
                        // center_incharge_declaration_from_url:
                        //     response.data.center_incharge_declaration_from_url,
                        // candidate_feedback_form_url:
                        //     response.data.candidate_feedback_form_url,
                        // center_feedback_form_url:
                        //     response.data.center_feedback_form_url,
                        // annexure_n_url: response.data.annexure_n_url,
                        // no_equipment_declaration_url:
                        //     response.data.no_equipment_declaration_url,
                        // no_document_declaration_url:
                        //     response.data.no_document_declaration_url,
                    });
                });
        }
        this.inputSearchStateCtrl.valueChanges
            .subscribe(() => {
                this.filterSearch('states', 'inputSearchStateCtrl', 'state', 'state_name');
            });
        this.inputSearchCityCtrl.valueChanges
            .subscribe(() => {
                this.filterSearch('cities', 'inputSearchCityCtrl', 'city', 'city_name');
            });
    }

    get err() {
        return this.batchForm.controls;
    }
    // show weightage
    onChangeWeightage() {
        this.totalMarks = 0;
        this.count.map((data, i) => {
            if (this.weightage[i]) {
                this.totalMarks += +(data * this.weightage[i]);
            }
        });
    }
    // show weightage end

    // Add question
    questions(): FormArray {
        return this.batchForm.get('no_of_question') as FormArray;
    }

    newQuestions(): FormGroup {
        return this._formBuilder.group({
            count: [0],
            weightage: [0],
        });
    }

    removeQuestion(i: number) {
        this.totalMarks = 0;
        this.count.splice(i, 1);
        this.weightage.splice(i, 1);
        this.questions().removeAt(i);
    }

    addQuestion() {
        this.questions().push(this.newQuestions());
        this._changeDetectorRef.markForCheck();
    }
    // Add question and waitage end

    getServiceData() {
        this.isLoading = true;
        this._batchExamService.getStates().subscribe((response: any) => {
            this.state = response.data;
            this.states.next(response.data);
        });
        this._paperService.getPapers().subscribe((response: any) => {
            this.paperSet = response;
        });
        this._schemeService.getScheme().subscribe((response: any) => {
            if (response) {
                this.isLoading = false;
                this.schemes = response.length > 0 ? response : response.data;
            }
        });
        this._sectorService.getSector().subscribe((response: any) => {
            if (response) {
                this.isLoading = false;
                this.sectors = response.length > 0 ? response : response.data;
            }
        });
        this._jobRoleService.getJobRole().subscribe((response: any) => {
            if (response) {
                this.isLoading = false;
                this.jobRoles = response.length > 0 ? response : response.data;
            }
        });
        this._examRulesService.getExamRules().subscribe((response) => {
            if (response.status === 200) {
                this.isLoading = false;
                this.examRules = response.data;
            }
        });
        this._assessorService.getAccessors().subscribe((response) => {
            if (response.status === 200) {
                this.isLoading = false;
                for (const iterator of response.data) {
                    if (iterator['user']) {
                        iterator['user']['id'] = iterator['id'];
                        this.assessors.push(iterator['user']);
                    }
                }
            }
        });
        this._batchExamService.getLocales().subscribe((response: any) => {
            this.language = response.length > 0 ? response : response.data;
        });
    }

    onSelectState(value) {
        this._batchExamService.getCities(value).subscribe((res) => {
            this.city = res.data;
            this.cities.next(res.data);
        });
    }

    addUpdateBatch() {
        console.log('Value => ', this.batchForm.value);
   
        if (this.batchForm.valid) {
            if (this.batchForm.value.assessor_id === '') {
                this.batchForm.value.assessor_id = null;
            }
            this.batchForm.value.time = String(this.batchForm.value.time);
            // validation for no_of_question
            let totalMarks = 0;
            this.batchForm.value.no_of_question.map((x) => {
                totalMarks += x['count'] * x['weightage'];
            });
            if (totalMarks !== +this.batchForm.value.total_marks) {
                this._toastr.warning(
                    'Total number of marks doesn\'t match with total marks'
                );
                return false;
            }
            // validation for no_of_question end
            this.batchForm.value.total_marks = Number(
                this.batchForm.value.total_marks
            );
            if (this.isNew) {
                delete this.batchForm.value.id;
                this.batchForm.value.start_date = new Date(
                    this.batchForm.value.start_date
                ).toISOString();
                this.batchForm.value.end_date = new Date(
                    this.batchForm.value.end_date
                ).toISOString();
                this._batchExamService
                    .createBatchExam(this.batchForm.value)
                    .subscribe((response) => {
                        if (response.status === 200) {
                            this._route.navigate(['batch-exam']);
                        }
                    });
            } else {
                this._batchExamService
                    .updateBatchExam(
                        this.batchForm.value.id,
                        this.batchForm.value
                    )
                    .subscribe((response) => {
                        if (response.status === 200) {
                            this._route.navigate(['batch-exam']);
                        }
                    });
            }
        }
    }

    onSelectPaper(paperId) {
        this.count.length = 0;
        this.weightage.length = 0;
        this.clearFormArray(this.batchForm.get('no_of_question') as FormArray);
        const newPaper = this.paperSet.find(paper => paper.id === paperId);
        let no_of_question = [];
        if (newPaper) {
            no_of_question = JSON.parse(newPaper.no_of_question);
            for (const iterator of no_of_question) {
                this.addQuestion();
                this.count.push(iterator.count);
                this.weightage.push(iterator.weightage);
            }
            this.batchForm.patchValue({
                total_marks: newPaper.total_marks,
                no_of_question: no_of_question
            });
        }
    }
    clearFormArray = (formArray: FormArray) => {
        while (formArray.length !== 0) {
            formArray.removeAt(0);
        }
    };

    autoGenerateId() {
        this.batchForm.patchValue({
            batch_id: Math.random().toString(36).substring(2, 7),
        });
    }

    onCheckBatchId(event) {
        if (event) {
            this.generateBatchId = true;
            this.batchForm.get('batch_id').setValidators(Validators.required);
        } else {
            this.batchForm.get('batch_id').clearValidators();
            this.generateBatchId = false;
        }
        this.batchForm.get('batch_id').updateValueAndValidity();
    }

    protected filterSearch(observable, searchInput, itemArray, fieldName) {
        if (!this[observable]) {
            return;
        }
        // get the search keyword
        let search = this[searchInput].value;
        if (!search) {
            this[observable].next(this[itemArray]?.slice());
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the banks
        this[observable].next(
            this[itemArray].filter(bank => bank[fieldName].toLowerCase().indexOf(search) > -1)
        );
    }

    // protected filterSearchCity() {
    //     if (!this.cities) {
    //         return;
    //     }
    //     // get the search keyword
    //     let search = this.inputSearchCityCtrl.value;
    //     if (!search) {
    //         this.cities.next(this.city.slice());
    //         return;
    //     } else {
    //         search = search.toLowerCase();
    //     }
    //     // filter the banks
    //     this.cities.next(
    //         this.city.filter(bank => bank.city_name.toLowerCase().indexOf(search) > -1)
    //     );
    // }

}
