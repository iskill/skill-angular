/* eslint-disable @typescript-eslint/naming-convention */
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BatchExamService } from '../batch-exam.service';
import { examType, IBatchDetails, loginType } from '../batch-exam.type';
import { saveAs } from 'file-saver';
import { ConfirmationDailogComponent } from 'app/shared/component/confirmation-dailog/confirmation-dailog.component';
import { MatDialog } from '@angular/material/dialog';
import * as XLSX from 'xlsx';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { BatchRatingComponent } from '../batch-rating/batch-rating.component';
import * as Highcharts from 'highcharts';
import { ToastrService } from 'ngx-toastr';
declare var require: any;
const More = require('highcharts/highcharts-more');
More(Highcharts);
import Histogram from 'highcharts/modules/histogram-bellcurve';
Histogram(Highcharts);
import highcharts3D from 'highcharts/highcharts-3d';
import { checkTimeDifference } from 'app/utils/date-utils';
highcharts3D(Highcharts);
const ExportData = require('highcharts/modules/export-data');
ExportData(Highcharts);
const Accessibility = require('highcharts/modules/accessibility');
Accessibility(Highcharts);

export interface DialogData {
    rating: number;
    review: string;
  }

@Component({
    selector: 'app-view-batch',
    templateUrl: './view-batch.component.html',
    styleUrls: ['./view-batch.component.scss'],
})
export class ViewBatchComponent implements OnInit {
    id: string;
    isLoading: boolean = false;
    hideResetExam: boolean = false;
    batchDetails: IBatchDetails[] = [];
    practicalData = {
        theory_photos: [],
        group_photos: [],
        training_attendance_day_wises: [],
        training_attendance_sheets: [],
        training_photos: [],
        training_videos: [],
        assessor_img_url: '',
        lab_video_url: ''
    };
    documentData: any;
    examType = examType;
    questionsList = [];
    candidatesSnapshots = [];
    locales = [];
    languageIndex = 0;
    loginTypeValue = {
        1: 'WEB',
        2: 'APP'
    };
    queryParams: any;
    candidatesVideos: any[];
    videoUrl: SafeResourceUrl;
    rating: number;
    review: string;
    assessorGivenRating: any;
    assessorId:any
    averageRating: number;
    options: Highcharts.Options;
    collectionData: any;
    batchStartDateTime: string | Date | null = null;
    batchEndDateTime: string | Date | null = null;;

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _route: Router,
        private _serviceBatch: BatchExamService,
        private dialog: MatDialog,
        private _check: ChangeDetectorRef,
        private sanitizer: DomSanitizer,
        private _toaster: ToastrService,
    ) { }

    ngOnInit(): void {
        this.id = this._activatedRoute.snapshot.paramMap.get('id');
        if (this.id) {
            this.getBatchDetails();
        } else {
            this._route.navigate(['batch-exam']);
        }
        this.getCandidatesVideos()
        
        this.collectionData = {
            chart: {
              type: 'column',
              options3d: {
                enabled: true,
                alpha: 0,
                beta: 20,
                depth: 50,
                viewDistance: 0
              }
            },
            title: {
              text: 'Total Theory Marks Obtained by Batch',
          },
          tooltip: {
            pointFormat: 'Marks :{point.count}<b>{point.y:1f}</b>'
          },
          plotOptions: {
            column: {
              colorByPoint: true,
              colors: ['#DE3163'],
              dataLabels: {
                enabled: true,
                format: '{point.y}',
                style: {
                  textOutline: false
                }
              }
            }
          },
            xAxis: {
              opposite: false,
              categories: [],
      
              title: {
                text: 'Candidates Name',
                skew3d: true,
                style: {
                  fontSize: '20px'
                }
              },
            },
            yAxis: {
              allowDecimals: false,
              min: 0,
              title: {
                text: 'Candidates Marks',
                skew3d: true,
                style: {
                  fontSize: '16px'
                }
              },
            },
            credits: {
              enabled: false
            },
            series: [{
              data: [],
              showInLegend: false,
              colorByPoint: true,
              events: {
                afterAnimate: function() {
                  var points = this.points;
                  var maxMark = Math.max.apply(Math, points.map(function(point) {
                    return point.y;
                  }));
          
                  points.forEach(function(point) {
                    if (point.y === maxMark) {
                      point.update({
                        color: '#11ACF3' 
                      });
                    }
                  });
                }
              }
            }],
          };
    }

 
    getBatchDetails() {
        this.isLoading = true;
        this._serviceBatch
            .getBatchExamById(this.id, this.queryParams)
            .subscribe((response: any) => {
                this.isLoading = false;
                if (!this.queryParams) {
                  let noOfQuestion = 0;
                
                  if (response?.data?.paper?.no_of_question) {
                    try {
                      const parsedQuestions = JSON.parse(response.data.paper.no_of_question);
                      
                      if (Array.isArray(parsedQuestions)) {
                        parsedQuestions.map(d => (noOfQuestion += d.count));
                      }
                    } catch (error) {
                      console.error("Failed to parse no_of_question:", error);
                    }
                  }
                
                  response.data.no_of_question = noOfQuestion;
                
                  if (response?.data?.start_date) {
                    this.batchStartDateTime = response.data.start_date;
                    this.batchEndDateTime = response.data.end_date; 
                  
                    const batchStart = new Date(this.batchStartDateTime);
                    const batchEnd = new Date(this.batchEndDateTime);
                    const currentTime = new Date();
                  
                    if (currentTime >= batchStart && currentTime <= batchEnd) {
                      this.hideResetExam = false;
                    } else {
                      this.hideResetExam = true;
                    }
                  } else {
                    this.hideResetExam = true; 
                  }
                  
                  
                
                  response.data.candidates.map((candidates) => {
                    candidates.candidate_logs.sort((a, b) => {
                      const dateA = new Date(a.createdAt).getTime();
                      const dateB = new Date(b.createdAt).getTime();
                      return dateA > dateB ? -1 : 1;
                    });
                  });
                
                  const batchdetails = response.data.candidates.filter(data => data.user);
                  response.data.batch_locales = response.data.batch_locales.map(data => data.locale?.name);
                  response.data.candidates = batchdetails;
                  this.batchDetails = response.data;
                  this.assessorId = response.data.assessor?.id;
                
                  this.collectionData.series[0].data = [];
                  for (let i = 0; i < this.batchDetails['candidates'].length; i++) {
                    this.collectionData.series[0]['data'].push({
                      y: +this.batchDetails['candidates'][i].marks,
                      count: this.batchDetails['candidates'][i].total_marks,
                    });
                    this.collectionData.xAxis.categories.push(this.batchDetails['candidates'][i].user?.first_name);
                  }
                
                  Highcharts.chart('container', this.collectionData);
                }
                
                this.getBatchRating()
                // if (this.queryParams === 'practical') {
                //     this.practicalData = response.data;
                // }
                if (this.queryParams === 'documents') {
                    this.documentData = response.data;
                }
                if (!response.data) {
                    this._route.navigate(['batch-exam']);
                }
            });
    }

    exportCandidateWiseResult() {
        this.batchDetails['candidates'].map((data) => {
            if (data) {
                this._serviceBatch.getCandidatesDetails(data.id, 'result').subscribe((res: any) => {
                    this.candidateWiseResultExport(res.data);
                });
            }
        });
    }
    //  Candidate Feedback
    exportBatchFeedback() {
        this._serviceBatch.getCandidatesFeedback(this.id).subscribe(
          (res: any) => {
            const blob = new Blob([res], { type: 'application/octet-stream' });
            saveAs(blob, 'feedback.xlsx');
          },
          (error: any) => {
            console.error('Failed to download the file.', error);
          }
        );
      }
      
    //   Batch Attendance
      exportBatchAttendance() {
        this._serviceBatch.getAttendance(this.id).subscribe(
          (res: any) => {
            const blob = new Blob([res], { type: 'application/octet-stream' });
            saveAs(blob, 'attendance.xlsx');
          },
          (error: any) => {
            console.error('Failed to download the file.', error);
          }
        );
      }
    //   Batch Training  attendance
      exportTrainingAttendance() {
        this._serviceBatch.getTrainingAttendance(this.id).subscribe(
          (res: any) => {
            const blob = new Blob([res], { type: 'application/octet-stream' });
            saveAs(blob, 'training_attendance.xlsx');
          },
          (error: any) => {
            console.error('Failed to download the file.', error);
          }
        );
      }
    //   Candidate Status
      exportCandidateStatus() {
        this._serviceBatch.getCandidateStatus(this.id).subscribe(
          (res: any) => {
            const blob = new Blob([res], { type: 'application/octet-stream' });
            saveAs(blob, 'candidate_status.xlsx');
          },
          (error: any) => {
            console.error('Failed to download the file.', error);
          }
        );
      }
    //   Question Sheet
      exportQuestion() {
        this._serviceBatch.getQuestion(this.batchDetails['job_role_id']).subscribe(
          (res: any) => {
            const blob = new Blob([res], { type: 'application/octet-stream' });
            saveAs(blob, 'all_Questions.xlsx');
          },
          (error: any) => {
            console.error('Failed to download the file.', error);
          }
        );
      }

      openRatingDialog(): void {
        const dialogRef = this.dialog.open(BatchRatingComponent, {
          data: {rating: this.rating, review: this.review},
        });
    
        dialogRef.afterClosed().subscribe(result => {
           
            if (result) {
                this._serviceBatch.postRating(this.batchDetails['assessor_id'] ,{rating:result.rating ,batch_id:this.id,review:result.review ,}).subscribe((res)=>{
                    const a = document.createElement('a');
                    a.click();
                    a.remove();
                })
            }
        });
      }

      getBatchRating (){
        this._serviceBatch.getRating(this.assessorId).subscribe((res)=>{
            const ratings = res?.data?.map(item => item.rating);
            this.assessorGivenRating= ratings
           let sum = 0;
           for (let i = 0; i < this.assessorGivenRating?.length; i++) {
             sum += this.assessorGivenRating[i];
           }
           this.averageRating = sum / this.assessorGivenRating?.length;
        })
      }
      

    candidateWiseResultExport(candidateResult) {
        const modifiedArray = [];
        // 'Batch ID', 'Candidate Name', 'Candidate ID', 'Login Time', 'Submit Time'
        // modifiedArray.push({
        //     'Batch ID': candidateResult.batch.batch_id,
        //     'Candidate Name': candidateResult.user?.first_name,
        //     'Candidate ID': candidateResult.candidate_id,
        //     'Login Time': new Date(candidateResult.candidate_logs[0].createdAt),
        //     'Submit Time': new Date(candidateResult.candidate_logs[1].createdAt),
        // });

        // modifiedArray.push({ 'Batch ID': candidateResult.batch.batch_id });
        // modifiedArray.push({ 'Candidate Name': candidateResult.user?.first_name });
        // modifiedArray.push({ 'Candidate ID': candidateResult.candidate_id });
        // modifiedArray.push({ 'Login Time': new Date(candidateResult.candidate_logs[0].createdAt) });
        // modifiedArray.push({ 'Submit Time': new Date(candidateResult.candidate_logs[1].createdAt) });
        candidateResult.results.map((data, index) => {
            if (data) {
                modifiedArray.push({
                    'Sr No.': index + 1,
                    // 'NOS': 'N/A',
                    'Question': data.question?.img_url ? data.question?.img_url : data.question?.question_locales[0]?.text,
                    'Weightage': data.question?.weightage,
                    'Correct Answer': data.option?.option_locales[0]?.text,
                    // 'Attempted Answer': 'N/A',
                    'Wrong/Right': data.option?.isCorrect ? 'Right' : 'Wrong',
                    'Marks Obtained': candidateResult.marks
                });
            }
        });
        if (modifiedArray.length !== 0) {
            const wb = XLSX.utils.book_new();
            const ws = XLSX.utils.json_to_sheet(modifiedArray);
            ws['!cols'] = [{ wch: 5 }, { wch: 30 }, { wch: 10 }, { wch: 30 }, { wch: 10 }, { wch: 13 }];
            XLSX.utils.book_append_sheet(wb, ws, 'candidate sheet');
            XLSX.writeFile(wb, candidateResult.user?.first_name + '_result.xlsx');
        }
    }

    exportResult() {
        const modifiedArray = [];
        this.batchDetails['candidates'].map((data) => {
            if (data) {
                modifiedArray.push({
                    'Candidate Id': data.candidate_id,
                    'Candidate First Name': data.user.first_name,
                    // 'Candidate Last Name': data.user.last_name, //, { wch: 18 }
                    'Candidate Email': data.user.email,
                    'Theory Submitted': data.theorySubmitted ? 'Yes' : 'N/A',
                    'Marks': data.marks ? String(data.marks) : 'N/A',
                    'Total Marks': String(this.batchDetails['total_marks'])
                });
            }
        });
        const wb = XLSX.utils.book_new();
        const ws = XLSX.utils.json_to_sheet(modifiedArray);
        ws['!cols'] = [{ wch: 12 }, { wch: 18 }, { wch: 27 }, { wch: 15 }, { wch: 5 }, { wch: 9 }];
        XLSX.utils.book_append_sheet(wb, ws, 'candidate sheet');
        XLSX.writeFile(wb, 'candidates-result.xlsx');
    }



    getCandidatesSnapshots() {
        this.getCandidatesVideos()
        this.candidatesSnapshots = [];
        if (this.batchDetails['theorySubmitted']) {
            this.batchDetails['candidates'].map((candidate) => {
                const path = `batches/${this.batchDetails['batch_id']}/candidates/${candidate.user.first_name}_${candidate.user.last_name}`;
                this._serviceBatch.getAllFiles(path, 1).then((res) => {
                    if (res.Contents.length !== 0) {
                        res.Contents.map((data) => {
                            data.Key = 'https://iskill-development.s3.ap-south-1.amazonaws.com/' + data.Key;
                            const pathSplit = data.Key.split('/');
                            const finalName = pathSplit[pathSplit.length - 1].split('.')[0];
                            data['Name'] = candidate.user.first_name ? candidate.user.first_name : 'N/A';
                            data['Time'] = new Date(+finalName).toISOString();
                            data['candidateId'] = candidate.candidate_id;
                        });
                        this.candidatesSnapshots.push(res.Contents[0]);
                    }
                });
            });
        }
        this._check.markForCheck();
        // const path = `batches/${this.batchDetails['batch_id']}/candidates`;
        // this._serviceBatch.getAllFiles(path, 40).then((res) => {
        //     console.log(res.Contents);
        //     res.Contents.map((data, index) => {
        //         data.Key = 'https://iskill-development.s3.ap-south-1.amazonaws.com/' + data.Key;
        //         const pathSplit = data.Key.split('/');
        //         const finalName = pathSplit[pathSplit.length - 1].split('.')[0];
        //         try {
        //             data['Name'] = new Date(+finalName).toISOString();
        //         } catch (error) {
        //             data['Name'] = '';
        //         }
        //     });
        //     this.candidatesSnapshots = res.Contents;
        // });
    }

    getCandidatesVideos() {
        this.candidatesVideos = [];
        if (this.batchDetails['theorySubmitted']) {
            this.batchDetails['candidates'].map((candidate) => {
                // const path = `batches/Demo/candidates/Palak_NA/video/Palak_NA.webm`;
                const path = `batches/${this.batchDetails['batch_id']}/candidates/${candidate.user.first_name}_${candidate.user.last_name}/video/`;
                this._serviceBatch.getAllFiles(path, 1).then((res) => {
                    if (res.Contents.length !== 0) {
                        res.Contents.map((data) => {
                            data.Key = 'https://iskill-development.s3.ap-south-1.amazonaws.com/' + data.Key;
                            const pathSplit = data.Key.split('/');
                            const finalName = pathSplit[pathSplit.length - 2].split('.')[0];
                            data['Name'] = candidate.user.first_name ? candidate.user.first_name : 'N/A';
                            data['candidateId'] = candidate.candidate_id;
                        });
                        this.candidatesVideos.push(res.Contents[0]);
                        for(let i = 0; i < this.candidatesVideos.length; i++){
                            this.sanitizer.bypassSecurityTrustResourceUrl(this.candidatesVideos[i].Key)
                        }
                    }
                });
            });
        }
        this._check.markForCheck();
    }
    hasVideoForCandidate(candidateId: string): boolean {
        return this.candidatesVideos.some(item => item.candidateId === candidateId);
    }
    
    downloadZip(id) {
        this._serviceBatch.downloadZip(id).subscribe((res) => {
            window.open(res.data['Location']);
        });
    }

    downloadFile(url) {
        return saveAs(url);
    }

    // Open delete confirmation dialog
    openDialog(id, first, last) {
        const dialogRef = this.dialog.open(ConfirmationDailogComponent, {
            data: {
                message: 'Are you sure want to reset data?',
                buttonText: {
                    ok: 'Yes',
                    cancel: 'No'
                }
            }
        });
        dialogRef.afterClosed().subscribe((confirmed: boolean) => {
            if (confirmed) {
                this.resetExam(id, first, last);
                const a = document.createElement('a');
                a.click();
                a.remove();
            }
        });
    }
    // Open delete confirmation dialog end

    resetExam(id, firstName, lastName) {
        const path = `batches/${this.batchDetails['batch_id']}/candidates/${firstName}_${lastName}/`;
        this._serviceBatch.deleteFileS3(path);
        this._serviceBatch.resetCandidateExam(id).subscribe((res) => {
            this.ngOnInit();
        });
    }
    getQuestions() {
        if (this.questionsList.length === 0) {
            this.isLoading = true;
            this._serviceBatch.getQuestions(this.id).subscribe((res: any) => {
                this.isLoading = false;
                this.questionsList = res.data;
                this.locales = res.data[0].question.question_locales.map(lan => lan.locale);
            });
        }
    }
    onChangeLanguage(event) {
        this.languageIndex = this.locales.findIndex(data => data.id === event);
    }
    selectedTabValue(event) {
        if (event.tab.textLabel === 'Questions List') {
            this.getQuestions();
        }
        if (event.tab.textLabel === 'Candidates snapshots') {
            this.getCandidatesSnapshots();
        }
        if (event.tab.textLabel === 'Documents List') {
            this.queryParams = 'documents';
            if (!this.documentData) {
                this.getBatchDetails();
            }
        }
        if (event.tab.textLabel === 'Practical Photos/Videos') {
            // this.queryParams = 'practical';
            if (!this.practicalData.theory_photos ||
                !this.practicalData.assessor_img_url ||
                !this.practicalData.group_photos ||
                !this.practicalData.lab_video_url ||
                !this.practicalData.training_attendance_day_wises ||
                !this.practicalData.training_attendance_sheets ||
                !this.practicalData.training_photos ||
                !this.practicalData.training_videos) {
                // this.getBatchDetails();
                const path = `batches/${this.batchDetails['batch_id']}/practical/`;
                this._serviceBatch.getAllFiles(path).then((res: any) => {
                    res.Contents.map((item) => {
                        if (item.Key.includes('/AssessorTheoryPhotos')) {
                            this.practicalData.theory_photos.push('https://iskill-development.s3.ap-south-1.amazonaws.com/' + item.Key);
                        }
                        if (item.Key.includes('/AssessorGroupPhotos')) {
                            this.practicalData.group_photos.push('https://iskill-development.s3.ap-south-1.amazonaws.com/' + item.Key);
                        }
                        if (item.Key.includes('/TrainingAttendanceOfEachDay')) {
                            this.practicalData.training_attendance_day_wises.push('https://iskill-development.s3.ap-south-1.amazonaws.com/' + item.Key);
                        }
                        if (item.Key.includes('/TrainingAttendanceSheet')) {
                            this.practicalData.training_attendance_sheets.push('https://iskill-development.s3.ap-south-1.amazonaws.com/' + item.Key);
                        }
                        if (item.Key.includes('/PhotoOfTraining')) {
                            this.practicalData.training_photos.push('https://iskill-development.s3.ap-south-1.amazonaws.com/' + item.Key);
                        }
                        if (item.Key.includes('/VideoOfTraining')) {
                            this.practicalData.training_videos.push('https://iskill-development.s3.ap-south-1.amazonaws.com/' + item.Key);
                        }
                        if (item.Key.includes('/Assessor.')) {
                            this.practicalData.assessor_img_url = 'https://iskill-development.s3.ap-south-1.amazonaws.com/' + item.Key;
                        }
                        if (item.Key.includes('/AssessmentVideo')) {
                            this.practicalData.lab_video_url = 'https://iskill-development.s3.ap-south-1.amazonaws.com/' + item.Key;
                        }
                    })
                }).catch((error) => {
                    console.error(error);
                })
            }
        }
    }

    dialogSubmit(id, first, last) {
      const dialogRef = this.dialog.open(ConfirmationDailogComponent, {
          data: {
              message: 'Are you sure want to submit exam?',
              buttonText: {
                  ok: 'Yes',
                  cancel: 'No'
              }
          }
      });
      dialogRef.afterClosed().subscribe((confirmed: boolean) => {
          if (confirmed) {
              this.submitExam(id);
              const a = document.createElement('a');
              a.click();
              a.remove();
          }
      });
  }

  submitExam(id) {
    this.isLoading = true;
      this._serviceBatch.submitCandidateExam(id).subscribe((res) => {
        if(res){
          this.isLoading = false;
          this._toaster.success(res.message)
        }
          this.ngOnInit();
      });
  }
}
