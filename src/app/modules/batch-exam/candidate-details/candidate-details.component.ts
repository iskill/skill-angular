/* eslint-disable max-len */
import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    OnDestroy,
    OnInit,
    ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BatchExamService } from '../batch-exam.service';
import { loginType } from '../batch-exam.type';

import { connect } from 'twilio-video';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-candidate-details',
    templateUrl: './candidate-details.component.html',
    styleUrls: ['./candidate-details.component.scss'],
})
export class CandidateDetailsComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('myVideo') myVideo: any;

    videoElement: any;
    id: string;
    isLoading: boolean = false;
    candidateDetails = [];
    candidateResult: any;
    feedbackData: any;
    snapshotImages: any;
    candidateLogs: any;
    addharAndSelfie: any;
    videoRecordedUrl: any;
    defaultImage =
        'https://images.unsplash.com/photo-1443890923422-7819ed4101c0?fm=jpg';
    isLiveStreaming = false;
    getLiveStream = false;
    closedLiveStream = false;
    loginTypeValue = loginType;
    query = '';
    candidatePractical = {
        img_url: '',
        adhaar_card_url: '',
        practical_img_url: '',
        practical_video_url: '',
        viva_video_url: ''
    };
    candidatesVideos: any[];

    constructor(
        private _activatedRoute: ActivatedRoute,
        private candidateService: BatchExamService,
        private changeDetectorRef: ChangeDetectorRef,
        private sanitizer: DomSanitizer
    ) { }

    ngOnInit(): void {
        this.id = this._activatedRoute.snapshot.paramMap.get('id');
        this.getCandidatesDetails();
        this.getVideoRecording();
    }
    getLiveStreamData() {
        this.getLiveStream = true;
        this.candidateService.getTwilioToken(this.id).subscribe((res: any) => {
            this.isLiveStreaming = true;
            connect(res.data, { name: this.id }).then(room => {
                console.log(`Successfully joined a Room: ${room}`);
                room.participants.forEach(participantConnected);
                room.on('participantConnected', participantConnected);
                room.on('participantConnected', participant => {
                    console.log(`A remote Participant connected: ${participant}`);
                });
                room.once('disconnected', error => room.participants.forEach(participantDisconnected));
            }, error => {
                this.isLiveStreaming = false;
                this.getLiveStream = false;
                console.error(`Unable to connect to Room: ${error.message}`);
            });
        })
        function participantConnected(participant) {
            console.log('participant', participant)
            console.log('Participant "%s" connected', participant.identity);

            const div = document.getElementById('myVideo');
            console.log(div);
            // this.getLiveStream = false;

            participant.on('trackSubscribed', track => {
                console.log(track);
                trackSubscribed(div, track)
            });
            participant.on('trackUnsubscribed', trackUnsubscribed);

            participant.tracks.forEach(publication => {
                if (publication.isSubscribed) {
                    trackSubscribed(div, publication.track);
                }
            });
        }
        function participantDisconnected(participant) {
            console.log('Participant "%s" disconnected', participant.identity);
            document.getElementById(participant.sid).remove();
        }
        function trackSubscribed(div, track) {
            console.log(div, track);
            div.appendChild(track.attach());
        }

        function trackUnsubscribed(track) {
            track.detach().forEach(element => element.remove());
        }

    }


    ngAfterViewInit() { }
    ngOnDestroy() {
    }
    getVideoRecording() {
        this.candidatesVideos = [];
        const path = `batches/${this.candidateDetails['batch']?.batch_id}/candidates/${this.candidateDetails['user']?.first_name}_${this.candidateDetails['user']?.last_name}/video/`;
        this.candidateService.getAllFiles(path).then((response) => {
            response.Contents.map((data) => {
                data.Key =
                    'https://iskill-development.s3.ap-south-1.amazonaws.com/' +
                    data.Key;
            });
            if (response.Contents.length !== 0) {
                this.videoRecordedUrl = response.Contents[0].Key;
                response.Contents.map((data) => {
                    
                    const pathSplit = data.Key.split('/');
                    const finalName = pathSplit[pathSplit.length - 1].split('.')[0];
                    data['Name'] = this.candidateDetails['user']?.first_name ? this.candidateDetails['user']?.first_name : 'N/A';
                    data['candidateId'] = this.candidateDetails['candidate_id'];
                });
                this.candidatesVideos.push(response.Contents[0]);
                console.log('this.candidatesVideos ==> ', this.candidatesVideos);
            }
        });
    }
    getCandidatesDetails() {
        this.isLoading = true;
        this.candidateService
            .getCandidatesDetails(this.id, this.query)
            .subscribe((res: any) => {
                this.isLoading = false;
                if (!this.query) {
                    this.candidateDetails = res.data;
                    this.getSnapshotAndVideo();
                    this.getSnapshots();
                }
                if (this.query === 'log') {
                    console.log(res.data.candidate_logs);
                    res.data.candidate_logs = res.data.candidate_logs.sort((a, b) => {
                        const dateA = new Date(a.createdAt).getTime();
                        const dateB = new Date(b.createdAt).getTime();
                        return dateB < dateA ? -1 : 1;
                    });
                    this.candidateLogs = res.data.candidate_logs;
                }
                if (this.query === 'result') {
                    this.candidateResult = res.data.results;
                }
                // if (this.query === 'practical') {
                //     this.candidatePractical = res.data;
                //     console.log('candidatePractical', this.candidatePractical);
                // }
            });
    }
    getSnapshotAndVideo() {
        const path = `batches/${this.candidateDetails['batch'].batch_id}/candidates/${this.candidateDetails['user'].first_name}_${this.candidateDetails['user'].last_name}/theory/`;
        this.candidateService.getAllFiles(path).then((response) => {
            console.log(response);
            response.Contents.map((data) => {
                data.Key =
                    'https://iskill-development.s3.ap-south-1.amazonaws.com/' +
                    data.Key;
            });
            this.addharAndSelfie = response.Contents;
            console.log(response.Contents);
        });
        this.getVideoRecording();
    }

    getResult() {
        this.isLoading = true;
        this.candidateService.getCandidateResult(this.id).subscribe((res: any) => {
            this.isLoading = false;
            this.candidateResult = res.data.results;
            this.changeDetectorRef.markForCheck();
        });
    }

    goBack() {
        window.history.back();
    }
    getSnapshots() {
        const path = `batches/${this.candidateDetails['batch'].batch_id}/candidates/${this.candidateDetails['user'].first_name}_${this.candidateDetails['user'].last_name}/snapshot`;
        console.log(path);
        this.candidateService.getAllFiles(path).then((res) => {
            console.log(res);
            res.Contents.map((data) => {
                data.Key =
                    'https://iskill-development.s3.ap-south-1.amazonaws.com/' +
                    data.Key;
                const fileName = new Date(
                    +data.Key.split('/').slice(-1)[0].split('.')[0]
                ).toISOString();
                // console.log(new Date(+fileName));
                data['Name'] = fileName;
            });
            this.snapshotImages = res.Contents;
            // console.log(this.snapshotImages);
        });
    }
    selectedTabValue(event: { tab: { textLabel: string } }) {
        if (event.tab.textLabel === 'Feedback') {
            this.candidateService.getFeedbackData(this.id).subscribe((res) => {
                this.feedbackData = res.data;
            });
        }
        if (event.tab.textLabel === 'Practical') {
            // this.query = 'practical';
            // this.getCandidatesDetails();
            if (!this.candidatePractical.img_url ||
                !this.candidatePractical.adhaar_card_url ||
                !this.candidatePractical.practical_img_url ||
                !this.candidatePractical.practical_video_url ||
                !this.candidatePractical.viva_video_url) {
                // this.getBatchDetails();
                const path = `batches/${this.candidateDetails['batch'].batch_id}/candidates/${this.candidateDetails['user'].first_name}_${this.candidateDetails['user'].last_name}/practical/`;
                this.candidateService.getAllFiles(path).then((res: any) => {
                    console.log('res.Contents ==> ', res.Contents);
                    res.Contents.map((item) => {
                        if (item.Key.includes('/candidateTakeIndividualPhotos')) {
                            this.candidatePractical.img_url = 'https://iskill-development.s3.ap-south-1.amazonaws.com/' + item.Key;
                        }
                        if (item.Key.includes('/Aadhaar')) {
                            this.candidatePractical.adhaar_card_url = 'https://iskill-development.s3.ap-south-1.amazonaws.com/' + item.Key;
                        }
                        if (item.Key.includes('/candidateTakePracticalPhoto')) {
                            this.candidatePractical.practical_img_url = 'https://iskill-development.s3.ap-south-1.amazonaws.com/' + item.Key;
                        }
                        if (item.Key.includes('/candidatePracticalVideo')) {
                            this.candidatePractical.practical_video_url = 'https://iskill-development.s3.ap-south-1.amazonaws.com/' + item.Key;
                        }
                        if (item.Key.includes('/candidatePracticalVideoViva')) {
                            this.candidatePractical.viva_video_url = 'https://iskill-development.s3.ap-south-1.amazonaws.com/' + item.Key;
                        }
                    })
                }).catch((error) => {
                    console.error(error);
                })
            }
        }
        if (event.tab.textLabel === 'Theory') {
            this.query = 'result';
            this.getCandidatesDetails();
        }
        if (event.tab.textLabel === 'logs') {
            this.query = 'log';
            this.getCandidatesDetails();
        }
        if (event.tab.textLabel === 'Videos') {
            this.query = 'video';
            this.getSnapshotAndVideo();
        }
    }
    getActualAnswer(resultData: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        options: { option_locales: { text: any }[] }[];
    }) {
        let result: any;
        if (resultData.options[0]['img_url']) {
            result = resultData.options[0]['img_url'];
        }
        if (resultData.options[0].option_locales[0]?.text) {
            result = resultData.options[0].option_locales[0].text;
        }
        return result;
    }
}
