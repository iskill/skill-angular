/* eslint-disable @typescript-eslint/naming-convention */
import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { ConfirmationDailogComponent } from 'app/shared/component/confirmation-dailog/confirmation-dailog.component';
import { ToastrService } from 'ngx-toastr';
import { fromEvent, Observable, of, Subject } from 'rxjs';
import { AssessorService } from '../assessor/assessor.service';
import { IAssessor } from '../assessor/assessor.type';
import { BatchExamService } from './batch-exam.service';
import { IBatchExam, IGetBatch } from './batch-exam.type';
import * as XLSX from 'xlsx';
import { debounceTime, distinctUntilChanged, filter, map, switchMap, tap } from 'rxjs/operators';

@Component({
    selector: 'app-batch-exam',
    templateUrl: './batch-exam.component.html',
    styleUrls: ['./batch-exam.component.scss'],
})
export class BatchExamComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild('searchFilter') searchFilter;
    batchExam$: Observable<IGetBatch[]>;

    isLoading: boolean = false;
    batchExamTableColumns: string[] = [
        'name',
        'jobRole',
        'sector',
        'assessor',
        'questions',
        'candidate',
        'practicalStatus',
        'theoryStatus',
        'actions',
    ];
    // 'theoryStatus',
    batchExamCount: number = 0;
    showAssessorModal: boolean = false;
    showExamModal: boolean = false;
    assessorList: IAssessor[];
    assessorSelect: any;
    StartDate: any;
    EndDate: any;
    selectedBatch: IBatchExam;
    // Pagination
    pageSize = 25;
    pageIndex = 0;
    pageSizeOptions = [25, 50, 100, 200];
    showFirstLastButtons = true;
    // Pagination end
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _bathcExamService: BatchExamService,
        private _assessorService: AssessorService,
        private dialog: MatDialog,
        private _tostr: ToastrService
    ) { }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.batchExam$ = this._bathcExamService.batchExams$;
        this.isLoading = true;
        this.getBatchExams();
        this._assessorService.getAccessors().subscribe((res) => {
            this.assessorList = res.data;
        });
    }

    getBatchExams() {
        this._bathcExamService.getBatchExams(this.pageSize, this.pageIndex).subscribe((res: any) => {
            this.searchFilter.nativeElement.value = null;
            this.isLoading = false;
            // Update the counts
            this.batchExamCount = res.data.count;
            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
    }
    // Open delete confirmation dialog
    openDialog(id) {
        const dialogRef = this.dialog.open(ConfirmationDailogComponent, {
            data: {
                message: 'Are you sure want to delete?',
                buttonText: {
                    ok: 'Yes',
                    cancel: 'No'
                }
            }
        });
        dialogRef.afterClosed().subscribe((confirmed: boolean) => {
            if (confirmed) {
                this.deleteSelectedBatch(id);
                const a = document.createElement('a');
                a.click();
                a.remove();
            }
        });
    }
    // Open delete confirmation dialog end

    handlePageEvent(event: PageEvent) {
        this.batchExamCount = event.length;
        this.pageSize = event.pageSize;
        this.pageIndex = event.pageIndex;
        this.getBatchExams();
    }

    toggleModal(batch?: IBatchExam) {
        if (batch) {
            console.log(batch);
            this.selectedBatch = batch;
            this.assessorSelect = batch.assessor_id;
            console.log(this.assessorList.find(data => data.id === this.assessorSelect));
            this.StartDate = '';
            this.EndDate = '';
        }
        this.showAssessorModal = !this.showAssessorModal;
    }
    // 4cc0edc0-0ee3-11ec-966c-db80ca80cec2
    // 4cc0edc0-0ee3-11ec-966c-db80ca80cec2
    toggleExamModal(batch?: IBatchExam) {
        this.showAssessorModal = false;
        if (batch) {
            this.selectedBatch = batch;
            console.log('batch => ', batch);
            this.StartDate = this.selectedBatch.start_date;
            this.EndDate = this.selectedBatch.end_date;
            this.assessorSelect = '';
        }
        this.showExamModal = !this.showExamModal;
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next({});
        this._unsubscribeAll.complete();
    }
    ngAfterViewInit() {
        this.searchFilterChange();
    }
    searchFilterChange() {
        const searchTerm2 = fromEvent<any>(this.searchFilter.nativeElement, 'keyup');
        searchTerm2.pipe(
            map(event => event.target.value),
            debounceTime(600),
            distinctUntilChanged()
        ).subscribe((res) => {
            this._bathcExamService.getBatchExams(this.pageSize, this.pageIndex, { q: res }).subscribe((response: any) => {
                this.batchExamCount = response.data['count'];
            });
        });
    }

    filterAssessor(event) {
        const value = event.target.value.toLowerCase();
        this._assessorService.assessors$.subscribe((res) => {
            this.assessorList = res.filter(item =>
                (item.user['first_name'] + item.user['last_name'])
                    .toLocaleLowerCase()
                    .includes(value));
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    assignAssessor(type) {
        let updatedData = {};
        if (type === 'Assessor') {
            this.selectedBatch.assessor_id = this.assessorSelect;
            updatedData = { assessor_id: this.assessorSelect };
        }
        console.log(this.selectedBatch);
        if (type === 'ExamDate') {
            // if (this.selectedBatch.start_date !== this.StartDate || this.selectedBatch.end_date !== this.EndDate) {
            updatedData['start_date'] = this.StartDate;
            updatedData['end_date'] = this.EndDate;
        }
        this._bathcExamService.updateBatchExam(this.selectedBatch.id, updatedData).subscribe((res) => {
            if (res.status === 200) {
                this.showAssessorModal = false;
                this.showExamModal = false;
                this.getBatchExams();
            }
        });
    }

    autoAddQuestions(batchId) {
        this._bathcExamService.addQuestions(batchId).subscribe((res) => { }, (error) => {
            this._tostr.warning(error.error.message);
        });
    }
    resetQuestions(batchId) {
        this._bathcExamService.resetQuestions(batchId).subscribe((res) => { }, (error) => {
            this._tostr.warning(error.error.message);
        });
    }

    deleteSelectedBatch(batchId): void {
        this._bathcExamService.deleteBatchExam(batchId).subscribe((res) => {
            if (res.status === 200) {
                this._changeDetectorRef.markForCheck();
            }
        });
    }

    downloadZip(id) {
        this._bathcExamService.downloadZip(id).subscribe((res) => {
            window.open(res.data['Location']);
        });
    }
    downloadSheet(id) {
        this._bathcExamService.downloadSheet(id).subscribe((res) => {
            console.log(res);
            this.convertData(res);
        });
    }

    convertData(candidateData) {
        const modifiedArray = [];
        candidateData.map((data) => {
            if (data.user) {
                modifiedArray.push({
                    'Candidate_Id': data.candidate_id,
                    'Candidate_First_Name': data.user.first_name,
                    'Candidate_Last_Name': data.user.last_name,
                    'Candidate_Father_Name': data.father_name,
                    'Candidate_Email': data.user.email,
                    'Candidate_Password': data.password,
                    'Candidate_Mobile_Number': data.contact_no,
                    'Enrollment_Number': data.enrollment_no,
                });
            }
        });
        const wb = XLSX.utils.book_new();
        const ws = XLSX.utils.json_to_sheet(modifiedArray);
        ws['!cols'] = [{ wch: 12 }, { wch: 18 }, { wch: 18 }, { wch: 20 }, { wch: 25 }, { wch: 16 }, { wch: 22 }, { wch: 18 }];
        XLSX.utils.book_append_sheet(wb, ws, 'candidate sheet');
        XLSX.writeFile(wb, 'candidates-sheet.xlsx');
    }

    trackByFn(index: number, item: any): any {
        return item.id || index;
    }
    onPublishChange(id, event) {
        console.log(id, event);
        this._bathcExamService.updateBatchExam(id, { isPublish: event }).subscribe((res) => {
            this.getBatchExams();
        });
    }
}
