/* eslint-disable @typescript-eslint/naming-convention */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, pluck, switchMap, take, tap } from 'rxjs/operators';
import { ApiResponse } from '../scheme/scheme.types';
import { IBatchExam, ICities, IGetBatch, ILocales, IStates } from './batch-exam.type';
import * as AWS from 'aws-sdk';

@Injectable({
    providedIn: 'root',
})
export class BatchExamService {
    // Private
    private _batchExams: BehaviorSubject<IGetBatch[] | null> =
        new BehaviorSubject(null);
    private _locales: BehaviorSubject<ILocales[] | null> = new BehaviorSubject(
        null
    );
    private _states: BehaviorSubject<IStates[] | null> = new BehaviorSubject(null);
    private _cities: BehaviorSubject<ICities[] | null> = new BehaviorSubject(null);
    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient) { }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Getter for batchExam
     */
    get batchExams$(): Observable<IGetBatch[]> {
        return this._batchExams.asObservable();
    }
    get states$(): Observable<IStates[]> {
        return this._states.asObservable();
    }
    get cities$(): Observable<ICities[]> {
        return this._cities.asObservable();
    }
    get locales$(): Observable<ILocales[]> {
        return this._locales.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Get batchExam
     */
    getBatchExams(limit, page, paramsObj?): Observable<ApiResponse> {
        return this._httpClient
            .get<ApiResponse>(
                `${environment.apiBaseUrl}batches?isPaginate=true&limit=${limit}&page=${page}`, { params: paramsObj }
            )
            .pipe(
                tap((response) => {
                    this._batchExams.next(response.data['rows']);
                }),
                debounceTime(1000),
                distinctUntilChanged()
            );
    }
    getStates(): Observable<ApiResponse> {
        return this._httpClient
            .get<ApiResponse>(
                `${environment.apiBaseUrl}states`
            )
            .pipe(
                tap((response) => {
                    this._states.next(response.data);
                }),
                debounceTime(1000),
                distinctUntilChanged()
            );
    }
    // cities?state_id=92033f4c-da08-408d-9558-55074834bc98
    getCities(stateId): Observable<ApiResponse> {
        return this._httpClient
            .get<ApiResponse>(
                `${environment.apiBaseUrl}cities?state_id=${stateId}`
            )
            .pipe(
                tap((response) => {
                    this._cities.next(response.data);
                }),
                debounceTime(1000),
                distinctUntilChanged()
            );
    }

    /**
     * Create batchExam
     */
    createBatchExam(batchExam: IBatchExam): Observable<ApiResponse> {
        console.log({ batchExam });
        return this._httpClient.post<ApiResponse>(
            `${environment.apiBaseUrl}batches`,
            batchExam
        );
    }

    getBatchExamById(id: string, query?): Observable<IBatchExam> {
        if (query) {
            return this._httpClient.get<IBatchExam>(
                `${environment.apiBaseUrl}batches/${id}?type=${query}`
            );
        } else {
            return this._httpClient.get<IBatchExam>(
                `${environment.apiBaseUrl}batches/${id}`
            );
        }
    }
    getLocales(): Observable<ILocales[]> {
        let data: ILocales[];
        this._locales.subscribe((response) => {
            data = response;
        });
        if (!data) {
            return this._httpClient
                .get<ILocales[]>(`${environment.apiBaseUrl}locales`)
                .pipe(
                    tap(response => this._locales.next(response['data'])),
                );
        } else {
            return this.locales$;
        }
    }

    /**
     * Update batchExam
     *
     * @param id
     * @param product
     */
    updateBatchExam(id: string, assessor): Observable<ApiResponse> {
        delete assessor.id;
        return this._httpClient.put<ApiResponse>(
            `${environment.apiBaseUrl}batches/${id}`,
            assessor
        );
    }

    /**
     * Delete the accessor
     *
     * @param id
     */
    deleteBatchExam(id: string): Observable<ApiResponse> {
        console.log(id);
        // return;
        return this.batchExams$.pipe(
            take(1),
            switchMap(batch =>
                this._httpClient
                    .delete<ApiResponse>(
                        `${environment.apiBaseUrl}batches/${id}`
                    )
                    .pipe(
                        map((newBatch) => {
                            // Find the index of the deleted batch
                            const index = batch.findIndex(
                                item => item.id === id
                            );
                            // Delete the batch
                            batch.splice(index, 1);

                            // Update the batchs
                            this._batchExams.next(batch);

                            return newBatch;
                        })
                    )
            )
        );
    }

    downloadZip(id: string) {
        return this._httpClient.get<ApiResponse>(
            `${environment.apiBaseUrl}s3-zip?prefix=S3//${id}`
        );
    }
    downloadSheet(id: string) {
        return this._httpClient
            .get<ApiResponse>(
                `${environment.apiBaseUrl}batches/${id}/candidates-list`
            )
            .pipe(pluck('data'));
    }
    // Candidates
    createCandidates(id: string, candidates): Observable<ApiResponse> {
        return this._httpClient.post<ApiResponse>(
            `${environment.apiBaseUrl}batches/${id}/candidates`,
            candidates
        );
    }
    addQuestions(id: string): Observable<ApiResponse> {
        return this._httpClient.post<ApiResponse>(
            `${environment.apiBaseUrl}batches/${id}/questions`,
            {}
        );
    }
    resetQuestions(id: string): Observable<ApiResponse> {
        return this._httpClient.delete<ApiResponse>(
            `${environment.apiBaseUrl}batches/${id}/questions`,
            {}
        );
    }
    getQuestions(id: string): Observable<ApiResponse> {
        return this._httpClient.get<ApiResponse>(
            `${environment.apiBaseUrl}batches/${id}/questions`
        );
    }
    getCandidates(id: string): Observable<ApiResponse> {
        return this._httpClient.get<ApiResponse>(
            `${environment.apiBaseUrl}batches/${id}/candidates`
        );
    }
    getCandidatesFeedback(id: string): Observable<any> {
        return this._httpClient.get(`${environment.apiBaseUrl}batches/${id}/feedback`, { responseType: 'blob' });
      }

    getAttendance(id: string): Observable<any> {
        return this._httpClient.get(`${environment.apiBaseUrl}batches/${id}/candidateAttendence`, { responseType: 'blob' });
      }
    getTrainingAttendance(id: string): Observable<any> {
        return this._httpClient.get(`${environment.apiBaseUrl}batches/${id}/candidateTrainingAttendence`, { responseType: 'blob' });
      }
    getCandidateStatus(id: string): Observable<any> {
        return this._httpClient.get(`${environment.apiBaseUrl}batches/${id}/candidate`, { responseType: 'blob' });
      }
    getQuestion(id: string): Observable<any> {
        return this._httpClient.get(`${environment.apiBaseUrl}questions/jobrole/${id}/list`, { responseType: 'blob' });
    }
    postRating(id: string ,data): Observable<any> {
        return this._httpClient.post(`${environment.apiBaseUrl}assessors/${id}/reviews`,data);
    }
    getRating(id: string): Observable<any> {
        return this._httpClient.get(`${environment.apiBaseUrl}assessors/${id}/reviews`);
    }

    getCandidateResult(id: string): Observable<ApiResponse> {
        return this._httpClient.get<ApiResponse>(
            `${environment.apiBaseUrl}candidates/${id}/result-detail`
        );
    }
    getCandidatesDetails(id: string, query): Observable<ApiResponse> {
        return this._httpClient.get<ApiResponse>(
            `${environment.apiBaseUrl}candidates/${id}?type=${query}`
        );
    }
    getTwilioToken(id): Observable<ApiResponse> {
        return this._httpClient.get<ApiResponse>(
            `${environment.apiBaseUrl}twilio-video/${id}`
        );
    }
    resetCandidateExam(id: string): Observable<ApiResponse> {
        return this._httpClient.put<ApiResponse>(
            `${environment.apiBaseUrl}reset-candidate-submissions`,
            { id }
        );
    }

    submitCandidateExam(id?: any): Observable<ApiResponse> {
        return this._httpClient.get<ApiResponse>(
          `${environment.apiBaseUrl}candidate/${id}/theory/submit`,
        );
      }


    getFeedbackData(id: string): Observable<ApiResponse> {
        return this._httpClient.get<ApiResponse>(
            `${environment.apiBaseUrl}candidate/${id}/feedback`
        );
    }
    async getAllFiles(key, maxvalue?) {
        AWS.config.region = 'ap-south-1';
        AWS.config.credentials = new AWS.CognitoIdentityCredentials({
            IdentityPoolId: 'ap-south-1:1c32c674-c143-48bb-9484-2a97443b4176',
        });
        AWS.config.update({ correctClockSkew: true });
        const params = {
            apiVersion: '2006-03-01',
            region: 'ap-south-1',
            Key: `${environment.accessKeyId}`
        };
        const s3 = new AWS.S3(params);
        try {
            return await s3
                .listObjectsV2({
                    Bucket: 'iskill-development',
                    Prefix: key,
                    MaxKeys: maxvalue,
                })
                .promise();
        } catch (error) {
            console.log('cannot list objects from', error);
            return null;
        }
    }
    async deleteFileS3(key) {
        AWS.config.region = 'ap-south-1';
        AWS.config.credentials = new AWS.CognitoIdentityCredentials({
            IdentityPoolId: 'ap-south-1:1c32c674-c143-48bb-9484-2a97443b4176',
        });
        AWS.config.update({ correctClockSkew: true });
        const params = {
            apiVersion: '2006-03-01',
            region: 'ap-south-1',
        };
        const s3 = new AWS.S3(params);
        const listParams = {
            Bucket: 'iskill-development',
            Prefix: key,
        };

        console.log('deleteParams =>>', listParams);
        const listedObjects = await s3
            .listObjectsV2(listParams)
            .promise()
            .then((res) => {
                console.log(res);
                return res;
            })
            .catch((error) => {
                console.log(error);
                return null;
            });

        if (listedObjects.Contents.length === 0) {
            return;
        }

        const deleteParams = {
            Bucket: 'iskill-development',
            Delete: { Objects: [] },
        };
        console.log('deleteParams =>>', deleteParams);
        listedObjects.Contents.forEach(({ Key }) => {
            deleteParams.Delete.Objects.push({ Key });
        });
        try {
            return await s3
                .deleteObjects(deleteParams)
                .promise()
                .then((res) => {
                    console.log(res);
                })
                .catch((error) => {
                    console.log(error);
                });
        } catch (error) {
            console.log('cannot delete objects from s3', error);
            return null;
        }
    }
}
