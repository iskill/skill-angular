import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../view-batch/view-batch.component';

@Component({
  selector: 'app-batch-rating',
  templateUrl: './batch-rating.component.html',
  styleUrls: ['./batch-rating.component.scss']
})
export class BatchRatingComponent implements OnInit {
  data: {
    rating: number,
    review: string
  } = {
    rating: 0,
    review: ''
  };
  constructor(public dialogRef: MatDialogRef<BatchRatingComponent>,
    @Inject(MAT_DIALOG_DATA) public dataLog: DialogData) { }

  ngOnInit(): void {
    console.log("dailog data initialized" ,this.data)
  }
  setRating(rating: number) {
    if (this.data.rating === rating) {
      // Deselect the rating if it's already selected
      this.data.rating = 0;
    } else {
      this.data.rating = rating;
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
