import { NgModule } from '@angular/core';

import { Route, RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatRippleModule } from '@angular/material/core';
import { MatSortModule } from '@angular/material/sort';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatRadioModule } from '@angular/material/radio';
import { SharedModule } from 'app/shared/shared.module';
import { MatDividerModule } from '@angular/material/divider';
import { BatchExamComponent } from './batch-exam.component';
import { AddBatchComponent } from './add-batch/add-batch.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { ViewBatchComponent } from './view-batch/view-batch.component';
import { CandidatesComponent } from './candidates/candidates.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatChipsModule } from '@angular/material/chips';
import { CandidateDetailsComponent } from './candidate-details/candidate-details.component';
import { LazyLoadImageModule, LAZYLOAD_IMAGE_HOOKS, ScrollHooks } from 'ng-lazyload-image';
import { BatchRatingComponent } from './batch-rating/batch-rating.component';

const batchExamRoutes: Route[] = [
  { path: '', component: BatchExamComponent },
  { path: 'add-batch', component: AddBatchComponent },
  { path: 'update-batch/:id', component: AddBatchComponent },
  { path: 'view-batch/:id', component: ViewBatchComponent },
  { path: 'upload-candidate/:id', component: CandidatesComponent },
  { path: 'candidate-detail/:id', component: CandidateDetailsComponent },
];

@NgModule({
  declarations: [
    BatchExamComponent,
    AddBatchComponent,
    ViewBatchComponent,
    CandidatesComponent,
    CandidateDetailsComponent,
    BatchRatingComponent
  ],
  imports: [
    RouterModule.forChild(batchExamRoutes),
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatRippleModule,
    MatSortModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatTableModule,
    MatTooltipModule,
    MatRadioModule,
    MatDividerModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatTabsModule,
    MatChipsModule,
    SharedModule,
    LazyLoadImageModule
  ],
  providers: [
    { provide: LAZYLOAD_IMAGE_HOOKS, useClass: ScrollHooks }
  ]
})
export class BatchExamModule { }
