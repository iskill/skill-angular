/* eslint-disable @typescript-eslint/naming-convention */
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PasswordType } from 'app/shared/constant';
import { LoaderService } from 'app/shared/loader.service';
import { ToastrService } from 'ngx-toastr';
import * as XLSX from 'xlsx';
import { BatchExamService } from '../batch-exam.service';
import { ICandidates } from '../batch-exam.type';

@Component({
    selector: 'app-candidates',
    templateUrl: './candidates.component.html',
    styleUrls: ['./candidates.component.scss'],
})
export class CandidatesComponent implements OnInit {
    @ViewChild('myInput') myInputVariable: ElementRef;
    storeData: any;
    candidateData: ICandidates[];
    candidateDataLength: number;
    fileUploaded: File;
    worksheet: any;
    id: string;
    passwordType = PasswordType;
    password: string;
    batchData: any;
    data = [
        'Candidate_Id',
        'Candidate_First_Name',
        'Candidate_Last_Name',
        'Candidate_Father_Name',
        'Candidate_Email',
        'Candidate_Password',
        'Candidate_Mobile_Number',
        'Enrollment_Number',
    ];

    constructor(
        private _toastr: ToastrService,
        private _serviceBatch: BatchExamService,
        private _activatedRoute: ActivatedRoute,
        private _route: Router,
        private _loading: LoaderService
    ) { }

    ngOnInit() {
        this.id = this._activatedRoute.snapshot.paramMap.get('id');
        this._serviceBatch
            .getBatchExamById(this.id)
            .subscribe((response: any) => {
                this.password = response.data.exam_rule.passwordType;
                this.batchData = response.data;
            });
    }

    uploadedFile(event) {
        if (event.target.files.length > 0) {
            this.fileUploaded = event.target.files[0];
            this.readExcel();
        }
    }

    readExcel() {
        const readFile = new FileReader();
        readFile.onload = (e) => {
            this.storeData = readFile.result;
            const data = new Uint8Array(this.storeData);
            const arr = new Array();
            for (let i = 0; i !== data.length; ++i) {
                arr[i] = String.fromCharCode(data[i]);
            }
            const bstr = arr.join('');
            const workbook = XLSX.read(bstr, { type: 'binary' });
            const firstSheetName = workbook.SheetNames[0];
            this.worksheet = workbook.Sheets[firstSheetName];
        };
        readFile.readAsArrayBuffer(this.fileUploaded);
        setTimeout(() => {
            this.candidateData = XLSX.utils.sheet_to_json(this.worksheet, {
                raw: false,
            });
            this.candidateDataLength = this.candidateData.length;
            if (this.candidateDataLength === 0) {
                this._toastr.warning('File can\'t be empty', 'Error');
                this.onClickCancel();
            }
            if (this.candidateData.length > 0) {
                const objKeys = Object.keys(this.candidateData[0]);
                // To check is password
                const uniqueCandidateData = this.candidateData;
                this.candidateData.map((data: ICandidates) => {
                    const checkEmailUnique = uniqueCandidateData.filter(uniqueData => uniqueData.Candidate_Email === data.Candidate_Email);
                    const checkCandidateIdUnique = uniqueCandidateData.filter(uniqueData => uniqueData.Candidate_Id === data.Candidate_Id);
                    if (checkEmailUnique.length > 1) {
                        this._toastr.warning(`${checkEmailUnique[0].Candidate_Email} is not unique`, '', {
                            timeOut: 5000
                        });
                        this.onClickCancel();
                    }
                    if (checkCandidateIdUnique.length > 1) {
                        this._toastr.error(`${checkCandidateIdUnique[0].Candidate_Id} is not unique`, '', {
                            timeOut: 5000
                        });
                        this.onClickCancel();
                    }
                    // check is password is Auto
                    if (this.password === this.passwordType[0].viewValue) {
                        // data.Candidate_Password = '';
                        delete data.Candidate_Password;
                    }
                    // check is password is Manual
                    if (this.password === this.passwordType[1].viewValue) {
                        if (!data.Candidate_Password) {
                            if (data.Candidate_Password !== '') {
                                this._toastr.info(
                                    'Candidate_Password field can\'t be empty'
                                );
                                this.onClickCancel();
                            }
                        }
                    }
                });
                // To check if file format is proper
                objKeys.map((x) => {
                    if (!this.data.includes(x)) {
                        this._toastr.warning('File format is not proper');
                        this.onClickCancel();
                    }
                });
            }
        }, 500);
    }

    uploadCandidates() {
        this._loading.setLoading(true, 'questions');
        console.log('candidateData ==>', this.candidateData);
        const batch = { candidates: [] };
        this.candidateData.map((data) => {
            batch['candidates'].push({
                candidate_id: data.Candidate_Id,
                first_name: data.Candidate_First_Name,
                last_name: data.Candidate_Last_Name,
                father_name: data.Candidate_Father_Name,
                email: data.Candidate_Email,
                password: data.Candidate_Password,
                enrollment_no: data.Enrollment_Number,
                contact_no: data.Candidate_Mobile_Number,
            });
        });
        console.log('batch ==>', JSON.stringify(batch));

        this._serviceBatch.createCandidates(this.id, batch).subscribe(
            (response) => {
                if (response.status === 200) {
                    this._loading.setLoading(false, 'questions');
                    this._route.navigate(['batch-exam']);
                }
            },
            (error) => {
                this._loading.setLoading(false, 'questions');
                this._toastr.warning(error.error.message, '', { timeOut: 5000 });
            }
        );
    }
    reset() {
        this.myInputVariable.nativeElement.value = '';
    }
    onClickCancel() {
        this.candidateData = [];
        this.candidateDataLength = 0;
        this.storeData = '';
        this.reset();
        return false;
    }
}
