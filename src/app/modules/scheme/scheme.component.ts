import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    OnDestroy,
    OnInit,
    ViewEncapsulation,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, of, Subscription } from 'rxjs';
import { fuseAnimations } from '@fuse/animations';
import { SchemeService } from 'app/modules/scheme/scheme.service';
import { IScheme } from './scheme.types';
import { ConfirmationDailogComponent } from 'app/shared/component/confirmation-dailog/confirmation-dailog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
    selector: 'app-scheme',
    templateUrl: './scheme.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: fuseAnimations,
})
export class SchemeComponent implements OnInit, OnDestroy {

    schemes$: Observable<IScheme[]>;

    flashMessage: 'success' | 'error' | null = null;
    isLoading: boolean = false;
    schemeCount: number = 0;
    schemeTableColumns: string[] = ['name', 'createdDate', 'actions'];
    selectedScheme = null;
    selectedSchemeForm: FormGroup;
    count: number = 0;
    filterRecords = '';
    private _unsubscribeAll: Subscription;

    /**
     * Constructor
     */
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _formBuilder: FormBuilder,
        private _schemeService: SchemeService,
        private dialog: MatDialog
    ) { }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Create the selected scheme form
        this.selectedSchemeForm = this._formBuilder.group({
            id: [''],
            name: ['', [Validators.required]],
            createdAt: [''],
        });
        this.isLoading = true;
        this.schemes$ = this._schemeService.schemes$;
        this._unsubscribeAll = this._schemeService
            .getScheme()
            .subscribe((res) => {
                this.isLoading = false;

                // Update the counts
                this.schemeCount = res.length;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });
    }

    // Open delete confirmation dialog
    openDialog(id) {
        const dialogRef = this.dialog.open(ConfirmationDailogComponent,{
            data: {
                message: 'Are you sure want to delete?',
                buttonText: {
                    ok: 'Yes',
                    cancel: 'No'
                }
            }
        });
        dialogRef.afterClosed().subscribe((confirmed: boolean) => {
            if (confirmed) {
                this.deleteSelectedScheme(id);
                const a = document.createElement('a');
                a.click();
                a.remove();
            }
        });
    }
    // Open delete confirmation dialog end

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._schemeService
                .createSchemeForm({ id: 'new' }, 'remove')
                .subscribe();
        this._unsubscribeAll.unsubscribe();
        this.selectedSchemeForm.reset();
        this.selectedScheme = null;
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle product details
     *
     * @param productId
     */
    toggleDetails(scheme: IScheme): void {
        this.selectedScheme = scheme;
        if (scheme.id === 'new') {
            // to remove 'new' id for add scheme
            this._schemeService
                .createSchemeForm({ id: 'new' }, 'remove')
                .subscribe();
            this._changeDetectorRef.markForCheck();
            this.closeDetails();
            return;
        }
        if (
            this.selectedScheme &&
            this.selectedScheme.id === scheme.id &&
            this.count === 0
        ) {
            // set value in form for update
            this.selectedSchemeForm.patchValue(scheme);
            this.count++;
            this._changeDetectorRef.markForCheck();
        } else if (this.count === 1) {
            // to close for update
            // Close the details
            this.count--;
            this.selectedScheme = null;
            this._changeDetectorRef.markForCheck();
            this.closeDetails();
            return;
        }
    }

    /**
     * Filter schemes
     */
    filterSearch(event) {
        const value = event.target.value.toLowerCase();
        this._schemeService.schemes$.subscribe((res) => {
            this.schemes$ = of(res.filter(scheme => scheme.name.toLocaleLowerCase().includes(value)));
        });
    }

    /**
     * Close the details
     */
    closeDetails(): void {
        this.selectedScheme = null;
    }

    /**
     * Create product
     */
    createScheme(): void {
        this._schemeService
            .createSchemeForm({ id: 'new' }, 'add')
            .subscribe((response) => {
                this.schemes$ = this._schemeService.schemes$;
                this.selectedScheme = { id: 'new' };
                this.selectedSchemeForm.reset();
                // Mark for check
                this._changeDetectorRef.markForCheck();
            });
    }

    /**
     * Update the selected product using the form mock-api
     */
    addUpdateSelectedScheme(): void {
        if (this.selectedSchemeForm.valid) {
            const scheme = this.selectedSchemeForm.getRawValue();
            if (this.selectedScheme?.id === 'new') {
                this._schemeService
                    .createSchemeForm({ id: 'new' }, 'remove')
                    .subscribe();
                this._schemeService.createScheme(scheme.name).subscribe((res) => {
                    if (res['status'] === 200) {
                        this.showFlashMessage('success');
                        this.selectedScheme = null;
                        this.selectedSchemeForm.reset();
                    }
                });
            } else {
                this._schemeService
                    .updateScheme(scheme.id, scheme)
                    .subscribe((res) => {
                        if (res['status'] === 200) {
                            this.showFlashMessage('success');
                            this.selectedScheme = null;
                            this.selectedSchemeForm.reset();
                        }
                    });
            }
        }
    }

    /**
     * Delete the selected scheme
     */
    deleteSelectedScheme(scheme): void {
        // Delete the scheme on the server
        this._schemeService.deleteScheme(scheme.id).subscribe();
        this.selectedSchemeForm.reset();
    }

    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'error'): void {
        // Show the message
        this.flashMessage = type;

        // Mark for check
        this._changeDetectorRef.markForCheck();

        // Hide it after 1.5 seconds
        setTimeout(() => {
            this.flashMessage = null;
            this.selectedScheme = null;
            // Mark for check
            this._changeDetectorRef.markForCheck();
        }, 1500);
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any {
        return item.id || index;
    }
    filterDataChanged(value) {
        if (value === 'true') {
            console.log('True => ', value);
            this._schemeService.filterRecord(value).subscribe((res) => {
                console.log(res);
            });
            return false;
        }
        this._schemeService.filterRecord(false).subscribe((res) => {
            console.log(res);
        });
    }
}
