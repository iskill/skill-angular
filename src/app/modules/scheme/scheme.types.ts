export interface IScheme{
    id: string;
    name: string;
}

export interface ApiResponse{
    data: [];
    message: string;
    status: number;
}
