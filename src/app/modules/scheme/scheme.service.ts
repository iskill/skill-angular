import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, pluck, switchMap, take, tap } from 'rxjs/operators';
import { IScheme, ApiResponse } from 'app/modules/scheme/scheme.types';
import { environment } from 'environments/environment';

@Injectable({
    providedIn: 'root'
})
export class SchemeService {
    // Private
    private _schemes: BehaviorSubject<IScheme[] | null> = new BehaviorSubject(null);

    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Getter for Scheme
     */
    get schemes$(): Observable<IScheme[]> {
        return this._schemes.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Create scheme form
     */
    createSchemeForm(addNewScheme, type): Observable<IScheme[]> {
        let schemes: IScheme[];
        this.schemes$.subscribe((res) => {
            schemes = res;
        });
        if (!schemes.some(d => d.id === 'new')) {
            if (type === 'add') {
                // Add new scheme
                this._schemes.next([addNewScheme, ...schemes]);
            }
        }
        if (type === 'remove') {
            const index = schemes.findIndex(item => item.id === 'new');
            // Delete the scheme
            if (index === 0) {
                schemes.splice(index, 1);
            }
            this._schemes.next(schemes);
        }
        // Return the new scheme
        return of(schemes);
    }

    /**
     * Get scheme
     */
    getScheme(): Observable<IScheme[]> {
        let data: IScheme[];
        this._schemes.subscribe((response) => {
            data = response;
        });
        if (!data) {
            return this._httpClient.get<IScheme[]>(`${environment.apiBaseUrl}schemes?isSoftDeleted=false`).pipe(
                tap((response) => {
                    this._schemes.next(response['data']);
                }),
            );
        } else {
            return this.schemes$;
        }
    }

    /**
     * Create scheme
     */
    createScheme(name: string): Observable<IScheme> {
        return this.schemes$.pipe(
            take(1),
            switchMap(schemes => this._httpClient.post<IScheme>(`${environment.apiBaseUrl}schemes`, { name: name }).pipe(
                map((newScheme) => {

                    // Update the Schemes with the new scheme
                    this._schemes.next([newScheme['data'], ...schemes]);

                    // Return the new scheme
                    return newScheme;
                })
            ))
        );
    }

    /**
     * Update scheme
     *
     */
    updateScheme(id: string, scheme: IScheme): Observable<IScheme> {
        return this.schemes$.pipe(
            take(1),
            switchMap(schemes => this._httpClient.put<IScheme>(`${environment.apiBaseUrl}schemes/${id}`, { name: scheme.name }).pipe(
                map((updatedScheme) => {

                    // Find the index of the updated scheme
                    const index = schemes.findIndex(item => item.id === id);

                    // Update the scheme
                    schemes[index] = scheme;

                    // Update the schemes
                    this._schemes.next(schemes);

                    // Return the updated scheme
                    return updatedScheme;
                })
            ))
        );
    }

    /**
     * Delete the scheme
     */
    deleteScheme(id: string): Observable<ApiResponse> {
        return this.schemes$.pipe(
            take(1),
            switchMap(schemes => this._httpClient.delete<ApiResponse>(`${environment.apiBaseUrl}schemes/${id}`).pipe(
                map((scheme) => {
                    // Find the index of the deleted scheme
                    const index = schemes.findIndex(item => item.id === id);

                    // Delete the scheme
                    schemes.splice(index, 1);

                    // Update the schemes
                    this._schemes.next(schemes);

                    return scheme;
                })
            ))
        );
    }

    filterRecord(type) {
        return this._httpClient.get<IScheme[]>(`${environment.apiBaseUrl}schemes?isSoftDeleted=${type}`).pipe(
            tap((response) => {
                console.log(response);
                this._schemes.next(response['data']);
            }),
            pluck('data')
        );
    }
}
