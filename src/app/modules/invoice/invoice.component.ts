/* eslint-disable @typescript-eslint/naming-convention */
import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { fromEvent, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { IAssessor } from '../assessor/assessor.type';
import { BatchExamService } from '../batch-exam/batch-exam.service';
import { IBatchExam, IGetBatch } from '../batch-exam/batch-exam.type';

@Component({
    selector: 'app-invoice',
    templateUrl: './invoice.component.html',
    styleUrls: ['./invoice.component.scss'],
})
export class InvoiceComponent implements OnInit, AfterViewInit {
    @ViewChild('searchFilter') searchFilter: ElementRef;
    isLoading = false;
    batchExam$: Observable<IGetBatch[]>;

    batchExamTableColumns: string[] = [
        'name',
        'assessorPaid',
        'assessorPaidDate',
        'govPaid',
        'govPaidDate',
        'resultPaid',
        'resultPaidDate',
        'actions',
    ];
    batchExamCount: number = 0;
    showInvoiceModal: boolean = false;
    showExamModal: boolean = false;
    assessorSelect: any;
    StartDate: any;
    EndDate: any;
    // Pagination
    pageSize = 25;
    pageIndex = 0;
    pageSizeOptions = [25, 50, 100, 200];
    showFirstLastButtons = true;
    // Pagination end
    // Modal data
    selectedBatch: any;
    isAssessorPaid: any;
    isGovtPaid: any;
    isResultPaid: any;
    // Modal data end
    filterObj = {};
    dropdownFilter: any;

    constructor(
        private _bathcExamService: BatchExamService,
        private _changeDetectorRef: ChangeDetectorRef
    ) { }

    ngOnInit(): void {
        this.batchExam$ = this._bathcExamService.batchExams$;
        this.isLoading = true;
        this.getBatchExamsData();
    }
    ngAfterViewInit() {
        const searchTerm2 = fromEvent<any>(this.searchFilter.nativeElement, 'keyup');
        searchTerm2.pipe(
            map(event => event.target.value),
            debounceTime(600),
            distinctUntilChanged()
        ).subscribe((res) => {
            this.filterObj['q'] = res;
            this.getBatchExamsData();
        });
    }
    getBatchExamsData() {
        this._bathcExamService
            .getBatchExams(this.pageSize, this.pageIndex, this.filterObj)
            .subscribe((res) => {
                this.isLoading = false;
                // Update the counts
                this.batchExamCount = res.data['count'];
                // Mark for check
                this._changeDetectorRef.markForCheck();
            });
    }

    handlePageEvent(event: PageEvent) {
        this.batchExamCount = event.length;
        this.pageSize = event.pageSize;
        this.pageIndex = event.pageIndex;
        this.getBatchExamsData();
    }

    selectFilter(eventValue) {
        eventValue.map(data => this.filterObj[data] = true);
        for (const key in this.filterObj) {
            if (key && !eventValue.includes(key) && ['isAssessorPaid', 'isGovtPaid', 'isResultPaid',].includes(key)) {
                delete this.filterObj[key];
            }
        }
        this.getBatchExamsData();
    }

    showModal(batch) {
        this.showInvoiceModal = !this.showInvoiceModal;
        this.selectedBatch = batch;
        this.isAssessorPaid = batch.is_assessor_paid;
        this.isGovtPaid = batch.is_govt_paid;
        this.isResultPaid = batch.is_result_paid;
    }

    updateInvoice() {
        const updateData = {};
        this.isAssessorPaid = +this.isAssessorPaid === 0 ? false : true;
        this.isGovtPaid = +this.isGovtPaid === 0 ? false : true;
        this.isResultPaid = +this.isResultPaid === 0 ? false : true;
        if (this.isAssessorPaid) {
            updateData['is_assessor_paid'] = this.isAssessorPaid;
            updateData['assessor_paid_date'] = new Date().toISOString();
        }
        if (!this.isAssessorPaid) {
            updateData['is_assessor_paid'] = this.isAssessorPaid;
            updateData['assessor_paid_date'] = '';
        }
        if (this.isGovtPaid) {
            updateData['is_govt_paid'] = this.isGovtPaid;
            updateData['govt_paid_date'] = new Date().toISOString();
        }
        if (!this.isGovtPaid) {
            updateData['is_govt_paid'] = this.isGovtPaid;
            updateData['govt_paid_date'] = '';
        }
        if (this.isResultPaid) {
            updateData['is_result_paid'] = this.isResultPaid;
            updateData['result_paid_date'] = new Date().toISOString();
        }
        if (!this.isResultPaid) {
            updateData['is_result_paid'] = this.isResultPaid;
            updateData['result_paid_date'] = '';
        }
        this._bathcExamService
            .updateBatchExam(this.selectedBatch.id, updateData)
            .subscribe((res) => {
                this.getBatchExamsData();
                this.closeModal();
            });
    }
    dateRangeChange(dateRangeStart: HTMLInputElement, dateRangeEnd: HTMLInputElement) {
        if (dateRangeStart.value && dateRangeEnd.value) {
            this.filterObj['startDate'] = (new Date(dateRangeStart.value).toLocaleDateString().split('/').reverse().join('-'));
            this.filterObj['endDate'] = new Date(dateRangeEnd.value).toLocaleDateString().split('/').reverse().join('-');
            this.getBatchExamsData();
        }
    }

    closeModal() {
        this.showInvoiceModal = false;
    }

    trackByFn(index: number, item: any): any {
        return item.id || index;
    }
}
