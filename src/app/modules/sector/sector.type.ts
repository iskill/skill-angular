export interface ISector {
    id: string;
    name: string;
}
