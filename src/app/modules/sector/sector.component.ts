import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { fuseAnimations } from '@fuse/animations';
import { ConfirmationDailogComponent } from 'app/shared/component/confirmation-dailog/confirmation-dailog.component';
import { Observable, of, Subscription } from 'rxjs';
import { SectorService } from './sector.service';
import { ISector } from './sector.type';

@Component({
  selector: 'app-sector',
  templateUrl: './sector.component.html',
  styleUrls: ['./sector.component.scss'],
  animations     : fuseAnimations
})
export class SectorComponent implements OnInit, OnDestroy {

    sectors$: Observable<ISector[]>;
    flashMessage: 'success' | 'error' | null = null;
    isLoading: boolean = false;
    sectorTableColumns: string[] = ['name', 'createdDate', 'actions'];
    sectorCount: number = 0;
    selectedSector = null;
    selectedSectorForm: FormGroup;
    count: number = 0;
    filterRecords = '';
    private _unsubscribeAll: Subscription;

    /**
     * Constructor
     */
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _formBuilder: FormBuilder,
        private _sectorService: SectorService,
        private dialog: MatDialog
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the selected product form
        this.selectedSectorForm = this._formBuilder.group({
            id: [''],
            name: ['', [Validators.required]],
            createdAt: [''],
            updatedAt: ['']
        });
        this.isLoading = true;
        this.sectors$ = this._sectorService.sectors$;
        this._unsubscribeAll = this._sectorService.getSector().subscribe((res) => {
            this.isLoading = false;
            // Update the counts
            this.sectorCount = res.length;
            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
    }

    // Open delete confirmation dialog
    openDialog(id) {
        const dialogRef = this.dialog.open(ConfirmationDailogComponent,{
            data: {
                message: 'Are you sure want to delete?',
                buttonText: {
                    ok: 'Yes',
                    cancel: 'No'
                }
            }
        });
        dialogRef.afterClosed().subscribe((confirmed: boolean) => {
            if (confirmed) {
                this.deleteselectedSector(id);
                const a = document.createElement('a');
                a.click();
                a.remove();
            }
        });
    }
    // Open delete confirmation dialog end

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._sectorService
                .createSectorsForm({ id: 'new' }, 'remove')
                .subscribe();
        this._unsubscribeAll.unsubscribe();
        this.selectedSectorForm.reset();
        this.selectedSector = null;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle product details
     */
    toggleDetails(sector: ISector): void
    {
        this.selectedSector = sector;
        if (sector.id === 'new') { // to remove 'new' id for add sector
            this._sectorService.createSectorsForm({ id: 'new' }, 'remove').subscribe();
            this._changeDetectorRef.markForCheck();
            this.closeDetails();
            return;
        }
        if (this.selectedSector && this.selectedSector.id === sector.id && this.count === 0){
            // set value in form for update
            this.selectedSectorForm.patchValue(sector);
            this.count++;
            this._changeDetectorRef.markForCheck();
        } else if (this.count === 1) { // to close for update
            // Close the details
            this.count--;
            this.selectedSector = null;
            this._changeDetectorRef.markForCheck();
            this.closeDetails();
            return;
        }
    }

    filterSearch(event){
        const value = event.target.value.toLowerCase();
        this._sectorService.sectors$.subscribe((res) => {
            this.sectors$ = of(res.filter(item => item.name.toLocaleLowerCase().includes(value)));
        });
    }

    /**
     * Close the details
     */
    closeDetails(): void
    {
        this.selectedSector = null;
    }

    /**
     * Create sector
     */
    createSector(): void
    {
        this._sectorService.createSectorsForm({ id: 'new' },'add').subscribe((response) => {
            this.sectors$ = this._sectorService.sectors$;
            this.selectedSector = { id: 'new' };
            this.selectedSectorForm.reset();
            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
    }

    /**
     * Update the selected sector
     */
    addUpdateSelectedSector(): void
    {
        if (this.selectedSectorForm.valid) {
            // Get the sector object
            const sector = this.selectedSectorForm.getRawValue();
            if (this.selectedSector?.id === 'new') {
                this._sectorService.createSectorsForm({ id: 'new' }, 'remove').subscribe();
                this._sectorService.createSector(sector.name).subscribe((res) => {
                    if (res['status'] === 200) {
                        this.showFlashMessage('success');
                        this.selectedSectorForm.reset();
                        this.selectedSector = null;
                    }
                });
            } else {
                this._sectorService.updateSector(sector.id, sector).subscribe((res) => {
                    if (res['status'] === 200){
                        this.showFlashMessage('success');
                        this.selectedSectorForm.reset();
                        this.selectedSector = null;
                    }
                });
            }
        }
    }

    /**
     * Delete the sector
     */
    deleteselectedSector(sector): void
    {
        // Delete the sector on the server
        this._sectorService.deleteSector(sector.id).subscribe((res) => {
            if (res.status === 200) {
                this.showFlashMessage('success');
                this.selectedSectorForm.reset();
                this.selectedSector = null;
            }
        });
    }

    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'error'): void
    {
        // Show the message
        this.flashMessage = type;

        // Mark for check
        this._changeDetectorRef.markForCheck();

        // Hide it after 1.5 seconds
        setTimeout(() => {

            this.flashMessage = null;
            this.selectedSector = {};
            // Mark for check
            this._changeDetectorRef.markForCheck();
        }, 1500);
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any
    {
        return item.id || index;
    }
    filterDataChanged(value) {
        if (value === 'true') {
            console.log('True => ', value);
            this._sectorService.filterRecord(value).subscribe((res) => {
                console.log(res);
            });
            return false;
        }
        this._sectorService.filterRecord(false).subscribe((res) => {
            console.log(res);
        });
    }
}
