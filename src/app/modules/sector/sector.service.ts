import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, pluck, switchMap, take, tap } from 'rxjs/operators';
import { ApiResponse } from 'app/modules/scheme/scheme.types';
import { environment } from 'environments/environment';
import { ISector } from './sector.type';

@Injectable({
    providedIn: 'root'
})
export class SectorService
{
    private _sectors: BehaviorSubject<ISector[] | null> = new BehaviorSubject(null);
    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Getter for sectors
     */
    get sectors$(): Observable<ISector[]>
    {
        return this._sectors.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Get sector
     */
    getSector(option?): Observable<ISector[]>
    {
        let data: ISector[];
        let query = 'sectors?isSoftDeleted=false'
        if(option){
            query = query+`&sector_id=${option}`
        }else{
            this._sectors.subscribe((response) => {
                data = response;
            });
        }
        
        if (!data) {
            return this._httpClient.get<ISector[]>(`${environment.apiBaseUrl}${query}`).pipe(
                tap((res) => {
                    this._sectors.next(res['data']);
                }),
            );
        } else {
            return this.sectors$;
        }
    }

    createSectorsForm(addNewScheme, type): Observable<ISector[]>{
        let sectors: ISector[];
        this.sectors$.subscribe((res) => {
            sectors = res;
        });
        if (!sectors.some(d => d.id === 'new')) {
            if (type === 'add') {
                // Add new scheme
                this._sectors.next([addNewScheme, ...sectors]);
            }
        }
        if (type === 'remove') {
            const index = sectors.findIndex(item => item.id === 'new');
            // Delete the scheme
            if (index === 0) {
                sectors.splice(index, 1);
            }
            this._sectors.next(sectors);
        }
        // Return the new scheme
        return of(sectors);
    }

    /**
     * Create sector
     */
    createSector(name: string): Observable<ISector>
    {
        return this.sectors$.pipe(
            take(1),
            switchMap(sectors => this._httpClient.post<ISector>(`${environment.apiBaseUrl}sectors`, {name:name}).pipe(
                map((newSectors) => {

                    // Update the Sectors with the new scheme
                    this._sectors.next([newSectors['data'], ...sectors]);

                    // Return the new sectors
                    return newSectors;
                })
            ))
        );
    }

    /**
     * Update sector
     */
    updateSector(id: string, sector: ISector): Observable<ISector>
    {
        return this.sectors$.pipe(
            take(1),
            switchMap(sectors => this._httpClient.put<ISector>(`${environment.apiBaseUrl}sectors/${id}`, {name :sector.name}).pipe(
                map((updatedScheme) => {

                    // Find the index of the updated scheme
                    const index = sectors.findIndex(item => item.id === id);

                    // Update the sector
                    sectors[index] = sector;

                    // Update the sectors
                    this._sectors.next(sectors);

                    // Return the updated scheme
                    return updatedScheme;
                })
            ))
        );
    }

    /**
     * Delete the sector
     */
    deleteSector(id: string): Observable<ApiResponse>
    {
        return this.sectors$.pipe(
            take(1),
            switchMap(sectors => this._httpClient.delete<ApiResponse>(`${environment.apiBaseUrl}sectors/${id}`).pipe(
                map((sector) => {
                    // Find the index of the deleted sector
                    const index = sectors.findIndex(item => item.id === id);

                    // Delete the sector
                    sectors.splice(index, 1);

                    // Update the sectors
                    this._sectors.next(sectors);

                    return sector;
                })
            ))
        );
    }
    filterRecord(type) {
        return this._httpClient.get<ISector[]>(`${environment.apiBaseUrl}sectors?isSoftDeleted=${type}`).pipe(
            tap((response) => {
                console.log(response);
                this._sectors.next(response['data']);
            }),
            pluck('data')
        );
    }
}
