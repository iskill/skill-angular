import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, pluck, switchMap, take, tap } from 'rxjs/operators';
import { ApiResponse } from '../scheme/scheme.types';
import { SectorService } from '../sector/sector.service';
import { IJobRole } from './job-role.types';

@Injectable({
    providedIn: 'root'
})
export class JobRoleService {

    // Private
    private _jobRoles: BehaviorSubject<IJobRole[] | null> = new BehaviorSubject(null);
    private _nos: BehaviorSubject<IJobRole[] | null> = new BehaviorSubject(null);
    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient,private _sectorService: SectorService) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Getter for JobRoles
     */
    get jobRoles$(): Observable<IJobRole[]> {
        return this._jobRoles.asObservable();
    }
    get nos$(): Observable<IJobRole[]> {
        return this._nos.asObservable();
    }


    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get JobRole
     */
    getJobRole(sectorId?): Observable<IJobRole[]> {
        let data: IJobRole[];
        let query = 'job-roles?isSoftDeleted=false'
        if(sectorId){
            query = query+`&sector_id=${sectorId}`
        }else{
            this._jobRoles.subscribe((response) => {
                data = response;
            });
        }
        if (!data) {
            
            return this._httpClient.get<IJobRole[]>(`${environment.apiBaseUrl}${query}`).pipe(
            tap((response) => {
                this._jobRoles.next(response['data']);
            }),
        );
        } else {
            return this.jobRoles$;
        }
    }
    createJobRoleForm(addNewJob, type): Observable<IJobRole[]> {
        let job: IJobRole[];
        this.jobRoles$.subscribe((res) => {
            job = res;
        });
        if (!job.some(d => d.id === 'new')) {
            if (type === 'add') {
                // Add new jobRole
                this._jobRoles.next([addNewJob, ...job]);
            }
        }
        if (type === 'remove') {
            const index = job.findIndex(item => item.id === 'new');
            // Delete the jobRole
            if (index === 0) {
                job.splice(index, 1);
            }
            this._jobRoles.next(job);
        }
        // Return the new JobRole
        return of(job);
    }

    /**
     * Create JobRole
     */
    createJobRole(job: IJobRole): Observable<IJobRole> {
        return this.jobRoles$.pipe(
            take(1),
            // eslint-disable-next-line @typescript-eslint/naming-convention
            switchMap(jobRoles => this._httpClient.post<IJobRole>(`${environment.apiBaseUrl}job-roles`, { name: job.name, sector_id: job['sector_id'] }).pipe(
                map((newJob) => {
                    newJob['data']['sector'] = job['sector'];

                    // Update the JobRoles with the new scheme
                    this._jobRoles.next([newJob['data'], ...jobRoles]);

                    // Return the new newJob
                    return newJob;
                })
            ))
        );
    }

    /**
     * Update JobRole
     */
    updateJobRole(id: string, jobRole: IJobRole): Observable<IJobRole> {
        console.log(jobRole);
        return this.jobRoles$.pipe(
            take(1),
            // eslint-disable-next-line @typescript-eslint/naming-convention
            switchMap(jobRoles => this._httpClient.put<IJobRole>(`${environment.apiBaseUrl}job-roles/${id}`, { name: jobRole.name, sector_id: jobRole['sector_id'] }).pipe(
                tap((updatedjobRole) => {

                    // Find the index of the updated JobRole
                    const index = jobRoles.findIndex(item => item.id === id);
                    this._sectorService.sectors$.subscribe((res) => {
                        jobRole['sector'] = res.find(sector => sector.id === jobRole['sector_id']);
                    });
                    console.log(jobRole);
                    // Update the sector
                    jobRoles[index] = jobRole;

                    // Update the jobRoles
                    this._jobRoles.next(jobRoles);

                    // Return the updated JobRole
                    return updatedjobRole;
                })
            ))
        );
    }

    /**
     * Delete the JobRole
     *
     * @param id
     */
    deleteJobRole(id: string): Observable<ApiResponse> {
        return this.jobRoles$.pipe(
            take(1),
            switchMap(jobRoles => this._httpClient.delete<ApiResponse>(`${environment.apiBaseUrl}job-roles/${id}`).pipe(
                map((newJob) => {
                    // Find the index of the deleted JobRole
                    const index = jobRoles.findIndex(item => item.id === id);

                    // Delete the sector
                    jobRoles.splice(index, 1);

                    // Update the sectors
                    this._jobRoles.next(jobRoles);

                    return newJob;
                })
            ))
        );
    }
    filterRecord(type) {
        return this._httpClient.get<IJobRole[]>(`${environment.apiBaseUrl}job-roles?isSoftDeleted=${type}`).pipe(
            tap((response) => {
                console.log(response);
                this._jobRoles.next(response['data']);
            }),
            pluck('data')
        );
    }
    filterJobRole(jobroleId?) {
        return this._httpClient.get<IJobRole[]>(`${environment.apiBaseUrl}job-roles?jobrole_id=${jobroleId}`).pipe(
            tap((response) => {
                console.log(response);
                this._jobRoles.next(response['data']);
            }),
            pluck('data')
        );
    }

    // Pos functions
    getPos(): Observable<IJobRole[]> {
        let data: IJobRole[];
        this._nos.subscribe((response) => {
            data = response;
        });
        if (!data) {
            return this._httpClient.get<IJobRole[]>(`${environment.apiBaseUrl}nos`).pipe(
            tap((response) => {
                this._nos.next(response['data']);
            }),
        );
        } else {
            return this.nos$;
        }
    }
    getPosById(id): Observable<IJobRole[]> {
        return this._httpClient.get<IJobRole[]>(`${environment.apiBaseUrl}nos/${id}`);
    }
    createPosForm(addNewJob, type){
        let job: IJobRole[];
        this.nos$.subscribe((res) => {
            job = res;
        });
        if (!job.some(d => d.id === 'new')) {
            if (type === 'add') {
                // Add new jobRole
                this._nos.next([addNewJob, ...job]);
            }
        }
        if (type === 'remove') {
            const index = job.findIndex(item => item.id === 'new');
            // Delete the jobRole
            if (index === 0) {
                job.splice(index, 1);
            }
            this._nos.next(job);
        }
        // Return the new JobRole
        return of(job);
    }
    createPos(payload: IJobRole): Observable<IJobRole> {
        return this.nos$.pipe(
            take(1),
            // eslint-disable-next-line @typescript-eslint/naming-convention
            switchMap(jobRoles => this._httpClient.post<IJobRole>(`${environment.apiBaseUrl}nos`, { name: payload.name, job_id: payload['job_id'] }).pipe(
                map((newJob) => {
                    // newJob['data']['sector'] = job['sector'];
                    newJob['data']['job_role'] = payload['job_role'];

                    // Update the JobRoles with the new scheme
                    this._nos.next([newJob['data'], ...jobRoles]);

                    // Return the new newJob
                    return newJob;
                })
            ))
        );
    }
    updatePos(id: string, jobRole: IJobRole): Observable<IJobRole> {
        console.log(jobRole);
        return this.nos$.pipe(
            take(1),
            // eslint-disable-next-line @typescript-eslint/naming-convention
            switchMap(jobRoles => this._httpClient.put<IJobRole>(`${environment.apiBaseUrl}nos/${id}`, { name: jobRole.name, job_id: jobRole['job_id'] }).pipe(
                tap((updatedjobRole) => {

                    // Find the index of the updated JobRole
                    const index = jobRoles.findIndex(item => item.id === id);
                    this.jobRoles$.subscribe((res) => {
                        console.log('jobRole',jobRole);
                        jobRole['job_role'] = res.find(item => item.id === jobRole['job_id']);
                    });
                    console.log(jobRole);
                    // Update the sector
                    jobRoles[index] = jobRole;

                    // Update the jobRoles
                    this._nos.next(jobRoles);

                    // Return the updated JobRole
                    return updatedjobRole;
                })
            ))
        );
    }

    /**
     * Delete the JobRole
     *
     * @param id
     */
    deletePos(id: string): Observable<ApiResponse> {
        return this.nos$.pipe(
            take(1),
            switchMap(jobRoles => this._httpClient.delete<ApiResponse>(`${environment.apiBaseUrl}nos/${id}`).pipe(
                map((newJob) => {
                    // Find the index of the deleted JobRole
                    const index = jobRoles.findIndex(item => item.id === id);

                    // Delete the sector
                    jobRoles.splice(index, 1);

                    // Update the sectors
                    this._nos.next(jobRoles);

                    return newJob;
                })
            ))
        );
    }
    filterNosRecord(type) {
        return this._httpClient.get<IJobRole[]>(`${environment.apiBaseUrl}job-roles?isSoftDeleted=${type}`).pipe(
            tap((response) => {
                console.log(response);
                this._nos.next(response['data']);
            }),
            pluck('data')
        );
    }
    // Nos functions end

}
