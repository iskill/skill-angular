import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  ViewEncapsulation
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSelect } from '@angular/material/select';
import { fuseAnimations } from '@fuse/animations';
import { ConfirmationDailogComponent } from 'app/shared/component/confirmation-dailog/confirmation-dailog.component';
import { Observable, of, ReplaySubject, Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SectorService } from '../../sector/sector.service';
import { JobRoleService } from '../job-role.service';
import { IJobRole } from '../job-role.types';
@Component({
  selector: 'app-nos',
  templateUrl: './nos.component.html',
  styleUrls: ['./nos.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: fuseAnimations,
})
export class NosComponent implements OnInit, OnDestroy {

  // Dropdown Search
  @ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;
  public inputFilterCtrl: FormControl = new FormControl();
  public jobRoles: ReplaySubject<IJobRole[]> = new ReplaySubject<IJobRole[]>(1);
  _onDestroy = new Subject<void>();
  // Dropdown Search end

  nos$: Observable<IJobRole[]>;
  jobRole$: Observable<any>;
  jobRole: any;
  flashMessage: 'success' | 'error' | null = null;
  isLoading: boolean = false;
  jobRoleTableColumns: string[] = ['name', 'job', 'createdDate', 'actions'];
  selectedJobRole = null;
  jobRoleCount: number = 0;
  selectedJobRoleForm: FormGroup;
  count: number = 0;
  filterRecords = '';
  private _unsubscribeAll: Subscription;

  /**
   * Constructor
   */
  constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _nosService: JobRoleService,
    private _sectorService: SectorService,
    private dialog: MatDialog
  ) { }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.selectedJobRoleForm = this._formBuilder.group({
      name: ['', [Validators.required]],
      // eslint-disable-next-line @typescript-eslint/naming-convention
      job_id: ['', [Validators.required]],
      createdAt: [''],
      jobRole: [''],
      updatedAt: [''],
    });

    this._nosService.getJobRole().subscribe((res: any) => {
      const response = res.length > 0 ? res : res.data;
      this.jobRole$ = of(response);
      this.jobRole = response;
      this.jobRoles.next(response);
    });
    this.nos$ = this._nosService.nos$;
    this.nos$.subscribe({
      next: (res) => {
        console.log('this.nos$',res);
      }
    });
    this.isLoading = true;
    this._unsubscribeAll = this._nosService
      .getPos()
      .subscribe((res) => {
        this.isLoading = false;
        // Update the counts
        this.jobRoleCount = res.length;
        // Mark for check
        this._changeDetectorRef.markForCheck();
      });
    this.inputFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.sectorFilterSearch();
      });
  }
  // Open delete confirmation dialog
  openDialog(id) {
    const dialogRef = this.dialog.open(ConfirmationDailogComponent, {
      data: {
        message: 'Are you sure want to delete?',
        buttonText: {
          ok: 'Yes',
          cancel: 'No'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.deleteSelectedJobRole(id);
        const a = document.createElement('a');
        a.click();
        a.remove();
      }
    });
  }
  // Open delete confirmation dialog end

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.unsubscribe();
    this._nosService
      .createPosForm({ id: 'new' }, 'remove')
      .subscribe();
    this.selectedJobRoleForm.reset();
    this.selectedJobRole = null;
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Toggle product details
   *
   * @param productId
   */
  toggleDetails(job: IJobRole): void {
    this.selectedJobRole = job;
    if (job.id === 'new') {
      // to remove 'new' id for add jobRole
      this._nosService
        .createPosForm({ id: 'new' }, 'remove')
        .subscribe();
      this._changeDetectorRef.markForCheck();
      this.closeDetails();
      return;
    }
    if (
      this.selectedJobRole &&
      this.selectedJobRole.id === job.id &&
      this.count === 0
    ) {
      // set value in form for update
      this.selectedJobRoleForm.patchValue(job);
      this.count++;
      this._changeDetectorRef.markForCheck();
    } else if (this.count === 1) {
      // to close for update
      this.count--;
      this.selectedJobRole = null;
      this._changeDetectorRef.markForCheck();
      this.closeDetails();
      return;
    }
  }

  /**
   * Close the details
   */
  closeDetails(): void {
    this.selectedJobRole = null;
  }

  filterSearch(event) {
    const value = event.target.value.toLowerCase();
    this._nosService.nos$.subscribe((res) => {
      this.nos$ = of(res.filter(item => item.name.toLocaleLowerCase().includes(value)));
    });
  }
  filterSector(event) {
    const value = event.target.value.toLowerCase();
    this.jobRole$.subscribe((res) => {
      this.jobRole$ = of(res.filter(item => item.name.toLocaleLowerCase().includes(value)));
    });
  }

  /**
   * Create Pos
   */
  createNos(): void {
    this._nosService
      .createPosForm({ id: 'new' }, 'add')
      .subscribe((response) => {
        this.nos$ = this._nosService.nos$;
        this.selectedJobRole = { id: 'new' };
        this.selectedJobRoleForm.reset();
        // Mark for check
        this._changeDetectorRef.markForCheck();
      });
  }

  /**
   * Update the selected product using the form mock-api
   */
  updateselectedJobRole() {
    if (this.selectedJobRoleForm.invalid) {
      return false;
    }
    if (this.selectedJobRole?.id === 'new') {
      const selectedJobRole = this.jobRole.find(
        sector =>
          sector.id === this.selectedJobRoleForm.value.job_id
      );
      this.selectedJobRoleForm.value['job_role'] = selectedJobRole;
      this._nosService
        .createPosForm({ id: 'new' }, 'remove')
        .subscribe();
      this._nosService
        .createPos(this.selectedJobRoleForm.value)
        .subscribe((res) => {
          this._changeDetectorRef.markForCheck();
          this.selectedJobRoleForm.reset();
          this.selectedJobRole = null;
        });
    } else {
      this._nosService
        .updatePos(
          this.selectedJobRole.id,
          this.selectedJobRoleForm.value
        )
        .subscribe((res) => {
          this._changeDetectorRef.markForCheck();
          this.selectedJobRoleForm.reset();
          this.selectedJobRole = null;
        });
    }
  }

  /**
   * Delete the selected product using the form mock-api
   */
  deleteSelectedJobRole(job): void {
    console.log(job.id);
    this._nosService.deletePos(job.id).subscribe((res) => {
      this._changeDetectorRef.markForCheck();
    });
  }

  /**
   * Show flash message
   */
  showFlashMessage(type: 'success' | 'error'): void {
    // Show the message
    this.flashMessage = type;

    // Mark for check
    this._changeDetectorRef.markForCheck();

    // Hide it after 1.5 seconds
    setTimeout(() => {
      this.flashMessage = null;
      this.selectedJobRole = {};
      // Mark for check
      this._changeDetectorRef.markForCheck();
    }, 1500);
  }

  /**
   * Track by function for ngFor loops
   *
   * @param index
   * @param item
   */
  trackByFn(index: number, item: any): any {
    return item.id || index;
  }
  protected sectorFilterSearch() {
    if (!this.jobRoles) {
      return;
    }
    // get the search keyword
    let search = this.inputFilterCtrl.value;
    if (!search) {
      this.jobRoles.next(this.jobRole.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.jobRoles.next(
      this.jobRole.filter(bank => bank.name.toLowerCase().includes(search))
    );
  }

}
