import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    OnDestroy,
    OnInit,
    ViewChild,
    ViewEncapsulation
} from '@angular/core';
import {
    FormBuilder,
    FormControl,
    FormGroup,
    Validators
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSelect } from '@angular/material/select';
import { fuseAnimations } from '@fuse/animations';
import { ConfirmationDailogComponent } from 'app/shared/component/confirmation-dailog/confirmation-dailog.component';
import { Observable, of, ReplaySubject, Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SectorService } from '../sector/sector.service';
import { JobRoleService } from './job-role.service';
import { IJobRole } from './job-role.types';

@Component({
    selector: 'app-job-role',
    templateUrl: './job-role.component.html',
    styleUrls: ['./job-role.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: fuseAnimations,
})
export class JobRoleComponent implements OnInit, OnDestroy {

    // Dropdown Search
    @ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;
    public selectCtrl: FormControl = new FormControl();
    public inputFilterCtrl: FormControl = new FormControl();
    public jobSectors: ReplaySubject<IJobRole[]> = new ReplaySubject<IJobRole[]>(1);
    _onDestroy = new Subject<void>();
    // Dropdown Search end

    jobRoles$: Observable<IJobRole[]>;
    jobSector$: Observable<any>;
    jobSector: any;
    flashMessage: 'success' | 'error' | null = null;
    isLoading: boolean = false;
    jobRoleTableColumns: string[] = ['name', 'job', 'createdDate', 'actions'];
    selectedJobRole = null;
    jobRoleCount: number = 0;
    selectedJobRoleForm: FormGroup;
    count: number = 0;
    filterRecords = '';
    currentLanguageIndex = 0;
    roleCount: number = 0;
    private _unsubscribeAll: Subscription;
    jobRoleModel: any;

    /**
     * Constructor
     */
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _formBuilder: FormBuilder,
        private _jobRoleService: JobRoleService,
        private _sectorService: SectorService,
        private dialog: MatDialog
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.selectedJobRoleForm = this._formBuilder.group({
            name: ['', [Validators.required]],
            // eslint-disable-next-line @typescript-eslint/naming-convention
            sector_id: ['', [Validators.required]],
            createdAt: [''],
            sector: [''],
            updatedAt: [''],
        });

        this._sectorService.getSector().subscribe((res: any) => {
            const response = res.length > 0 ? res : res.data;
            this.jobSector$ = of(response);
            this.jobSector = response;
            this.jobSectors.next(response);
        });
        this.jobRoles$ = this._jobRoleService.jobRoles$;
        this.isLoading = true;
        this._unsubscribeAll = this._jobRoleService
            .getJobRole()
            .subscribe((res) => {
                this.isLoading = false;
                // Update the counts
                this.jobRoleCount = res.length;
                // Mark for check
                this._changeDetectorRef.markForCheck();
            });
        this.inputFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
            this.sectorFilterSearch();
        });
    }
    // Open delete confirmation dialog
    openDialog(id) {
        const dialogRef = this.dialog.open(ConfirmationDailogComponent,{
            data: {
                message: 'Are you sure want to delete?',
                buttonText: {
                    ok: 'Yes',
                    cancel: 'No'
                }
            }
        });
        dialogRef.afterClosed().subscribe((confirmed: boolean) => {
            if (confirmed) {
                this.deleteselectedJobRole(id);
                const a = document.createElement('a');
                a.click();
                a.remove();
            }
        });
    }
    // Open delete confirmation dialog end

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.unsubscribe();
        this._jobRoleService
                .createJobRoleForm({ id: 'new' }, 'remove')
            .subscribe();
        this.selectedJobRoleForm.reset();
        this.selectedJobRole = null;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle product details
     *
     * @param productId
     */
    toggleDetails(job: IJobRole): void {
        this.selectedJobRole = job;
        if (job.id === 'new') {
            // to remove 'new' id for add jobRole
            this._jobRoleService
                .createJobRoleForm({ id: 'new' }, 'remove')
                .subscribe();
            this._changeDetectorRef.markForCheck();
            this.closeDetails();
            return;
        }
        if (
            this.selectedJobRole &&
            this.selectedJobRole.id === job.id &&
            this.count === 0
        ) {
            // set value in form for update
            this.selectedJobRoleForm.patchValue(job);
            this.count++;
            this._changeDetectorRef.markForCheck();
        } else if (this.count === 1) {
            // to close for update
            this.count--;
            this.selectedJobRole = null;
            this._changeDetectorRef.markForCheck();
            this.closeDetails();
            return;
        }
    }

    /**
     * Close the details
     */
    closeDetails(): void {
        this.selectedJobRole = null;
    }

    filterSearch(event) {
        const value = event.target.value.toLowerCase();
        this._jobRoleService.jobRoles$.subscribe((res) => {
            this.jobRoles$ = of(res.filter(item => item.name.toLocaleLowerCase().includes(value)));
        });
    }
    filterSector(event) {
        const value = event.target.value.toLowerCase();
        this.jobSector$.subscribe((res) => {
            this.jobSector$ = of(res.filter(item => item.name.toLocaleLowerCase().includes(value)));
        });
    }

    /**
     * Create JobRole
     */
    createJobRole(): void {
        this._jobRoleService
            .createJobRoleForm({ id: 'new' }, 'add')
            .subscribe((response) => {
                this.jobRoles$ = this._jobRoleService.jobRoles$;
                this.selectedJobRole = { id: 'new' };
                this.selectedJobRoleForm.reset();
                // Mark for check
                this._changeDetectorRef.markForCheck();
            });
    }

    /**
     * Update the selected product using the form mock-api
     */
    updateselectedJobRole() {
        if (this.selectedJobRoleForm.invalid) {
            return false;
        }
        if (this.selectedJobRole?.id === 'new') {
            const selectedJobRole = this.jobSector.find(
                sector =>
                    sector.id === this.selectedJobRoleForm.value.sector_id
            );
            this.selectedJobRoleForm.value['sector'] = selectedJobRole;
            this._jobRoleService
                .createJobRoleForm({ id: 'new' }, 'remove')
                .subscribe();
            this._jobRoleService
                .createJobRole(this.selectedJobRoleForm.value)
                .subscribe((res) => {
                    this._changeDetectorRef.markForCheck();
                    this.selectedJobRoleForm.reset();
                    this.selectedJobRole = null;
                });
        } else {
            this._jobRoleService
                .updateJobRole(
                    this.selectedJobRole.id,
                    this.selectedJobRoleForm.value
                )
                .subscribe((res) => {
                    this._changeDetectorRef.markForCheck();
                    this.selectedJobRoleForm.reset();
                    this.selectedJobRole = null;
                });
        }
    }

    /**
     * Delete the selected product using the form mock-api
     */
    deleteselectedJobRole(job): void {
        this._jobRoleService.deleteJobRole(job.id).subscribe((res) => {
            this._changeDetectorRef.markForCheck();
        });
    }

    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'error'): void {
        // Show the message
        this.flashMessage = type;

        // Mark for check
        this._changeDetectorRef.markForCheck();

        // Hide it after 1.5 seconds
        setTimeout(() => {
            this.flashMessage = null;
            this.selectedJobRole = {};
            // Mark for check
            this._changeDetectorRef.markForCheck();
        }, 1500);
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any {
        return item.id || index;
    }
    filterDataChanged(value) {
        if (value === 'true') {
            console.log('True => ', value);
            this._jobRoleService.filterRecord(value).subscribe((res) => {
                console.log(res);
            });
            return false;
        }
        this._jobRoleService.filterRecord(false).subscribe((res) => {
            console.log(res);
        });
    }
    protected sectorFilterSearch() {
        if (!this.jobSectors) {
            return;
        }
        // get the search keyword
        let search = this.inputFilterCtrl.value;
        if (!search) {
            this.jobSectors.next(this.jobSector.slice());
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the banks
        this.jobSectors.next(
            this.jobSector.filter(bank => bank.name.toLowerCase().includes(search))
        );
    }
    onChangeJobRoleSelect(value) {
       console.log("valuee++++++++++++++++" ,value)
        if(value !=''){
        this._jobRoleService.filterJobRole(value).subscribe((res) => {
        console.log(res);
        });
        }
    }
}
