export interface IJobRole{
    id: string;
    name: string;
}
export interface ISector{
    id: string;
    name: string;
}
