/* eslint-disable @typescript-eslint/naming-convention */
export interface IPapers {
    id: string;
    name: string;
    no_of_question: string;
    total_marks: string;
}
