/* eslint-disable @typescript-eslint/naming-convention */
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PaperService } from '../papers.service';

@Component({
  selector: 'app-add-paper',
  templateUrl: './add-paper.component.html',
  styleUrls: ['./add-paper.component.scss']
})
export class AddPaperComponent implements OnInit {

  paperForm: FormGroup;
  isNew = true;
  id: string;
  count: any[] = [];
  weightage: any[] = [];
  totalMarks: number = 0;
  marks: any;
  isLoading = false;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private _changeDetectorRef: ChangeDetectorRef,
    private _paperService: PaperService,
    private router: Router,
    private _toastr: ToastrService
  ) {
    this.paperForm = _formBuilder.group({
      paperName: ['', [Validators.required]],
      totalMarks: ['', [Validators.required]],
      noOfQuestion: _formBuilder.array([this.newQuestions()]),
    });
  }

  ngOnInit() {
    this.id = this._activatedRoute.snapshot.paramMap.get('id');
    if (this.id) {
      this.isNew = false;
      this.isLoading = true;
      this._paperService.getPaperDetails(this.id).subscribe((res: any) => {
        console.log('Response ==> ', res);
        const no_of_question = JSON.parse(res.no_of_question);
        for (let i = 1; i < no_of_question.length; i++) {
          this.addQuestion();
          this.count.push(no_of_question[i].count);
          this.weightage.push(no_of_question[i].weightage);
        }
        this.paperForm.patchValue({
          paperName: res.name,
          totalMarks: res.total_marks,
          noOfQuestion: no_of_question
        });
        this.isLoading = false;
      });
    }
  }
  get err() {
    return this.paperForm.controls;
  }
  // show weightage
  onChangeWeightage() {
    this.totalMarks = 0;
    this.count.map((data, i) => {
      if (this.weightage[i]) {
        this.totalMarks += +(data * this.weightage[i]);
      }
    });
  }
  // show weightage end

  // Add question
  questions(): FormArray {
    return this.paperForm.get('noOfQuestion') as FormArray;
  }

  newQuestions(): FormGroup {
    return this._formBuilder.group({
      count: [0],
      weightage: [0],
    });
  }

  removeQuestion(i: number) {
    this.totalMarks = 0;
    this.count.splice(i, 1);
    this.weightage.splice(i, 1);
    this.questions().removeAt(i);
  }

  addQuestion() {
    this.questions().push(this.newQuestions());
    this._changeDetectorRef.markForCheck();
  }
  // Add question and waitage end

  addUpdatePaper() {
    console.log('Paper Submit');
    let totalMarks = 0;
    this.paperForm.value.noOfQuestion.map((x) => {
      totalMarks += x['count'] * x['weightage'];
    });
    if (totalMarks !== +this.paperForm.value.totalMarks) {
      this._toastr.warning(
        'Total number of marks doesn\'t match with total marks'
      );
      return false;
    }
    const addUpdatePaper = {
      name: this.paperForm.value.paperName,
      total_marks: this.paperForm.value.totalMarks,
      no_of_question: JSON.stringify(this.paperForm.value.noOfQuestion)
    };
    if (this.isNew) {
      this._paperService.postPaper(addUpdatePaper).subscribe((res) => {
        console.log('API Response => ', res);
        this.router.navigateByUrl('/papers');
      });
    } else {
      this._paperService.updatePaper(this.id, addUpdatePaper).subscribe((res) => {
        console.log('API Response => ', res);
        this.router.navigateByUrl('/papers');
      });
    }
  }

}
