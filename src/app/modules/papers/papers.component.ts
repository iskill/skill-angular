import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDailogComponent } from 'app/shared/component/confirmation-dailog/confirmation-dailog.component';
import { Observable, of } from 'rxjs';
import { PaperService } from './papers.service';
import { IPapers } from './papers.types';

@Component({
  selector: 'app-paper',
  templateUrl: './papers.component.html',
  styleUrls: ['./papers.component.scss']
})
export class PaperComponent implements OnInit {

  isLoading = false;
  papers$: Observable<IPapers[]>;
  paperTableColumns: string[] = ['name', 'totalMarks', 'noOfQuestions', 'actions'];

  constructor(
    private dialog: MatDialog,
    private _paperService: PaperService,
    private _changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.papers$ = this._paperService.papers$;
    this._paperService.getPapers().subscribe((response) => {
      response.map((data: any) => {
        data.no_of_question = JSON.parse(data.no_of_question).map(li => li.count).reduce(this.sumReducer, 0);
      });
    });
  }
  sumReducer(sum, val) {
    return sum + val;
  }

  filterSearch(event) {
    const value = event.target.value.toLowerCase();
    this._paperService.papers$.subscribe((res) => {
      this.papers$ = of(res.filter(item => item.name.toLocaleLowerCase().includes(value)));
    });
  }

  // Open delete confirmation dialog
  openDialog(id) {
    const dialogRef = this.dialog.open(ConfirmationDailogComponent, {
      data: {
        message: 'Are you sure want to delete?',
        buttonText: {
          ok: 'Yes',
          cancel: 'No'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.deletePaper(id);
        const a = document.createElement('a');
        a.click();
        a.remove();
      }
    });
  }
  // Open delete confirmation dialog end

  deletePaper(id): void {
    this._paperService.deletePaper(id).subscribe((res) => {
      this._changeDetectorRef.markForCheck();
    });
  }

  trackByFn(index: number, item: any): any {
    return item.id || index;
  }

}
