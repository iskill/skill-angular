import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';
import { ApiResponse } from '../scheme/scheme.types';
import { IPapers } from './papers.types';

@Injectable({
  providedIn: 'root'
})
export class PaperService {

  private _papers: BehaviorSubject<IPapers[] | null> = new BehaviorSubject(null);

  constructor(private http: HttpClient) { }

  get papers$(): Observable<IPapers[]> {
    return this._papers.asObservable();
  }

  getPapers(): Observable<IPapers[]> {
    return this.http.get<ApiResponse>(`${environment.apiBaseUrl}papers`).pipe(
      map((response) => {
        if (response.status === 200) {
          this._papers.next(response.data);
          return response.data;
        }
      })
    );
  }
  postPaper(paperData) {
    return this.http.post(`${environment.apiBaseUrl}paper`, paperData);
  }
  updatePaper(id, updateData) {
    return this.http.put(`${environment.apiBaseUrl}paper/${id}`, updateData);
  }
  deletePaper(id: string): Observable<ApiResponse> {
    return this.papers$.pipe(
      take(1),
      switchMap(papers => this.http.delete<ApiResponse>(`${environment.apiBaseUrl}paper/${id}`).pipe(
        map((newJob) => {
          const index = papers.findIndex(item => item.id === id);
          papers.splice(index, 1);
          this._papers.next(papers);
          return newJob;
        })
      ))
    );
  }
  getPaperDetails(id): Observable<IPapers[]> {
    return this.http.get<ApiResponse>(`${environment.apiBaseUrl}paper/${id}`).pipe(
      map((response) => {
        if (response.status === 200) {
          this._papers.next(response.data);
          return response.data;
        }
      })
    );
  }

}
