import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddPaperComponent } from './add-paper/add-paper.component';
import { PaperComponent } from './papers.component';
import { Route, RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRippleModule } from '@angular/material/core';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SharedModule } from 'app/shared/shared.module';

const paperRoute: Route[] = [
  {
    path: '',
    component: PaperComponent
  },
  {
    path: 'add-paper',
    component: AddPaperComponent
  },
  {
    path: 'update-paper/:id',
    component: AddPaperComponent
  }
];

@NgModule({
  declarations: [
    PaperComponent,
    AddPaperComponent
  ],
  imports: [
    RouterModule.forChild(paperRoute),
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatRippleModule,
    MatSortModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatTableModule,
    MatTooltipModule,
    MatDividerModule,
    SharedModule
  ]
})
export class PapersModule { }
