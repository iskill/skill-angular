/* eslint-disable @typescript-eslint/naming-convention */
export interface IAssessor{
    id: string;
    pic_url: string;
    first_name: string;
    last_name: string;
    father_name: string;
    email: string;
    contact_no: string;
    address: string;
    adhaar_no: string;
    adhaar_img_url: string;
    voter_id: string;
    voter_id_img_url: string;
    qualification: string;
    qualification_doc_img_url: string;
    experience: string;
    designation: string;
    isCertified: boolean;
    account_holder_name: string;
    account_number: string;
    bank: string;
    ifsc: string;
    branch: string;
    password: string;
    prefered_location: string;
    remark: string;
    applied_for: string;
    user: [];
}
