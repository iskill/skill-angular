/* eslint-disable @typescript-eslint/naming-convention */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, switchMap, take, tap } from 'rxjs/operators';
import { ApiResponse } from '../scheme/scheme.types';
import { IAssessor } from './assessor.type';
import * as AWS from 'aws-sdk';
@Injectable({
  providedIn: 'root'
})
export class AssessorService {

  // Private
  private _assessors: BehaviorSubject<IAssessor[] | null> = new BehaviorSubject(null);

  /**
   * Constructor
   */
  constructor(private _httpClient: HttpClient) { }

  // -----------------------------------------------------------------------------------------------------
  // @ Accessors
  // -----------------------------------------------------------------------------------------------------

  /**
   * Getter for JobRoles
   */
  get assessors$(): Observable<IAssessor[]> {
    return this._assessors.asObservable();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Get JobRole
   */
  getAccessors(options?): Observable<ApiResponse> {
    return this._httpClient.get<ApiResponse>(`${environment.apiBaseUrl}assessors`,{params: options}).pipe(
      tap((response) => {
        const ifUser = response.data.filter(data => data['user']);
        this._assessors.next(ifUser);
      })
    );
  }

  /**
   * Create Accessor
   */
  createAccessor(accessor: IAssessor): Observable<IAssessor> {
    return this._httpClient.post<IAssessor>(`${environment.apiBaseUrl}assessors`, accessor);
  }

  getAssessor(id: string): Observable<IAssessor> {
    return this._httpClient.get<IAssessor>(
      `${environment.apiBaseUrl}assessors/${id}`
    );
  }

  /**
   * Update Accessor
   *
   * @param id
   * @param product
   */
  updateAccessor(id: string, assessor): Observable<IAssessor> {
    delete assessor.id;
    return this._httpClient.put<IAssessor>(`${environment.apiBaseUrl}assessors/${id}`, assessor).pipe(
      tap((response) => {
        console.log(response);
      })
    );
  }

  /**
   * Delete the accessor
   *
   * @param id
   */
  deleteAssessor(id: string): Observable<ApiResponse> {
    return this.assessors$.pipe(
      take(1),
      switchMap(accessors => this._httpClient.delete<ApiResponse>(`${environment.apiBaseUrl}assessors/${id}`).pipe(
        map((newRule) => {
          // Find the index of the deleted accessor
          const index = accessors.findIndex(item => item.id === id);

          // Delete the accessor
          accessors.splice(index, 1);

          // Update the accessors
          this._assessors.next(accessors);

          return newRule;
        })
      ))
    );
  }

  uploadfile(file, name) {
    console.log(file);
    AWS.config.region = 'ap-south-1';
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: 'ap-south-1:1c32c674-c143-48bb-9484-2a97443b4176',
    });

    const s3 = new AWS.S3({
      apiVersion: '2006-03-01',
      params: { Bucket: 'iskill-development' }
    });

    return s3.upload({
      Key: name,
      Bucket: 'iskill-development',
      Body: file[0],
      ACL: 'private'
    },
      (err, data) => {
        if (err) {
          console.log(err, 'there was an error uploading your file');
        } else {
          console.log(data);
        }
      });
  }
}
