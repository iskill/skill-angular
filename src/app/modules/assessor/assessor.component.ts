import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDailogComponent } from 'app/shared/component/confirmation-dailog/confirmation-dailog.component';
import { ToastrService } from 'ngx-toastr';
import { Observable, of, ReplaySubject, Subject } from 'rxjs';
import { BatchExamService } from '../batch-exam/batch-exam.service';
import { JobRoleService } from '../job-role/job-role.service';
import { SectorService } from '../sector/sector.service';
import { ISector } from '../sector/sector.type';
import { MustMatch } from './add-assessor/add-assessor.component';
import { AssessorService } from './assessor.service';
import { IAssessor } from './assessor.type';

@Component({
    selector: 'app-assessor',
    templateUrl: './assessor.component.html',
    styleUrls: ['./assessor.component.scss'],
})
export class AssessorComponent implements OnInit, OnDestroy {

    assessors$: Observable<IAssessor[]>;
    showPopup = false;
    selectedAssessor = '';
    flashMessage: 'success' | 'error' | null = null;
    isLoading: boolean = false;
    assessorTableColumns: string[] = [
        'name',
        'email',
        'certified',
        'applied',
        'Review',
        'Rating',
        'actions',
    ];
    passwordResetForm: FormGroup;
    assessorCount: number = 0;
    filterCertified = '';
    filterDate = '';
    isSelectedState=''
    isSelectedCity='';
    isSelectedSector='';
    isSelectedJobRole='';
    States=[];
    sectorName=[];
    sectorRole: ISector[];
    public sectors: ReplaySubject<ISector[]> = new ReplaySubject<ISector[]>(1);
    City:any
    todayDate:Date = new Date();
    selectedFilter = {
        state: '0',
        city:'0',
        sector:'0',
      }
    private _unsubscribeAll: Subject<any> = new Subject<any>();
    cityName: any;
    jobRoleName: any;
    endDateToa: any;
    assessorGivenRating: any;
    averageRating: number;

    /**
     * Constructor
     */
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _assessorService: AssessorService,
        private dialog: MatDialog,
        private toastr: ToastrService,
        private _formBuilder: FormBuilder,
        private _stateService: BatchExamService,
        private _sectorService:SectorService,
        private _jonRolesService:JobRoleService,
        private _serviceBatch: BatchExamService,
    ) {
        this.passwordResetForm = _formBuilder.group({
            newPassword: ['', [Validators.required, Validators.minLength(6)]],
            confirmPassword: ['', Validators.required],
        }, { validator: MustMatch('newPassword', 'confirmPassword') });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.isLoading = true;
        this.assessors$ = this._assessorService.assessors$;
        this.getFilteredAssessor();
        this._stateService.getStates().subscribe((res)=>{
            this.States =res?.data
            console.log("this.States" ,this.States);
        })
        // this._sectorService.getSector().subscribe((res)=>{
        //     this.sectorName =res['data']
        //     console.log("this.sectorName" ,this.sectorName);
        // })
        this._sectorService.getSector().subscribe((res:any)=>{
            this.sectorRole=res
            this.sectors.next(res.length > 0 ? res : res.data);
            this.isLoading = false;
            
            console.log("sector role" ,this.sectorRole);
            
        })
    }

    get f() {
        return this.passwordResetForm.controls;
    }

    getFilteredAssessor(option?) {
        this._assessorService.getAccessors(option).subscribe((res) => {
            this.isLoading = false;
            // Update the counts
            this.assessorCount = res.data.length;
            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
    }

    openDialog(id) {
        const dialogRef = this.dialog.open(ConfirmationDailogComponent, {
            data: {
                message: 'Are you sure want to delete?',
                buttonText: {
                    ok: 'Yes',
                    cancel: 'No'
                }
            }
        });
        dialogRef.afterClosed().subscribe((confirmed: boolean) => {
            if (confirmed) {
                this.deleteSelectedAssessor(id);
                const a = document.createElement('a');
                a.click();
                a.remove();
            }
        });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next({});
        this._unsubscribeAll.complete();
    }

    filterSearch(event) {
        const value = event.target.value.toLowerCase();
        this._assessorService.assessors$.subscribe((res) => {
            this.assessors$ = of(res.filter((item: any) =>
                (item.user.first_name + item.user.last_name)
                    .toLocaleLowerCase()
                    .includes(value))
            );
        });
    }
    toggle(id, value) {
        this._assessorService.updateAccessor(id, { isCertified: value }).subscribe((res) => {
            console.log(res);
        });
    }
    addFilterCertified(value) {
        this.filterDate=''
        if (value !== '') {
            this.getFilteredAssessor({ isCertified: value });
        } else {
            this.getFilteredAssessor();
        }
    }

    addFilterToa(value) {
        console.log("++++++++++++++++" ,value);
        this.filterCertified=''
        if (value !== '') {
            this.endDateToa = (new Date(value).toLocaleDateString().split('/').reverse().join('-'));
            this.getFilteredAssessor({ toa_end_date: this.endDateToa });
        } else {
            this.getFilteredAssessor();
        }
    }

    onSelectState(id: string ,type:'state' |'city' | 'sector'|'jobRole'){
        console.log('onSelectState', id, type)
      this._stateService[type] =id;
      if(type === 'state'){
        this._stateService.getCities(id).subscribe((res)=>{
        this.cityName=res.data
        console.log("this.cityName" ,this.cityName)
        })
        if (this.selectedFilter.state != "0") {
            this.isLoading = true;
          
          }
      }
      if(type == "city"){
        this.getFilteredAssessor({ city_id: id });
      
        // if (this.selectedFilter.city != undefined) {
        //     this.isLoading = true;
        //      this._assessorService.getAccessors().subscribe()
        //   }
      }
      if(type =='sector'){
       this._jonRolesService.getJobRole(id).subscribe((res)=>{
        this.jobRoleName=res['data']
        console.log("this.jobRoleName" ,this.jobRoleName)
       })
      }
      if(type =='jobRole'){
        this.getFilteredAssessor({ jobrole_id: id });
        this.isLoading = false;
       }
    }
    toggleAssessorModal(id) {
        this.showPopup = !this.showPopup;
        if (this.showPopup) {
            this.selectedAssessor = id;
        }
        if (!this.showPopup) {
            this.selectedAssessor = '';
        }
        this.passwordResetForm.reset();
    }
    resetPassword() {
        const updateData = {
            password: this.passwordResetForm.value.newPassword
        };
        this._assessorService.updateAccessor(this.selectedAssessor, updateData).subscribe((res) => {
            console.log(res);
            if (res) {
                this.showPopup = false;
            }
        });
    }

    getBatchRating (){
        this._serviceBatch.getRating('').subscribe((res)=>{
            const ratings = res?.data?.map(item => item.rating);
            console.log(ratings);
            this.assessorGivenRating= ratings
           let sum = 0;
           for (let i = 0; i < this.assessorGivenRating?.length; i++) {
             sum += this.assessorGivenRating[i];
           }
           this.averageRating = sum / this.assessorGivenRating?.length;
           console.log("Average Rating:", this.averageRating);       
        })
      }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Delete the selected product using the form mock-api
     */
    deleteSelectedAssessor(accessorId): void {
        // Delete the rule on the server
        this._assessorService.deleteAssessor(accessorId).subscribe((res) => {
            if (res.status === 200) {
                this._changeDetectorRef.markForCheck();
            }
        }, (error) => {
            console.log(error);
            this.toastr.error(error.error.message);
        });
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any {
        return item.id || index;
    }
}
