/* eslint-disable @typescript-eslint/naming-convention */
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BatchExamService } from 'app/modules/batch-exam/batch-exam.service';
import { ICities, IStates } from 'app/modules/batch-exam/batch-exam.type';
import { JobRoleService } from 'app/modules/job-role/job-role.service';
import { IJobRole } from 'app/modules/job-role/job-role.types';
import { PaperService } from 'app/modules/papers/papers.service';
import { SchemeService } from 'app/modules/scheme/scheme.service';
import { SectorService } from 'app/modules/sector/sector.service';
import { ISector } from 'app/modules/sector/sector.type';
import { ReplaySubject } from 'rxjs';
import { AssessorService } from '../assessor.service';


@Component({
  selector: 'app-add-assessor',
  templateUrl: './add-assessor.component.html',
  styleUrls: ['./add-assessor.component.scss'],
  
})
export class AddAssessorComponent implements OnInit {

  @ViewChild('fileInput') nativeFile;

  id: string;
  assessorForm: FormGroup;
  isNew: boolean = true;
  submitted = false;
  isLoading: boolean = false;
  // Image upload
  picFile = [];
  adhaarImgFile = [];
  voterIdImgFile = [];
  qualificationDocImgFile = [];
  experienceCertificateImgFile = [];
  toaImgFile = [];
  picUrl = '';
  adhaarImgUrl = '';
  voterIdImgUrl: any;
  qualificationDocImgUrl = '';
  experienceCertificateImgUrl = '';
  toaCertificateImgUrl='';
  todayDate:Date = new Date();
  public inputSearchStateCtrl: FormControl = new FormControl();
  public inputSearchCityCtrl: FormControl = new FormControl();
  public states: ReplaySubject<IStates[]> = new ReplaySubject<IStates[]>(1);
  public cities: ReplaySubject<ICities[]> = new ReplaySubject<ICities[]>(1);
  public jobrolId: ReplaySubject<IJobRole[]> = new ReplaySubject<IJobRole[]>(1);
  public sectorsId: ReplaySubject<ISector[]> = new ReplaySubject<ISector[]>(1);
  state: IStates[];
  city_id: ICities[];
  sectors: ISector[];
  jobRoles: IJobRole[];
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private _assessorService: AssessorService,
    private _route: Router,
    private cd: ChangeDetectorRef,
    private _batchExamService: BatchExamService,
    private _schemeService: SchemeService,
    private _paperService: PaperService,
    private _sectorService: SectorService,
    private _jobRoleService: JobRoleService,
  ) {
    this.assessorForm = this._formBuilder.group({
      id: [''],
      pic_url: [''],
      first_name: ['',Validators.required],
      last_name: [''],
      father_name: [''],
      email: ['',Validators.required],
      contact_no: ['', Validators.required],
      address: [''],
      adhaar_no: [''],
      adhaar_img_url: [''],
      voter_id: [''],
      voter_id_img_url: [''],
      qualification: [''],
      qualification_doc_img_url: [''],
      experience: [''],
      experience_certificate_img_url: [''],
      designation: [''],
      toa: [''],
      toa_url:[''],
      // toa_start_date:[''],
      toa_end_date:[''],
      city_id: ['',Validators.required],
      state_id: ['', Validators.required],
      jobrole_id: ['',Validators.required],
      sector_id: ['',Validators.required],
      account_holder_name: [''],
      account_number: [''],
      bank: [''],
      ifsc: [''],
      branch: [''],
      password: [''],
      confirmPassword: [''],
      remark: [''],
      applied_for: [''],
    }, { validator: MustMatch('password', 'confirmPassword') });
  }

  ngOnInit(): void {

    this.id = this._activatedRoute.snapshot.paramMap.get('id');
    this.getServiceData();
    if (this.id) {
      this._assessorService.getAssessor(this.id).subscribe((res: any) => {
        this._batchExamService.getCities(res.data.state_id).subscribe((res) => {
            this.city_id = res.data;
            this.cities.next(res.data);
        });
         this._jobRoleService.getJobRole(res.data.sector_id).subscribe((res) => {
       this.jobRoles = res['data'];
       this.jobrolId.next(res['data']);
       });
        if (res.status === 200 && res.data) {
          this.isNew = false;
          this.picUrl = res.data.pic_url;
          this.adhaarImgUrl = res.data.adhaar_img_url;
          this.voterIdImgUrl = res.data.voter_id_img_url?.split('/');
          this.qualificationDocImgUrl = res.data.qualification_doc_img_url;
          this.experienceCertificateImgUrl = res.data.experience_certificate_img_url;
          this.toaCertificateImgUrl = res.data.toa_url;
          
          this.assessorForm.patchValue({
            id: res.data.id,
            first_name: res.data.user.first_name,
            last_name: res.data.user.last_name,
            father_name: res.data.father_name,
            password: res.data.user.password,
            confirmPassword: res.data.user.password,
            email: res.data.user.email,
            contact_no: res.data.contact_no,
            address: res.data.address,
            adhaar_no: res.data.adhaar_no,
            voter_id: res.data.voter_id,
            qualification: res.data.qualification,
            experience: res.data.experience,
            designation: res.data.designation,
            toa:res.data.toa,
            state_id: res.data.state_id,
            city_id: res.data.city_id,
            sector_id: res.data.sector_id,
            jobrole_id: res.data.jobrole_id,
            toa_end_date:res.data.toa_end_date,
            account_holder_name: res.data.bank_detail.account_holder_name,
            account_number: res.data.bank_detail.account_number,
            bank: res.data.bank_detail.bank,
            ifsc: res.data.bank_detail.ifsc,
            branch: res.data.bank_detail.branch,
            prefered_location: res.data.prefered_location,
            remark: res.data.remark,
            applied_for: res.data.applied_for,
          });
        } else {
          this._route.navigate(['assessor']);
        }
      });
    } else {
      const password = this.assessorForm.get('password');
      password.setValidators([
        Validators.required,
        Validators.minLength(6)
      ]);
    }
    this.inputSearchStateCtrl.valueChanges
            .subscribe(() => {
                this.filterSearch('states', 'inputSearchStateCtrl', 'state', 'state_name');
            });
        this.inputSearchCityCtrl.valueChanges
            .subscribe(() => {
                this.filterSearch('cities', 'inputSearchCityCtrl', 'city_id', 'city_name');
            });
    }
     dateRangeChange( dateRangeEnd: HTMLInputElement) {
    if (dateRangeEnd.value) {
        // this.assessorForm.value.toa_start_date = (new Date(dateRangeStart.value).toLocaleDateString().split('/').reverse().join('-'));
        // this.assessorForm.value.toa_end_date= new Date(dateRangeEnd.value).toLocaleDateString().split('/').reverse()?.join('-');
        const dateRangeEndValue = new Date(dateRangeEnd.value)?.toLocaleDateString()?.split('/')?.reverse()?.join('-');
        this.assessorForm.value.toa_end_date = dateRangeEndValue || '';

    }
    console.log( this.assessorForm.value.toa_end_date)
     }
    protected filterSearch(observable, searchInput, itemArray, fieldName) {
    if (!this[observable]) {
       return;
    }
     // get the search keyword
    let search = this[searchInput].value;
    if (!search) {
      this[observable].next(this[itemArray]?.slice());
      return;
    } else {
       search = search.toLowerCase();
    }
  // filter the banks
    this[observable].next(
       this[itemArray].filter(bank => bank[fieldName].toLowerCase().indexOf(search) > -1)
   );  
   }
   getServiceData() {
    this.isLoading = true;
    this._batchExamService.getStates().subscribe((response: any) => {
        this.state = response.data;
        this.states.next(response.data);
        console.log("response.data" ,response.data);
    });
    this._sectorService.getSector().subscribe((response: any) => {
      if(response.data){
        this.sectors = response.data;
        this.sectorsId.next(response.data);
      }
      else{
        this.sectors = response;
        this.sectorsId.next(response);
      }      
      console.log("response.data33" ,response);       
      console.log("response.data sectros" ,response.data);
  });
  
    // this._sectorService.getSector().subscribe((response: any) => {
    //     if (response) {
    //         this.isLoading = false;
    //         this.sectors = response.length > 0 ? response : response.data;
    //     }
    // });
    // this._jobRoleService.getJobRole().subscribe((response: any) => {
    //     if (response) {
    //         this.isLoading = false;
    //         this.jobRoles = response.length > 0 ? response : response.data;
    //     }
    // });
  
   
   
}
onSelectState(value) {
  this._batchExamService.getCities(value).subscribe((res) => {
      this.city_id = res.data;
      this.cities.next(res.data);
  });

}
onSelectSector(value) {
  this._jobRoleService.getJobRole(value).subscribe((res) => {
      this.sectors = res['data'];
      this.jobrolId.next(res['data']);
  });
  
}
  async addUpdateAssessor() {
    this.submitted = true;
    console.log("assessor",this.assessorForm.value);
    // return;
    if (this.assessorForm.valid) {
      if (this.picFile.length !== 0) {
        const fileName = this.picFile[0].name.split('.');
        fileName[fileName.length - 1] = fileName[fileName.length - 1].toLocaleLowerCase();
        console.log('picFile ==>', fileName);
        await this._assessorService.uploadfile(this.picFile, this.assessorForm.value.email + '_profile' + '.' + fileName[1]).promise().then((result) => {
          this.assessorForm.value.pic_url = result.Location;
        });
      } else {
        this.assessorForm.value.pic_url = this.picUrl;
      }
      if (this.adhaarImgFile.length !== 0) {
        const fileName = this.adhaarImgFile[0].name.split('.');
        fileName[fileName.length - 1] = fileName[fileName.length - 1].toLocaleLowerCase();
        console.log('adhaarImgFile ==>', fileName);
        await this._assessorService.uploadfile(this.adhaarImgFile, this.assessorForm.value.email + '_addhar' + '.' + fileName[1]).promise().then((result) => {
          this.assessorForm.value.adhaar_img_url = result.Location;
        });
      } else {
        this.assessorForm.value.adhaar_img_url = this.adhaarImgUrl;
      }
      if (this.voterIdImgFile.length !== 0) {
        const fileName = this.voterIdImgFile[0].name.split('.');
        fileName[fileName.length - 1] = fileName[fileName.length - 1].toLocaleLowerCase();
        console.log('voterIdImgFile ==>', fileName);
        await this._assessorService.uploadfile(this.voterIdImgFile, 'resume/' + fileName.join('.')).promise().then((result) => {
          this.assessorForm.value.voter_id_img_url = result.Location;
        });
      } else {
        this.assessorForm.value.voter_id_img_url = this.voterIdImgUrl?.join('/');
      }
      if (this.qualificationDocImgFile.length !== 0) {
        const fileName = this.qualificationDocImgFile[0].name.split('.');
        fileName[fileName.length - 1] = fileName[fileName.length - 1].toLocaleLowerCase();
        console.log('voterIdImgFile ==>', fileName);
        await this._assessorService.uploadfile(this.qualificationDocImgFile, this.assessorForm.value.email + '_doc' + '.' + fileName[1]).promise().then((result) => {
          this.assessorForm.value.qualification_doc_img_url = result.Location;
        });
      } else {
        this.assessorForm.value.qualification_doc_img_url = this.qualificationDocImgUrl;
      }
      if (this.experienceCertificateImgFile.length !== 0) {
        const fileName = this.experienceCertificateImgFile[0].name.split('.');
        fileName[fileName.length - 1] = fileName[fileName.length - 1].toLocaleLowerCase();
        console.log('experienceCertificateImgFile ==>', fileName);
        await this._assessorService.uploadfile(this.experienceCertificateImgFile, this.assessorForm.value.email + '_doc' + '.' + fileName[1]).promise().then((result) => {
          this.assessorForm.value.experience_certificate_img_url = result.Location;
        });
      } else {
        this.assessorForm.value.experience_certificate_img_url = this.experienceCertificateImgUrl;
      }
      if (this.toaImgFile.length !== 0) {
        const fileName = this.toaImgFile[0].name.split('.');
        fileName[fileName.length - 1] = fileName[fileName.length - 1].toLocaleLowerCase();
        console.log('toaImgFile ==>', fileName);
        await this._assessorService.uploadfile(this.toaImgFile, this.assessorForm.value.email + '_doc' + '.' + fileName[1]).promise().then((result) => {
          this.assessorForm.value.toa_url = result.Location;
        });
      } else {
        this.assessorForm.value.toa_url = this.toaCertificateImgUrl;
      }
      delete this.assessorForm.value.confirmPassword;
      if (this.isNew) {
        delete this.assessorForm.value.id;
        this._assessorService.createAccessor(this.assessorForm.value).subscribe((response: any) => {
          if (response.status === 200) {
            this._route.navigate(['assessor']);
          }
        });
      } else {
        console.log(this.assessorForm.value.id, this.assessorForm.value);
        this._assessorService.updateAccessor(this.assessorForm.value.id, this.assessorForm.value).subscribe((response: any) => {
          if (response.status === 200) {
            this._route.navigate(['assessor']);
          }
        });
      }
    }
  }

  get f() {
    return this.assessorForm.controls;
  }

  removeImage(variable) {
    this[variable] = '';
  }

  selectFile(file, parameter) {
    const reader = new FileReader();
    reader.readAsDataURL(file[0]);
    reader.onload = (_event) => {
      if (parameter === 'pic_url') {
        this.picUrl = String(reader.result);
        this.picFile = file;
      }
      if (parameter === 'adhaar_img_url') {
        this.adhaarImgFile = file;
        this.adhaarImgUrl = String(reader.result);
      }
      if (parameter === 'voter_id_img_url') {
        this.voterIdImgFile = file;
        console.log('file==>', file);
        this.voterIdImgUrl = file[0].name;
        // this.voterIdImgUrl = String(reader.result);
      }
      if (parameter === 'qualification_doc_img_url') {
        this.qualificationDocImgFile = file;
        this.qualificationDocImgUrl = String(reader.result);
      }
      if (parameter === 'experience_certificate_img_url') {
        this.experienceCertificateImgFile = file;
        this.experienceCertificateImgUrl = String(reader.result);
      }
      if (parameter === 'toa_url') {
        this.toaImgFile = file;
        this.toaCertificateImgUrl = String(reader.result);
      }
    };
  }
}
// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      // return if another validator has already found an error on the matchingControl
      return;
    }

    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
}
