import {
    ChangeDetectorRef,
    Component,
    OnDestroy,
    OnInit,
    ViewChild,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { ConfirmationDailogComponent } from 'app/shared/component/confirmation-dailog/confirmation-dailog.component';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BatchExamService } from '../batch-exam/batch-exam.service';
import { JobRoleService } from '../job-role/job-role.service';
import { IJobRole } from '../job-role/job-role.types';
import { SectorService } from '../sector/sector.service';
import { ISector } from '../sector/sector.type';
import { QuestionBankService } from './question-bank.service';
import { IQuestions } from './question-bank.type';
import { saveAs } from 'file-saver';

@Component({
    selector: 'app-question-bank',
    templateUrl: './question-bank.component.html',
    styleUrls: ['./question-bank.component.scss'],
})
export class QuestionBankComponent implements OnInit, OnDestroy {

    // Dropdown Search
    @ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;
    public selectCtrl: FormControl = new FormControl();
    public selectSector: FormControl = new FormControl();
    public inputFilterCtrl: FormControl = new FormControl();
    public jobroles: ReplaySubject<IJobRole[]> = new ReplaySubject<IJobRole[]>(1);
    public sectors: ReplaySubject<ISector[]> = new ReplaySubject<ISector[]>(1);
    _onDestroy = new Subject<void>();
    // Dropdown Search end

    questions$: Observable<IQuestions[]>;

    flashMessage: 'success' | 'error' | null = null;
    isLoading: boolean = false;
    jobRole: IJobRole[];
    sectorRole: ISector[];
    jobRoleModel: any;
    questionTableColumns: string[] = [
        'Sector',
        'JobRole',
        'Nos',
        'Question',
        'weightage',
        'languages',
        'difficultyLevel',
        // 'actions',
    ];
    questionCount: number = 0;
    // Pagination
    pageSize = 25;
    pageIndex = 0;
    pageSizeOptions = [25, 50, 100, 200];
    showFirstLastButtons = true;
    searchTxt: any;
    currentLanguageIndex = 0;
    // Pagination end
    private _unsubscribeAll: Subject<any> = new Subject<any>();
    sectorModel: any;

    /**
     * Constructor
     */
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _questionBankService: QuestionBankService,
        private _jobRoleService: JobRoleService,
        private _sectorService:SectorService,
        private dialog: MatDialog,
        private _serviceBatch: BatchExamService,
    ) { }

    /**
     * On init
     */
    ngOnInit(): void {
        this.isLoading = true;
        this._jobRoleService.getJobRole().subscribe((res: any) => {
            this.jobRole = res;
            this.jobroles.next(res.length > 0 ? res : res.data);
            this.isLoading = false;
            console.log("jobRole role" ,this.jobRole);
        });
        this._sectorService.getSector().subscribe((res:any)=>{
            this.sectorRole=res
            this.sectors.next(res.length > 0 ? res : res.data);
            this.isLoading = false;
            
            console.log("sector role" ,this.sectorRole);
            
        })
        this.questions$ = this._questionBankService.questions$;
        this.inputFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                this.filterSearch();
            });
    }

    // Open delete confirmation dialog
    openDialog(id) {
        const dialogRef = this.dialog.open(ConfirmationDailogComponent, {
            data: {
                message: 'Are you sure want to delete?',
                buttonText: {
                    ok: 'Yes',
                    cancel: 'No'
                }
            }
        });
        dialogRef.afterClosed().subscribe((confirmed: boolean) => {
            if (confirmed) {
                this.deleteselectedQuestion(id);
                const a = document.createElement('a');
                a.click();
                a.remove();
            }
        });
    }
    // Open delete confirmation dialog end

    handlePageEvent(event: PageEvent) {
        this.questionCount = event.length;
        this.pageSize = event.pageSize;
        this.pageIndex = event.pageIndex;
        this._questionBankService.getQuestions(this.jobRoleModel, this.pageSize, this.pageIndex).subscribe();
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next({});
        this._unsubscribeAll.complete();
    }

    onChangeJobRoleSelect(event) {
        this.jobRoleModel = event.value;
        this.currentLanguageIndex = 0;
        this.isLoading = true;
        this._questionBankService.getQuestions(event.value, this.pageSize, this.pageIndex).subscribe((res) => {
            this.isLoading = false;
            this.questionCount = res['data']['count'];
        });
    }
    /**
     * Delete the selected product using the form mock-api
     */
    deleteselectedQuestion(questionId): void {
        // Delete the rule on the server
        this._questionBankService
            .deleteQuestion(questionId)
            .subscribe((res) => {
                if (res.status === 200) {
                    this._changeDetectorRef.markForCheck();
                }
            });
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any {
        return item.id || index;
    }
    protected filterSearch() {
        if (!this.jobroles) {
            return;
        }
        // get the search keyword
        let search = this.inputFilterCtrl.value;
        if (!search && this.jobRole.length>0) {
            this.jobroles.next(this.jobRole.slice());
            return;
        } else {
            search = search.toLowerCase();
        }
        // filter the banks
        // this.jobroles.next(
        //     this.jobRole.filter(bank => bank.name.toLowerCase().indexOf(search) > -1)
        // );
    }

    onChangeSectorSelect(event){
        this.sectorModel = event.value;
        this._jobRoleService.getJobRole(this.sectorModel).subscribe((res: any) => {
            this.jobRole = res;
            this.jobroles.next(res.length > 0 ? res : res.data);
            // this.isLoading = false;
            this.isLoading = false;
            console.log("jobRole role" ,this.jobRole);
        });
        console.log("this.sectorModel" ,this.sectorModel)
        this.currentLanguageIndex = 0;
       
       
    }
    exportQuestion() {
        this._serviceBatch.getQuestion(this.jobRoleModel).subscribe(
          (res: any) => {
            const blob = new Blob([res], { type: 'application/octet-stream' });
            saveAs(blob, 'all_Questions.xlsx');
          },
          (error: any) => {
            console.error('Failed to download the file.', error);
          }
        );
      }
}


