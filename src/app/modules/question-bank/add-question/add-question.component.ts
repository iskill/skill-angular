/* eslint-disable @typescript-eslint/naming-convention */
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BatchExamService } from 'app/modules/batch-exam/batch-exam.service';
import { ILocales } from 'app/modules/batch-exam/batch-exam.type';
import { JobRoleService } from 'app/modules/job-role/job-role.service';
import { IJobRole } from 'app/modules/job-role/job-role.types';
import { SectorService } from 'app/modules/sector/sector.service';
import { ISector } from 'app/modules/sector/sector.type';
import { LoaderService } from 'app/shared/loader.service';
import { debug, log } from 'console';
import { ToastrService } from 'ngx-toastr';
import * as XLSX from 'xlsx';
import { QuestionBankService } from '../question-bank.service';
import { IExcelQuestions } from '../question-bank.type';
type AOA = any[][];
@Component({
    selector: 'app-add-question',
    templateUrl: './add-question.component.html',
    styleUrls: ['./add-question.component.scss'],
})
export class AddQuestionComponent implements OnInit {
    @ViewChild('myInput') myInputVariable: ElementRef;

    storeData: any;
    questionData: any;
    showData: AOA = [[1, 2], [3, 4]];
    postQuestionsData: IExcelQuestions[] = [];
    questionDataLength: number;
    fileUploaded: File;
    worksheet: any;
    locales: ILocales[];
    sector: ISector[];
    jobRole: IJobRole[];
    arrayJobRole = [];
    arrayNos = [];
    selectedNos = '';
    selectedSector = '';
    selectedJobRole = '';
    
    headers = [
        'NOS',
        'IMAGE_QUESTION',
        'VIDEO_QUESTION',
        'IMAGE_A',
        'IMAGE_B',
        'IMAGE_C',
        'IMAGE_D',
        'ENGLISH',
        'ENGLISH_A',
        'ENGLISH_B',
        'ENGLISH_C',
        'ENGLISH_D',
        'HINDI',
        'HINDI_A',
        'HINDI_B',
        'HINDI_C',
        'HINDI_D',
        'MARATHI',
        'MARATHI_A',
        'MARATHI_B',
        'MARATHI_C',
        'MARATHI_D',
        'GUJARATI',
        'GUJARATI_A',
        'GUJARATI_B',
        'GUJARATI_C',
        'GUJARATI_D',
        'PUNJABI',
        'PUNJABI_A',
        'PUNJABI_B',
        'PUNJABI_C',
        'PUNJABI_D',
        'SINDHI',
        'SINDHI_A',
        'SINDHI_B',
        'SINDHI_C',
        'SINDHI_D',
        'TAMIL',
        'TAMIL_A',
        'TAMIL_B',
        'TAMIL_C',
        'TAMIL_D',
        'TELUGU',
        'TELUGU_A',
        'TELUGU_B',
        'TELUGU_C',
        'TELUGU_D',
        'MALAYALAM',
        'MALAYALAM_A',
        'MALAYALAM_B',
        'MALAYALAM_C',
        'MALAYALAM_D',
        'KANNADA',
        'KANNADA_A',
        'KANNADA_B',
        'KANNADA_C',
        'KANNADA_D',
        'BENGALI',
        'BENGALI_A',
        'BENGALI_B',
        'BENGALI_C',
        'BENGALI_D',
        'URDU',
        'URDU_A',
        'URDU_B',
        'URDU_C',
        'URDU_D',
        'ASSAMESSE',
        'ASSAMESSE_A',
        'ASSAMESSE_B',
        'ASSAMESSE_C',
        'ASSAMESSE_D',
        'CORRECT_ANSWER',
        'MIZO',
        'MIZO_A',
        'MIZO_B',
        'MIZO_C',
        'MIZO_D',
        'MEITEI',
        'MEITEI_A',
        'MEITEI_B',
        'MEITEI_C',
        'MEITEI_D',
        'ORIYA',
        'ORIYA_A',
        'ORIYA_B',
        'ORIYA_C',
        'ORIYA_D',
        'MARKS',
        'DIFFICULTY_LEVEL'
    ];

    constructor(
        private _toastr: ToastrService,
        private _batchExamService: BatchExamService,
        private _sectorService: SectorService,
        private _jobRoleService: JobRoleService,
        private _questionService: QuestionBankService,
        private _route: Router,
        private _loading: LoaderService,
        private _posService: JobRoleService
    ) { }

    ngOnInit(): void {
        this._batchExamService.getLocales().subscribe((response: any) => {
            this.locales = response.length > 0 ? response : response.data;
        });
        this._sectorService.getSector().subscribe((response: any) => {
            this.sector = response.length > 0 ? response : response.data;
        });
        this.getJobRoles();
    }

    getJobRoles() {
        this._jobRoleService.getJobRole().subscribe((response: any) => {
            this.jobRole = response.length > 0 ? response : response.data;
        });
    }

    onSelectSector(value) {
        const selectedJob = this.jobRole.filter((data: any) => data.sector && (data.sector.id === value));
        this.selectedJobRole = '';
        this.arrayJobRole = [];
        selectedJob.map((res) => {
            this.arrayJobRole.push({ id: res.id, name: res.name });
        });
        console.log(this.arrayJobRole);
    }
    onSelectJobRole(value) {
        this._posService.getPosById(value).subscribe({
            next: (response: any)=>{
                console.log(response);
                this.arrayNos = response.data;
            }
        });
    }

    reset() {
        this.myInputVariable.nativeElement.value = '';
    }

    uploadedFile(event) {
        if (event.target.files.length > 0) {
            this.fileUploaded = event.target.files[0];
            this.readExcel();
        }
    }

    readExcel() {
        const readFile = new FileReader();
        readFile.onload = (e) => {
            this.storeData = readFile.result;
            const data = new Uint8Array(this.storeData);
            const arr = new Array();
            for (let i = 0; i !== data.length; ++i) {
                arr[i] = String.fromCharCode(data[i]);
            }
            const bstr = arr.join('');
            const workbook = XLSX.read(bstr, { type: 'binary' });
            const firstSheetName = workbook.SheetNames[0];
            this.worksheet = workbook.Sheets[firstSheetName];
        };
        readFile.readAsArrayBuffer(this.fileUploaded);
        setTimeout(() => {
            this.showData = XLSX.utils.sheet_to_json(this.worksheet, { header: 1 });
            this.questionData = XLSX.utils.sheet_to_json(this.worksheet, {
                raw: false,
            });
            this.questionDataLength = this.questionData.length;
            if (this.questionDataLength === 0) {
                this._toastr.warning('File can\'t be empty', 'Error');
                this.onClickCancel();
            }
            if (this.questionData.length > 0) {
                const objKeys = Object.keys(this.questionData[0]);
                // To check if file format is proper
                objKeys.map((x) => {
                    console.log("222" ,x )
                    if (!this.headers.includes(x)) {
                        this._toastr.warning('File format is not proper');
                        this.onClickCancel();
                    }
                });
                this.questionData.forEach((element) => {
                    // To convert into lowercase
                    const newObj = Object.keys(element).reduce((c, k) => (c[k.toLowerCase()] = element[k], c), {});
                    // Get locales id and questions
                    console.log("newObj",newObj)
                    const questionLocales = [];
                    const newOptionLocales = [
                        { option_locales: [], isCorrect: false },
                        { option_locales: [], isCorrect: false },
                        { option_locales: [], isCorrect: false },
                        { option_locales: [], isCorrect: false }
                    ];
                    this.locales.map((lan) => {
                        if (lan.name && typeof lan.name === 'string') {
                            lan.name = lan.name.toLocaleLowerCase().trim();
                            newObj['correct_answer'] = newObj['correct_answer']?.trim();
                            newObj['difficulty_level'] = newObj['difficulty_level']?.trim();
                            newObj['marks'] = newObj['marks']?.trim();
                          }
                          
                       
                        if (newObj.hasOwnProperty(lan.name)) {
                            questionLocales.push({
                                locale_id: lan.id,
                                text: newObj[lan.name]
                            });
                            // for image options
                            newOptionLocales[0]['img_url'] = newObj['image_a']?.trim() ? newObj['image_a']?.trim() : '';
                            newOptionLocales[1]['img_url'] = newObj['image_b']?.trim() ? newObj['image_b']?.trim() : '';
                            newOptionLocales[2]['img_url'] = newObj['image_c']?.trim() ? newObj['image_c']?.trim() : '';
                            newOptionLocales[3]['img_url'] = newObj['image_d']?.trim() ? newObj['image_d']?.trim() : '';
                            // get options
                            console.log(!['a', 'b', 'c', 'd'].includes(newObj['correct_answer']));
                            
                         
                            if (!newObj['marks']) {
                                this._toastr.warning('Please enter weightage of all questions');
                                this.onClickCancel();
                                return false;
                            }
                            if (newObj['correct_answer'] === 'a') {
                                newOptionLocales[0]['isCorrect'] = true;
                            }
                            if (newObj['correct_answer'] === 'b') {
                                newOptionLocales[1]['isCorrect'] = true;
                            }
                            if (newObj['correct_answer'] === 'c') {
                                newOptionLocales[2]['isCorrect'] = true;
                            }
                            if (newObj['correct_answer'] === 'd') {
                                newOptionLocales[3]['isCorrect'] = true;
                            }
                            if (!['a', 'b', 'c', 'd'].includes(newObj['correct_answer'])) {
                                this._toastr.warning('Please enter answer from options !!');
                                this.onClickCancel();
                                return false;
                            }
                            // for option A
                            if (newObj[lan.name + '_a']) {
                                newOptionLocales[0]['option_locales'].push({
                                    'locale_id': lan.id, 'text': newObj[lan.name + '_a']
                                });
                            }
                            // for option B
                            if (newObj[lan.name + '_b']) {
                                newOptionLocales[1]['option_locales'].push({
                                    'locale_id': lan.id, 'text': newObj[lan.name + '_b']
                                });
                            }
                            // for option C
                            if (newObj[lan.name + '_c']) {
                                newOptionLocales[2]['option_locales'].push({
                                    'locale_id': lan.id, 'text': newObj[lan.name + '_c']
                                });
                            }
                            // for option D
                            if (newObj[lan.name + '_d']) {
                                newOptionLocales[3]['option_locales'].push({
                                    'locale_id': lan.id, 'text': newObj[lan.name + '_d']
                                });
                            }
                            if (newOptionLocales['isCorrect']) {
                                this._toastr.warning('Please enter correct answer from options');
                                this.onClickCancel();
                                return false;
                            }
                            if(!['easy','medium','hard'].includes(newObj['difficulty_level'])){
                                this._toastr.warning('Please enter correct difficulty level (easy,medium,hard)');
                                this.onClickCancel();
                                return false;
                            }
                        }
                    });
                 
                    this.postQuestionsData.push({
                        sector_id: this.selectedSector,
                        nos_id: this.selectedNos,
                        job_role_id: this.selectedJobRole,
                        weightage: Number(newObj['marks']),
                        img_url: newObj['image_question'],
                        video_url: newObj['video_question'],
                        question_locales: questionLocales,
                        options: newOptionLocales,
                        difficulty_level: newObj['difficulty_level'],
                    });
                });
                console.log('postQuestionsData', this.postQuestionsData);
            }
        }, 500);
    }

    uploadQuestions() {
        this._loading.setLoading(true, 'questions');
        this._questionService.uploadQuestions(this.postQuestionsData).subscribe((response) => {
            console.log("response" , response);
            if (response['status'] === 200) {
                this._loading.setLoading(false, 'questions');
                this._route.navigate(['question-bank']);
            }
        }, (error) => {
            console.log(error);
            this._loading.setLoading(false, 'questions');
            if (error.error.message.length > 0) {
                error.error.message.map((errors) => {
                    if (errors.message) {
                        this._toastr.warning(errors.message, errors.instancePath, { timeOut: 10000 });
                    }
                });
            }
            this.onClickCancel();
            return false;
        });
    }

    onClickCancel() {
        this.questionData = [];
        this.postQuestionsData = [];
        this.questionDataLength = 0;
        this.storeData = '';
        this.reset();
        return false;
    }
}
