/* eslint-disable @typescript-eslint/naming-convention */
export interface IQuestions {
    id: string;
    job_role: string;
    job_role_id: string;
    options: string;
    question_locales: string;
    weightage: number;
    updatedAt: string;
    createdAt: string;
}

export interface IExcelQuestions{
    sector_id: string;
    nos_id: string;
    job_role_id: string;
    weightage: number;
    img_url: string;
    video_url: any;
    question_locales: IQuestionLocales[];
    options: any[];
    difficulty_level: string;
}

export interface IQuestionLocales{
    locale_id: string;
    text: string;
}
