import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, switchMap, take, tap } from 'rxjs/operators';
import { JobRoleService } from '../job-role/job-role.service';
import { ApiResponse } from '../scheme/scheme.types';
import { IExcelQuestions, IQuestions } from './question-bank.type';

@Injectable({
  providedIn: 'root'
})
export class QuestionBankService {
  // Private
  private _questions: BehaviorSubject<IQuestions[] | null> = new BehaviorSubject(null);

  /**
   * Constructor
   */
  constructor(private _httpClient: HttpClient, private _jobRoleService: JobRoleService) { }

  // -----------------------------------------------------------------------------------------------------
  // @ Accessors
  // -----------------------------------------------------------------------------------------------------

  /**
   * Getter for JobRoles
   */
  get questions$(): Observable<IQuestions[]> {
    return this._questions.asObservable();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  getQuestions(jobRole, limit, page): Observable<IQuestions[]> {
    return this._httpClient.get<IQuestions[]>(`${environment.apiBaseUrl}questions?job_role_id=${jobRole}&isPaginate=true&page=${page}&limit=${limit}`).pipe(
      tap((response) => {
        this._questions.next(response['data']['rows']);
      })
    );
  }

  createQuestion(accessor: IQuestions): Observable<IQuestions> {
    return this._httpClient.post<IQuestions>(`${environment.apiBaseUrl}questions`, accessor);
  }

  getQuestion(id: string): Observable<IQuestions> {
    return this._httpClient.get<IQuestions>(
      `${environment.apiBaseUrl}questions/${id}`
    );
  }

  updateQuestion(id: string, assessor: IQuestions): Observable<IQuestions> {
    delete assessor.id;
    return this._httpClient.put<IQuestions>(`${environment.apiBaseUrl}questions/${id}`, assessor);
  }

  deleteQuestion(id: string): Observable<ApiResponse> {
    return this.questions$.pipe(
      take(1),
      switchMap(accessors => this._httpClient.delete<ApiResponse>(`${environment.apiBaseUrl}questions/${id}`).pipe(
        map((newRule) => {
          // Find the index of the deleted accessor
          const index = accessors.findIndex(item => item.id === id);

          // Delete the accessor
          accessors.splice(index, 1);

          // Update the accessors
          this._questions.next(accessors);

          return newRule;
        })
      ))
    );
  }

  uploadQuestions(questions) {
    return this._httpClient.post<IExcelQuestions>(`${environment.apiBaseUrl}questions`, {questions:questions});
  }
}
