/* eslint-disable @typescript-eslint/naming-convention */
export interface IExamRules {
    id: string;
    name: string;
    tab_switch: number;
    timeInMin: boolean;
    showResult: boolean;
    questionRandomise: boolean;
    optionRandomise: boolean;
    takeSnapshot: boolean;
    snapshot_interval: number;
    takeVideo: boolean;
    video_start_time: number;
    video_duration: number;
    isOffline: boolean;
    detailsRequired: boolean;
    geoRange: boolean;
    loginOnTime: boolean;
    password_type: string;
    instructions: string;
    allAttemptsRequired: boolean;
    showMarksPerQuestion: boolean;
}
