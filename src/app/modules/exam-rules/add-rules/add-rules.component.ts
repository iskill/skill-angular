import { messages } from './../../../mock-api/apps/chat/data';
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ExamRulesService } from '../exam-rules.service';
import { PasswordType } from '../../../shared/constant';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-rules',
  templateUrl: './add-rules.component.html',
  styleUrls: ['./add-rules.component.scss']
})
export class AddRulesComponent implements OnInit {

  isNew: boolean = true;
  showSnapshot: boolean = false;
  showTakeVideo: boolean = false;
  passwordType = PasswordType;
  selectedExamRulesForm: FormGroup;
  selectedExamRule: any;
  private id: string;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private _examRulesService: ExamRulesService,
    private _route: Router,
    private _toastr: ToastrService
  ) {
     this.selectedExamRulesForm = this._formBuilder.group({
      instructions: [''],
      name: ['', [Validators.required]],
      tab_switch: [''],
      showResult: [false],
      questionRandomise: [false],
      optionRandomise: [false],
      takeSnapshot: [false],
      snapshot_interval: [''],
      takeVideo: [false],
      video_start_time: [''],
      video_duration: [0],
      isOffline: [false],
      detailsRequired: [false],
      geoRange: [false],
      loginOnTime: [false],
      password_type: ['', [Validators.required]],
      allAttemptsRequired: [false],
      showMarksPerQuestion: [false],
    });
   }

  ngOnInit(): void {
    this.id = this._activatedRoute.snapshot.paramMap.get('id');
    if (this.id) {
      this._examRulesService.getExamDetails(this.id).subscribe((res) => {
        console.log("res);", res['data']);
        if (res) {
          this.isNew = false;
        }
        this.showSnapshot = res['data'].takeSnapshot;
        this.showTakeVideo = res['data'].takeVideo;

        this.selectedExamRulesForm.patchValue({
          instructions: res.instructions,
          name: res['data'].name,
          showResult: res['data'].showResult,
          tab_switch: res['data'].tab_switch,
          questionRandomise: res['data'].questionRandomise,
          optionRandomise: res['data'].optionRandomise,
          takeSnapshot: res['data'].takeSnapshot,
          snapshot_interval: res['data'].snapshot_interval,
          takeVideo: res['data'].takeVideo,
          video_start_time: res['data'].video_start_time,
          video_duration: res['data'].video_duration,
          isOffline: res['data'].isOffline,
          detailsRequired: res['data'].detailsRequired,
          geoRange: res['data'].geoRange,
          loginOnTime: res['data'].loginOnTime,
          password_type: res['data'].password_type,
          allAttemptsRequired: res['data'].allAttemptsRequired,
          showMarksPerQuestion: res['data'].showMarksPerQuestion,
        });
      });
    }
   
  }

  addUpdateExamRule() {
    if (this.selectedExamRulesForm.invalid) {
      this.selectedExamRulesForm.markAllAsTouched();
      return false;
    }
    this.selectedExamRulesForm.value.snapshot_interval = Number(this.selectedExamRulesForm.value.snapshot_interval);
    this.selectedExamRulesForm.value.video_start_time = Number(this.selectedExamRulesForm.value.video_start_time);
    console.log(this.selectedExamRulesForm.value);
    if (this.isNew) { // Add/Create Rule
      this._examRulesService.createExamRole(this.selectedExamRulesForm.value).subscribe((res) => {
        if (res['status'] === 200) {
          this._route.navigate(['/exam-rules']);
        }
      }, (error) => {
        this._toastr.warning(error.error.message);
      });
    } else { // update/edit Rule
      this._examRulesService.updateExamRole(this.id, this.selectedExamRulesForm.value).subscribe((res) => {
        if (res['status'] === 200) {
          this._route.navigate(['/exam-rules']);
        }
      });
    }
  }

  onCheckSnapshot() {
    this.showSnapshot = !this.showSnapshot;
    if (this.showSnapshot) {
      this.selectedExamRulesForm.get('snapshot_interval').setValidators([Validators.required]);
      this.selectedExamRulesForm.get('snapshot_interval').updateValueAndValidity();
    } else {
      this.selectedExamRulesForm.get('snapshot_interval').clearValidators();
      this.selectedExamRulesForm.get('snapshot_interval').updateValueAndValidity();
    }
  }

  onCheckTakeVideo() {
    this.showTakeVideo = !this.showTakeVideo;
    if (this.showSnapshot) {
      this.selectedExamRulesForm.get('video_start_time').setValidators([Validators.required, Validators.min(1)]);
      this.selectedExamRulesForm.get('video_start_time').updateValueAndValidity();
    } else {
      this.selectedExamRulesForm.get('video_start_time').clearValidators();
      this.selectedExamRulesForm.get('video_start_time').updateValueAndValidity();
    }
  }

}
