import { NgModule } from '@angular/core';

import { Route, RouterModule } from '@angular/router';
import { ExamRulesComponent } from './exam-rules.component';
import { AddRulesComponent } from './add-rules/add-rules.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatRippleModule } from '@angular/material/core';
import { MatSortModule } from '@angular/material/sort';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatRadioModule } from '@angular/material/radio';
import { SharedModule } from 'app/shared/shared.module';
import { MatDividerModule } from '@angular/material/divider';
import { CKEditorModule } from 'ckeditor4-angular';

const examRulesRoutes: Route[] = [
  {path: '', component: ExamRulesComponent},
  {path: 'add-exam-rules', component: AddRulesComponent},
  {path: 'update-exam-rules/:id', component: AddRulesComponent}
];

@NgModule({
  declarations: [
    ExamRulesComponent,
    AddRulesComponent
  ],
  imports: [
    RouterModule.forChild(examRulesRoutes),
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatRippleModule,
    MatSortModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatTableModule,
    MatTooltipModule,
    MatRadioModule,
    MatDividerModule,
    SharedModule,
    CKEditorModule
  ]
})
export class ExamRulesModule { }
