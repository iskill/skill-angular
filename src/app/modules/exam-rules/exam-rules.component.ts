import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    OnDestroy,
    OnInit,
    ViewEncapsulation,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { fuseAnimations } from '@fuse/animations';
import { ConfirmationDailogComponent } from 'app/shared/component/confirmation-dailog/confirmation-dailog.component';
import { Observable, of, Subject } from 'rxjs';
import { ExamRulesService } from './exam-rules.service';
import { IExamRules } from './exam-rules.type';

@Component({
    selector: 'app-exam-rules',
    templateUrl: './exam-rules.component.html',
    styleUrls: ['./exam-rules.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: fuseAnimations,
})
export class ExamRulesComponent implements OnInit, OnDestroy {
    examRules$: Observable<IExamRules[]>;

    flashMessage: 'success' | 'error' | null = null;
    isLoading: boolean = false;
    examRoleTableColumns: string[] = ['name', 'CreatedAt', 'actions'];
    examRulesCount: number = 0;
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _changeDetectorRef: ChangeDetectorRef,
        private _examRulesService: ExamRulesService,
        private dialog: MatDialog
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.examRules$ = this._examRulesService.examRules$;
        this.isLoading = true;
        this._examRulesService.getExamRules().subscribe((res) => {
            this.isLoading = false;
            // Update the counts
            this.examRulesCount = res.data.length;
            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
    }

    // Open delete confirmation dialog
    openDialog(id) {
        const dialogRef = this.dialog.open(ConfirmationDailogComponent,{
            data: {
                message: 'Are you sure want to delete?',
                buttonText: {
                    ok: 'Yes',
                    cancel: 'No'
                }
            }
        });
        dialogRef.afterClosed().subscribe((confirmed: boolean) => {
            if (confirmed) {
                this.deleteselectedExamRule(id);
                const a = document.createElement('a');
                a.click();
                a.remove();
            }
        });
    }
    // Open delete confirmation dialog end

    filterSearch(event) {
        const value = event.target.value.toLowerCase();
        this._examRulesService.examRules$.subscribe((res) => {
            this.examRules$ = of(res.filter(item =>
                item.name.toLocaleLowerCase().includes(value))
            );
        });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next({});
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Delete the selected product using the form mock-api
     */
    deleteselectedExamRule(rule): void {
        // Delete the rule on the server
        this._examRulesService.deleteExamRole(rule.id).subscribe((res) => {
            if (res.status === 200) {
                this.ngOnInit();
                this.showFlashMessage(null);
                this._changeDetectorRef.markForCheck();
            }
        });
    }

    /**
     * Show flash message
     */
    showFlashMessage(type: 'success' | 'error'): void {
        // Show the message
        this.flashMessage = type;

        // Mark for check
        this._changeDetectorRef.markForCheck();

        // Hide it after 1.5 seconds
        setTimeout(() => {
            this.flashMessage = null;
            // Mark for check
            this._changeDetectorRef.markForCheck();
        }, 1500);
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any {
        return item.id || index;
    }
}
