import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, pluck, switchMap, take, tap } from 'rxjs/operators';
import { ApiResponse } from '../scheme/scheme.types';
import { IExamRules } from './exam-rules.type';

@Injectable({
  providedIn: 'root',
})
export class ExamRulesService {
  // Private
  private _examRules: BehaviorSubject<IExamRules[] | null> = new BehaviorSubject(null);

  /**
   * Constructor
   */
  constructor(private _httpClient: HttpClient) { }

  // -----------------------------------------------------------------------------------------------------
  // @ Accessors
  // -----------------------------------------------------------------------------------------------------

  /**
   * Getter for JobRoles
   */
  get examRules$(): Observable<IExamRules[]> {
    return this._examRules.asObservable();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Get JobRole
   */
  getExamRules(): Observable<ApiResponse> {
    return this._httpClient.get<ApiResponse>(`${environment.apiBaseUrl}exam-rules`).pipe(
      tap((response) => {
        this._examRules.next(response.data);
      })
    );
  }

  /**
   * Create JobRole
   */
  createExamRole(rules: IExamRules): Observable<IExamRules> {
    return this._httpClient.post<IExamRules>(`${environment.apiBaseUrl}exam-rules`, rules);
  }

  getExamDetails(id: string): Observable<IExamRules> {
    return this._httpClient.get<IExamRules>(
      `${environment.apiBaseUrl}exam-rules/${id}`
    );
  }

  /**
   * Update JobRole
   *
   * @param id
   * @param product
   */
  updateExamRole(id: string, examRules: IExamRules): Observable<IExamRules> {
    return this._httpClient.put<IExamRules>(
      `${environment.apiBaseUrl}exam-rules/${id}`,
      examRules
    );
  }

  /**
   * Delete the JobRole
   *
   * @param id
   */
  deleteExamRole(id: string): Observable<ApiResponse> {
    return this.examRules$.pipe(
      take(1),
      switchMap(examRules => this._httpClient.delete<ApiResponse>(`${environment.apiBaseUrl}exam-rules/${id}`).pipe(
        map((newRule) => {
          // Find the index of the deleted examRules
          const index = examRules.findIndex(item => item.id === id);

          // Delete the examRule
          examRules.splice(index, 1);

          // Update the examRules
          this._examRules.next(examRules);

          return newRule;
        })
      ))
    );
  }
}
