import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import * as Highcharts from 'highcharts';
declare var require: any;
const More = require('highcharts/highcharts-more');
More(Highcharts);
import Histogram from 'highcharts/modules/histogram-bellcurve';
Histogram(Highcharts);
import highcharts3D from 'highcharts/highcharts-3d';
import { FormControl } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { ReplaySubject, Subject } from 'rxjs';
import { IJobRole, ISector } from '../job-role/job-role.types';
import { JobRoleService } from '../job-role/job-role.service';
import { DashboardService } from '../dashboard/dashboard.service';
highcharts3D(Highcharts);
const ExportData = require('highcharts/modules/export-data');
ExportData(Highcharts);
const Accessibility = require('highcharts/modules/accessibility');
Accessibility(Highcharts);
import { MapChart } from 'angular-highcharts';
import { HttpClient } from '@angular/common/http';
const IndiaMapData = require('../../../assets/india.geo.json');
import { Chart } from 'angular-highcharts';
import { SectorService } from '../sector/sector.service';
import { BatchExamService } from '../batch-exam/batch-exam.service';
import { IBatchDetails } from '../batch-exam/batch-exam.type';
import * as XLSX from 'xlsx';


require('highcharts/modules/map')(Highcharts);

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit,OnDestroy {
  collectionData: any;
  assessmentReport: any;
  nosReports: any;
  nosMarksReport: any;
  isSelectedSector='';
  isSelectedBatch=""
  isSelectedCandidate=''
  batchDetails: IBatchDetails[] = [];
  filterObj = {
    start_date:'',
    end_date:''
  };
  pageSize = 45;
    pageIndex = 0;

  jobRole: IJobRole[];
  sectorRole: ISector[];
  isLoading: boolean = false;
  chart: Chart;
  // Dropdown Search
  @ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;
  @ViewChild('searchFilter') searchFilter;
  public selectCtrl: FormControl = new FormControl();
  public selectSector: FormControl = new FormControl();
  public inputFilterCtrl: FormControl = new FormControl();
  public jobroles: ReplaySubject<IJobRole[]> = new ReplaySubject<IJobRole[]>(1);
  public sectors: ReplaySubject<ISector[]> = new ReplaySubject<ISector[]>(1);
  _onDestroy = new Subject<void>();
  private _unsubscribeAll: Subject<any> = new Subject<any>();
  sectorModel: any;
  nosQuestionCount:any 
  nosQuestionMarksCount: any;
  indiaMap: any;
  mapChart: MapChart;
  jsonData: any;
  stateWiseBatch: any;
  schemeWise: any;
  jobRoleWise: any;
	dataState3: any;
  start_date: any;
  end_date: any;
  sectorsIdd: any;
  ngShow: boolean =false;
  jobRoleName: any;
  id="c5b1df40-22ce-11ee-9d4c-13aeb88cc50d";
  collectionData1:any
  queryParams: any;

  filteredBatchname: any[] = [];
  batchname: any[]=[]
  batchid: any;
  batchCount: any;
  candidateName: any;
  candidateNameId: any;
  // Dropdown Search end
  constructor(private _jobRoleService: JobRoleService,private _sectorService:SectorService, private _dashboardService: DashboardService,private http: HttpClient,
    private _jonRolesService:JobRoleService,private _serviceBatch: BatchExamService,  private _bathcExamService: BatchExamService,) {
    
   }
  
  ngOnInit(): void {
    this.getNosQuestionCount()
    this.getNosQuestionMarksCount()
  
    this.getSchemeWiseData('','')
    this._sectorService.getSector().subscribe((res:any)=>{
      this.sectorRole=res
      this.sectors.next(res.length > 0 ? res : res.data);
      this.isLoading = false;

  })
  
  // scheme-wise report chart start
    this.collectionData = {
      chart: {
        type: 'column',
        options3d: {
          enabled: true,
          alpha: 0,
          beta: 20,
          depth: 50,
          viewDistance: 0
        }
      },
      title: {
        text: 'Schemewise Assessment Report',
    },
    tooltip: {
      pointFormat: 'Candidates :{point.count}<b>{point.y:1f}</b>'
    },
    plotOptions: {
      column: {
        colorByPoint: true,
        colors: ['#DE3163'],
        dataLabels: {
          enabled: true,
          format: '{point.y}',
          style: {
            textOutline: false
          }
        }
      }
    },
      xAxis: {
        opposite: false,
        categories: [],

        title: {
          text: 'Scheme Name',
          skew3d: true,
          style: {
            fontSize: '20px'
          }
        },
      },
      yAxis: {
        allowDecimals: false,
        min: 0,
        title: {
          text: 'Candidates Count',
          skew3d: true,
          style: {
            fontSize: '16px'
          }
        },
      },
      credits: {
        enabled: false
      },
      series: [{
        data: [],
        showInLegend: false,
        colorByPoint: true,
        events: {
          afterAnimate: function() {
            var points = this.points;
            var maxMark = Math.max.apply(Math, points.map(function(point) {
              return point.y;
            }));
    
            points.forEach(function(point) {
              if (point.y === maxMark) {
                point.update({
                  color: '#11ACF3' 
                });
              }
            });
          }
        }
      }],
    };
    // scheme-wise report chart End
    
      // jobRole-wise report chart start
    this.assessmentReport = {
      chart: {
        type: 'column',
        options3d: {
          enabled: true,
          alpha: 0,
          beta: 20,
          depth: 50,
          viewDistance: 0
        },
      
        scrollablePlotArea: {
          minWidth: 1800,
          scrollPositionX: 1
        }
       
      },
      scrollbar: {
        enabled: true
      },
   
    tooltip: {
      pointFormat: '{point.count}<b>{point.y:1f}</b>'
    },
    plotOptions: {
      column: {
        colorByPoint: true,
        colors: ['#DE3163'],
        dataLabels: {
          enabled: true,
          format: '{point.y}',
          style: {
            textOutline: false
          }
        },
     
      }
    },
      xAxis: {
        opposite: false,
        categories: [],
        labels: {
          rotation: -45 // Adjust the rotation angle as needed
        },
        title: {
          text: 'Job Roles',
          skew3d: true,
          style: {
            fontSize: '20px'
          }
        },
      },
      yAxis: {
        allowDecimals: false,
        min: 0,
        title: {
          text: 'Candidates Count',
          skew3d: true,
          style: {
            fontSize: '16px'
          }
        },
      },
      credits: {
        enabled: false
      },
      series: [{
        data: [],
        showInLegend: false,
        colorByPoint: true,
        events: {
          afterAnimate: function() {
            var points = this.points;
            var maxMark = Math.max.apply(Math, points.map(function(point) {
              return point.y;
            }));
    
            points.forEach(function(point) {
              if (point.y === maxMark) {
                point.update({
                  color: '#DE3163' 
                });
              }
            });
          }
        }
      }],
     
    }
 // jobRole-wise report chart End

    // nosQuestionsCount pie charts start
    this.nosReports = {
      chart: {
        type: 'pie',
        options3d: {
          enabled: true,
          alpha: 0,
          beta: 20,
          depth: 50,
          viewDistance: 0
        }
      },
      title: {
        text: 'Count of Question Nos Wise',
    },
    tooltip: {
      pointFormat: 'Questions Count:{point.count}{point.y:1f}</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
            enabled: true,
            format: '<b>{point.name}/No</b>: {point.y:1f}'
        }
    }
  
    },
      credits: {
        enabled: false
      },
      series: [{
        name:'Questions Count',
        data: [],
        showInLegend: false,
        colorByPoint: true,
      }],
    };
    // nosQuestionsCount pie charts end

    // nosQuestions Marks pie charts start
    this.nosMarksReport = {
      chart: {
        type: 'pie',
        options3d: {
          enabled: true,
          alpha: 0,
          beta: 20,
          depth: 50,
          viewDistance: 0
        }
      },
      title: {
        text: 'Marks of Question Nos Wise',
    },
    tooltip: {
      pointFormat: 'Questions Marks Count:{point.count}{point.y:1f}</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
            enabled: true,
            format: '<b>{point.name}/No</b>: {point.y:1f}'
        }
    }
  
    },
      credits: {
        enabled: false
      },
      series: [{
        name:'Questions Marks Count',
        data: [],
        showInLegend: false,
        colorByPoint: true,
      }],
    };
    // nosQuestions Marks pie charts End

    // Batch Result and marks Chart start
    this.collectionData1 = {
      chart: {
        type: 'column',
        options3d: {
          enabled: true,
          alpha: 0,
          beta: 20,
          depth: 50,
          viewDistance: 0
        }
      },
      title: {
        text: 'Total Theory Marks Obtained by Batch',
    },
    tooltip: {
      pointFormat: 'Marks :{point.count}<b>{point.y:1f}</b>'
    },
    plotOptions: {
      column: {
        colorByPoint: true,
        colors: ['#DE3163'],
        dataLabels: {
          enabled: true,
          format: '{point.y}',
          style: {
            textOutline: false
          }
        }
      }
    },
      xAxis: {
        opposite: false,
        categories: [],

        title: {
          text: 'Candidates Name',
          skew3d: true,
          style: {
            fontSize: '20px'
          }
        },
      },
      yAxis: {
        allowDecimals: false,
        min: 0,
        title: {
          text: 'Candidates Marks',
          skew3d: true,
          style: {
            fontSize: '16px'
          }
        },
      },
      credits: {
        enabled: false
      },
      series: [{
        data: [],
        showInLegend: false,
        colorByPoint: true,
        events: {
          afterAnimate: function() {
            var points = this.points;
            var maxMark = Math.max.apply(Math, points.map(function(point) {
              return point.y;
            }));
    
            points.forEach(function(point) {
              if (point.y === maxMark) {
                point.update({
                  color: '#11ACF3' 
                });
              }
            });
          }
        }
      }],
    };
    // Batch Result and marks Chart End

   this.getStateWiseData('','' ,'')
   
  //  this.getBatchDetails()
   this.getBatcount();
  }
  
  dateRangeChangeSchemes(dateRangeStart: HTMLInputElement, dateRangeEnd: HTMLInputElement) {
  
    if (dateRangeStart.value && dateRangeEnd.value) {
		this.filterObj['start_date'] =''
		this.filterObj['end_date']=''
        this.filterObj['start_date'] = (new Date(dateRangeStart.value).toLocaleDateString().split('/').reverse().join('-'));
        this.filterObj['end_date'] = new Date(dateRangeEnd.value).toLocaleDateString().split('/').reverse().join('-');
		function convertToISOString(dateString) {
			const date = new Date(dateString);
			const isoString = date.toISOString();
			return isoString;
		  }
		  this.filterObj['start_date']  = convertToISOString(this.filterObj['start_date'] );
		  this.filterObj['end_date']= convertToISOString(this.filterObj['end_date'] );

	
      this.getSchemeWiseData(this.filterObj['start_date'], this.filterObj['end_date'])
    }

	  
}

dateRangeChangeJobRole(dateRangeStart1: HTMLInputElement, dateRangeEnd1: HTMLInputElement) {

    if (dateRangeStart1.value && dateRangeEnd1.value) {
        this.filterObj['start_date'] = (new Date(dateRangeStart1.value).toLocaleDateString().split('/').reverse().join('-'));
        this.filterObj['end_date'] = new Date(dateRangeEnd1.value).toLocaleDateString().split('/').reverse().join('-');
		function convertToISOString(dateString) {
			const date = new Date(dateString);
			const isoString = date.toISOString();
			return isoString;
		  }
		  this.filterObj['start_date']  = convertToISOString(this.filterObj['start_date'] );
		  this.filterObj['end_date']= convertToISOString(this.filterObj['end_date'] );
      
	
        this.getJobRoleData(this.filterObj['start_date'], this.filterObj['end_date'])
    }

	  
}

onChangeSectorSelect(event){
 this.ngShow=true;
  this.sectorsIdd = event.value;
  this.getJobRoleData('','')
}

 onChangeSectorSelectCandidate(event){
  this.candidateNameId = event.value
 }
ngOnDestroy(): void {
  // Unsubscribe from all subscriptions
  this._unsubscribeAll.next({});
  this._unsubscribeAll.complete();
}


//  nos questions and marks Data Get function start 
getNosQuestionCount(){
  this._dashboardService.nosQuestionCount().subscribe((res)=>{
    this.nosQuestionCount = res['data']
    this.nosReports.series[0].data=[]
    for(let i=0; i<this.nosQuestionCount.length; i++){
      this.nosReports.series[0]['data'].push({
        name:'nos',
        y:+this.nosQuestionCount[i].QuestionCount
      })
    }
   
    Highcharts.chart('container2', this.nosReports);  
  })
}

getNosQuestionMarksCount(){
  this._dashboardService.nosQuestionMarksCount().subscribe((res)=>{
    this.nosQuestionMarksCount = res['data']
    this.nosMarksReport.series[0].data=[]
    for(let i=0; i<this.nosQuestionMarksCount.length; i++){
      this.nosMarksReport.series[0]['data'].push({
        name:'nos',
        y:+this.nosQuestionMarksCount[i].questionMarkCount
      })
   
    }
   
    Highcharts.chart('container3', this.nosMarksReport);  
  })
}
//  nos questions and marks Data Get function End 

dateRangeChangeState(dateRangeStart2: HTMLInputElement, dateRangeEnd2: HTMLInputElement) {
	this.filterObj['start_date'] =''
		this.filterObj['end_date']=''
    if (dateRangeStart2.value && dateRangeEnd2.value) {
        this.filterObj['start_date'] = (new Date(dateRangeStart2.value).toLocaleDateString().split('/').reverse().join('-'));
        this.filterObj['end_date'] = new Date(dateRangeEnd2.value).toLocaleDateString().split('/').reverse().join('-');
		function convertToISOString(dateString) {
			const date = new Date(dateString);
			const isoString = date.toISOString();
			return isoString;
		  }
		  this.filterObj['start_date']  = convertToISOString(this.filterObj['start_date'] );
		  this.filterObj['end_date']= convertToISOString(this.filterObj['end_date'] );
     
	
        this.getStateWiseData('' ,this.filterObj['start_date'], this.filterObj['end_date'] )
    }

	  
}

 getShortStateName(stateName) {
  return stateName.substring(0, 3).toUpperCase();
  
}

// state Wise map data start
onSelectState(id: string ,type: 'sector'|'jobRole'){
if(type =='sector'){
 this._jonRolesService.getJobRole(id).subscribe((res)=>{
  this.jobRoleName=res['data']
 })
}
if(type =='jobRole'){
  if (type === 'jobRole') {
    this.getStateWiseData(id, '', '');
    this.isLoading = false;
  }
  this.isLoading = false;
 }
}


getStateWiseData(jobrole_id,start_date ,end_date) {
	this._dashboardService.stateWiseData({ jobrole_id :jobrole_id,start_date:start_date,end_date:end_date})
	  .subscribe((res) => {
		this.stateWiseBatch = res['data'];
    console.log("stateWiseData" , this.stateWiseBatch)
		this.mapStateDataToFeatures(this.stateWiseBatch);
	  });
  }
  
  mapStateDataToFeatures(res) {
	this.dataState3 = IndiaMapData.features.map((feature) => {
	
	  const stateData = this.stateWiseBatch?.find((item) => item.state_name === feature.properties.name);
	  return {
		...feature,
		properties: {
		  ...feature.properties,
      state_short_name: this.getShortStateName(feature.properties.name),
		  batch_count: stateData ? stateData.batch_count : 0,
		  candidate_count: stateData ? stateData.candidate_count : 0,
		},
	  };
	});
	
    Highcharts.mapChart('indiaMapContainer', {
		chart: {
		  map: 'countries/in/custom/in-all-disputed'
		},
		title: {
		  text: ''
		},
		mapNavigation: {
		  enabled: true,
		  buttonOptions: {
			verticalAlign: 'bottom'
		  }
		},
		colorAxis: {
		  min: 0,
		  minColor: '#FFFFFF',
		  maxColor: '#005645'
		},
		credits: {
		  enabled: false
		},
		series: [{
		  type: 'map',
		  mapData: Highcharts.maps['countries/in/custom/in-all-disputed'],
		  data: this.dataState3, 
		  name: 'Batch',
		  states: {
			hover: {
			  color: '#a4edba'
			}
		  },
		  dataLabels: {
			enabled: true,
      style: {
        fontSize: '10px',
        fontWeight: 'bold',
        color: 'black'
      },
      format: '{point.properties.state_short_name} (BC: {point.properties.batch_count}, CC: {point.properties.candidate_count})'
    },
		  tooltip: {
			pointFormat: '<span>{point.properties.name}: {point.properties.batch_count}</span>'
		  },
		  colorByPoint: true
		}],
		legend: {
		  enabled: false 
		}
	  });
	  
  }

  stateWiseReport() {
    const modifiedArray = [];
    this.stateWiseBatch.map((data,index) => {
        if (data) {
            modifiedArray.push({
              'Serial Number': index + 1,
                'State Name': data.state_name,
                'Batch Count': data.batch_count,
                // 'Candidate Last Name': data.user.last_name, //, { wch: 18 }
                'Candidate Count': data.candidate_count,
             
               
            });
        }
    });
    const wb = XLSX.utils.book_new();
    const ws = XLSX.utils.json_to_sheet(modifiedArray );
    ws['!cols'] = [{ wch: 12 },{ wch: 12 },{ wch: 12 }, { wch: 18 }, { wch: 27 }];
    XLSX.utils.book_append_sheet(wb, ws, 'Statewise Assessment Report');
    XLSX.writeFile(wb, 'State-Wise-Assessment.xlsx');
  }
  // state Wise map data end


  // scheme-Wise data get Function And Download start
getSchemeWiseData(start_date,end_date){
  this.ngShow=true;
  this._dashboardService.schemeWiseData({start_date:start_date,end_date:end_date}).subscribe((res)=>{
    this.schemeWise=res['data']
    if(this.schemeWise.length>0){
      this.ngShow=false;
   
    }
    // console.log("schemeWise" ,this.schemeWise)
    this.collectionData.series[0].data=[]
    for(let i=0; i<this.schemeWise.length; i++){
      this.collectionData.series[0]['data'].push({
        // name:this.collectionData[i].name,
        y:+this.schemeWise[i].candidate_count

      })
      this.collectionData.xAxis.categories.push(this.schemeWise[i].name)
    }
const hasData = this.collectionData.series[0].data.length > 0;
if (hasData) {
  Highcharts.chart('container', this.collectionData); 
} else {
  document.getElementById('container').innerHTML = '<div style="text-align: center; padding: 20px;">No Data Available</div>';
}
     
  })
}

SchemeReport() {
  if(this.schemeWise.length>0){
    this.ngShow=false;
  }
  const modifiedArray = [];
  this.schemeWise.map((data,index) => {
      if (data) {
          modifiedArray.push({
            'Serial Number': index + 1,
              'Scheme Name': data.name,
              'Batch Count': data.batch_count,
              // 'Candidate Last Name': data.user.last_name, //, { wch: 18 }
              'Candidate Count': data.candidate_count,
           
             
          });
      }
  });
  const wb = XLSX.utils.book_new();
  const ws = XLSX.utils.json_to_sheet(modifiedArray);
  ws['!cols'] = [{ wch: 12 }, { wch: 12 }, { wch: 18 }, { wch: 12}];
  XLSX.utils.book_append_sheet(wb, ws, 'scheme  Wise Assessment');
  XLSX.writeFile(wb, 'scheme-Wise-Assessment.xlsx');
}
// scheme-Wise data get Function And Download End

// jobRole-Wise data get Function And Download start
getJobRoleData(start_date,end_date){
  let payload ={
    sector:this.sectorsIdd
  }
  this._dashboardService.jobRoleWiseData(payload,{start_date:start_date,end_date:end_date}).subscribe((res)=>{

    this.jobRoleWise=res['data']
    console.log("jobRoleWise" ,this.jobRoleWise)
    this.assessmentReport.series[0].data=[]
    for(let i=0; i<this.jobRoleWise.length; i++){
      this.assessmentReport.series[0]['data'].push({
        // name:this.collectionData[i].name,
        y:+this.jobRoleWise[i].candidate_count

      })
      this.assessmentReport.xAxis.categories.push(this.jobRoleWise[i].name)
    }
    const hasData =this.jobRoleWise.length > 0;
    if (hasData) {
      Highcharts.chart('container1', this.assessmentReport); 
    } else {
      document.getElementById('container1').innerHTML = '<div style="text-align: center; padding: 20px;">No Data Available</div>';
    }
   
    
  })
}

jobRoleReport() {
 
  const modifiedArray = [];
  this.jobRoleWise.map((data,index) => {
      if (data) {
          modifiedArray.push({
            'Serial Number': index + 1,
              'Job-Role Name': data.name,
              // 'Batch Count': data.batch_count,
              // 'Candidate Last Name': data.user.last_name, //, { wch: 18 }
              'Candidate Count': data.candidate_count,
           
             
          });
      }
  });
  const wb = XLSX.utils.book_new();
  const ws = XLSX.utils.json_to_sheet(modifiedArray );
  ws['!cols'] = [{ wch: 12 }, { wch: 18 }, { wch: 27 }];
  XLSX.utils.book_append_sheet(wb, ws, 'Jobrole Assessment Report');
  XLSX.writeFile(wb, 'JobRole-Wise-Assessment.xlsx');
}
// jobRole-Wise data get Function And Download End

// Candidate Results Batch wise and Download start
exportResult() {
  if(this.candidateName.length>0){
    this.ngShow=false;
  }
  const modifiedArray = [];
  this.candidateName.map((data,index) => {
      if (data) {
          modifiedArray.push({
            'Serial Number': index + 1,
              'Candidate Id': data.candidate_id,
              'Candidate First Name': data.user.first_name,
              // 'Candidate Last Name': data.user.last_name, //, { wch: 18 }
              'Candidate Email': data.user.email,
              'Theory Submitted': data.theorySubmitted ? 'Yes' : 'N/A',
              'Marks': data.marks ? String(data.marks) : 'N/A',
              // 'Total Marks': String(this.candidateName['total_marks'])
          });
      }
  });
  const wb = XLSX.utils.book_new();
  const ws = XLSX.utils.json_to_sheet(modifiedArray);
  ws['!cols'] = [{ wch: 12 },{ wch: 12 }, { wch: 18 }, { wch: 27 }, { wch: 15 }, { wch: 5 }, { wch: 9 }];
  XLSX.utils.book_append_sheet(wb, ws, 'candidate sheet');
  XLSX.writeFile(wb, 'candidates-result.xlsx');
}

async getBatcount() {
  try {
    const res: any = await this._bathcExamService.getBatchExams('', '').toPromise();
    this.batchCount = res.data.count;
    console.log("getBatchResults4", this.batchCount);
    // Call the getBatchExams() function here once you have the batchCount
    await this.getBatchExams();
  } catch (error) {
    console.error("Error in getBatcount:", error);
  }
}

async getBatchExams() {
  try {
    const res: any = await this._bathcExamService.getBatchExams(this.batchCount, '').toPromise();
    // this.searchFilter.nativeElement.value = null;
    this.isLoading = false;
    this.batchname = res.data.rows;
 // Create a new array to hold the filtered batch exams
 this.filteredBatchname = this.batchname.slice();
    
 this.inputFilterCtrl.valueChanges.subscribe(searchText => {
   const filterValue = searchText.toLowerCase();
   // Filter the batchname array and update the filteredBatchname array
   this.filteredBatchname = this.batchname.filter(batch => batch.name.toLowerCase().includes(filterValue));
    });
    console.log("filteredBatchname", this.filteredBatchname);
  } catch (error) {
    console.error("Error in getBatchExams:", error);
  }
}

 

getBatchDetails(candidateId: string) {
  this.ngShow = true;
  this.isLoading = true;
  this._serviceBatch.getBatchExamById(this.batchid).subscribe((response: any) => {
    this.isLoading = false;
    if (!this.queryParams) {
      let noOfQuestion = 0;
      JSON.parse(response.data.paper.no_of_question).map(d => (noOfQuestion += d.count));

      response.data.batch_locales = response.data.batch_locales.map(data => data.locale?.name);

      this.batchDetails = response.data;
      this.candidateName = this.batchDetails['candidates'];
      console.log("candidateName", this.candidateName);
      this.collectionData1.series[0].data = [];
      this.collectionData1.xAxis.categories = []; 
      for (let i = 0; i < this.batchDetails['candidates'].length; i++) {
     
          this.collectionData1.series[0]['data'].push({
            y: +this.batchDetails['candidates'][i].marks,
            count: this.batchDetails['candidates'][i].total_marks,
          });
          this.collectionData1.xAxis.categories.push(this.batchDetails['candidates'][i].user?.first_name);
         
        
      }
      // console.log("chart11", this.collectionData1.series[0]['data']);
      Highcharts.chart('container7', this.collectionData1);
      
    }
  });
}

onChangeSectorSelectBatch(id: string, type: 'batch' | 'candidate_name') {
  if (type === "batch") {
    this.batchid = id;
    this.getBatchDetails(id); // Pass the selected candidate ID here
    
  }
  if (type === "candidate_name") {
    this.isSelectedCandidate = id;
    // this.getBatchDetails(id); // Pass the selected candidate ID here
    this.collectionData1.series[0].data = [];
    this.collectionData1.xAxis.categories = []; 
      for (let i = 0; i < this.batchDetails['candidates'].length; i++) {
        if (this.batchDetails['candidates'][i].id === id) {
          this.collectionData1.series[0]['data'].push({
            y: +this.batchDetails['candidates'][i].marks,
            count: this.batchDetails['candidates'][i].total_marks,
          });
          this.collectionData1.xAxis.categories.push(this.batchDetails['candidates'][i].user?.first_name);
          break; // Stop the loop after finding the selected candidate
        }
      }
      console.log("chart11", this.collectionData1.series[0]['data']);
      Highcharts.chart('container7', this.collectionData1);
  }
}
// Candidate Results Batch wise and Download End

}
