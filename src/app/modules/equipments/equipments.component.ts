import { Observable, of, ReplaySubject, Subject } from 'rxjs';
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { ConfirmationDailogComponent } from 'app/shared/component/confirmation-dailog/confirmation-dailog.component';
import { JobRoleService } from '../job-role/job-role.service';
import { IJobRole } from '../job-role/job-role.types';
import { EquipmentsService } from './equipments.service';
import { IEquipments } from './equipments.type';
import { FormControl } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-equipments',
  templateUrl: './equipments.component.html',
  styleUrls: ['./equipments.component.scss']
})
export class EquipmentsComponent implements OnInit, OnDestroy {
  // Dropdown Search
  @ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;
  public jobroleCtrl: FormControl = new FormControl();
  public jobroleFilterCtrl: FormControl = new FormControl();
  public jobroles: ReplaySubject<IJobRole[]> = new ReplaySubject<IJobRole[]>(1);
  _onDestroy = new Subject<void>();
  // Dropdown Search end

  equipments$: Observable<IEquipments[]>;

  flashMessage: 'success' | 'error' | null = null;
  isLoading: boolean = false;
  jobRole: IJobRole[];
  jobRoleModel: any;
  questionTableColumns: string[] = [
    'JobRole',
    'Sector',
    'actions',
  ];
  questionCount: number = 0;
  searchTxt: any;
  private _unsubscribeAll: Subject<any> = new Subject<any>();

  /**
   * Constructor
   */
  constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private _equipmentsService: EquipmentsService,
    private _jobRoleService: JobRoleService,
    private dialog: MatDialog
  ) { }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.isLoading = true;
    this._jobRoleService.getJobRole().subscribe((res) => {
      this.jobRole = res;
      this.jobroles.next(res);
      this.isLoading = false;
    });
    this.equipments$ = this._equipmentsService.equipments$;
    this.jobroleFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterJobrole();
      });
  }

  // Open delete confirmation dialog
  openDialog(id) {
    const dialogRef = this.dialog.open(ConfirmationDailogComponent, {
      data: {
        message: 'Are you sure want to delete?',
        buttonText: {
          ok: 'Yes',
          cancel: 'No'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.deleteselectedEquipment(id);
        const a = document.createElement('a');
        a.click();
        a.remove();
      }
    });
  }
  // Open delete confirmation dialog end

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next({});
    this._unsubscribeAll.complete();
  }

  onChangeJobRoleSelect(event) {
    console.log(event.value);
    this._equipmentsService.getEquipments(event.value).subscribe((res) => {
      console.log(res);
      this.questionCount = res['data'].length;
    });
    this._changeDetectorRef.markForCheck();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Delete the selected product using the form mock-api
   */
  deleteselectedEquipment(questionId): void {
    // Delete the rule on the server
    this._equipmentsService
      .deleteEquipment(questionId)
      .subscribe((res) => {
        if (res.status === 200) {
          this.showFlashMessage(null);
          this._changeDetectorRef.markForCheck();
        }
      });
  }

  /**
   * Show flash message
   */
  showFlashMessage(type: 'success' | 'error'): void {
    // Show the message
    this.flashMessage = type;

    // Mark for check
    this._changeDetectorRef.markForCheck();

    // Hide it after 3 seconds
    setTimeout(() => {
      this.flashMessage = null;
      // Mark for check
      this._changeDetectorRef.markForCheck();
    }, 3000);
  }

  /**
   * Track by function for ngFor loops
   *
   * @param index
   * @param item
   */
  trackByFn(index: number, item: any): any {
    return item.id || index;
  }
  protected filterJobrole() {
    if (!this.jobroles) {
      return;
    }
    // get the search keyword
    let search = this.jobroleFilterCtrl.value;
    if (!search) {
      this.jobroles.next(this.jobRole.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.jobroles.next(
      this.jobRole.filter(bank => bank.name.toLowerCase().indexOf(search) > -1)
    );
  }

}
