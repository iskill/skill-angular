import { JobRoleService } from './../../job-role/job-role.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import * as XLSX from 'xlsx';
import { ToastrService } from 'ngx-toastr';
import { EquipmentsService } from '../equipments.service';
import { Router } from '@angular/router';
type AOA = any[][];

@Component({
  selector: 'app-add-equipments',
  templateUrl: './add-equipments.component.html',
  styleUrls: ['./add-equipments.component.scss']
})
export class AddEquipmentsComponent implements OnInit {

  @ViewChild('myInput') myInputVariable: ElementRef;
  equipmentsData = [];
  postEquipmentsData = [];
  equipmentsDataLength = 0;
  selectedJobRole: any;
  jobRole: any;
  fileUploaded: any;
  storeData: any;
  worksheet: any;
  showData: AOA = [[1, 2], [3, 4]];

  constructor(
    private _jobRoleService: JobRoleService,
    private _toastr: ToastrService,
    private _equipmentService: EquipmentsService,
    private _route: Router
  ) { }

  ngOnInit(): void {
    this.getJobRoles();
  }

  getJobRoles() {
    this._jobRoleService.getJobRole().subscribe((response) => {
      this.jobRole = response;
    });
  }

  uploadedFile(event) {
    if (event.target.files.length > 0) {
      this.fileUploaded = event.target.files[0];
      this.readExcel();
    }
  }

  readExcel() {
    const readFile = new FileReader();
    readFile.onload = (e) => {
      this.storeData = readFile.result;
      const data = new Uint8Array(this.storeData);
      const arr = new Array();
      for (let i = 0; i !== data.length; ++i) {
        arr[i] = String.fromCharCode(data[i]);
      }
      const bstr = arr.join('');
      const workbook = XLSX.read(bstr, { type: 'binary' });
      const firstSheetName = workbook.SheetNames[0];
      this.worksheet = workbook.Sheets[firstSheetName];
    };
    readFile.readAsArrayBuffer(this.fileUploaded);
    setTimeout(() => {
      this.showData = XLSX.utils.sheet_to_json(this.worksheet, { header: 1 });
      this.equipmentsData = XLSX.utils.sheet_to_json(this.worksheet, {
        raw: false,
      });
      this.equipmentsDataLength = this.equipmentsData.length;
      if (this.equipmentsData.length === 0) {
        this._toastr.warning('File can\'t be empty', 'Error');
        this.onClickCancel();
      }
      console.log(this.equipmentsData);
      this.equipmentsData.forEach((element) => {
        this.postEquipmentsData.push(element['Equipment_Name']);
      });
    }, 500);
  }
  uploadEquipments() {
    console.log(this.selectedJobRole, this.postEquipmentsData);
    this._equipmentService.uploadEquipment(this.selectedJobRole, this.postEquipmentsData).subscribe((response) => {
      if (response['status'] === 200) {
        this._route.navigate(['equipments']);
      }
    }, (error) => {
      console.log(error);
      if (error.error.message[0].message) {
        this._toastr.warning(error.error.message[0].message);
      }
      this.onClickCancel();
      return false;
    });
  }
  reset() {
    this.myInputVariable.nativeElement.value = '';
  }
  onClickCancel() {
    this.equipmentsData = [];
    this.equipmentsDataLength = 0;
    this.storeData = '';
    this.reset();
    return false;
  }

}
