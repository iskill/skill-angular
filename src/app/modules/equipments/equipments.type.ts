export interface IEquipments{
    id: string;
    name: string;
    jobRoleId: string;
};
