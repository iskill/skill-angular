import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, switchMap, take, tap } from 'rxjs/operators';
import { JobRoleService } from '../job-role/job-role.service';
import { IQuestions, IExcelQuestions } from '../question-bank/question-bank.type';
import { ApiResponse } from '../scheme/scheme.types';
import { IEquipments } from './equipments.type';

@Injectable({
  providedIn: 'root'
})
export class EquipmentsService {

  // Private
  private _equipments: BehaviorSubject<IEquipments[] | null> = new BehaviorSubject(null);

  /**
   * Constructor
   */
  constructor(private _httpClient: HttpClient, private _jobRoleService: JobRoleService) { }

  // -----------------------------------------------------------------------------------------------------
  // @ Accessors
  // -----------------------------------------------------------------------------------------------------

  /**
   * Getter for JobRoles
   */
  get equipments$(): Observable<IEquipments[]> {
    return this._equipments.asObservable();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  getQuestions(jobRole): Observable<IEquipments[]> {
    return this._httpClient.get<IEquipments[]>(`${environment.apiBaseUrl}questions?job_role_id=${jobRole}`).pipe(
      tap((response) => {
        this._equipments.next(response['data']);
      })
    );
  }

  createQuestion(accessor: IQuestions): Observable<IQuestions> {
    return this._httpClient.post<IQuestions>(`${environment.apiBaseUrl}questions`, accessor);
  }

  getEquipments(id: string): Observable<IQuestions> {
    return this._httpClient.get<IQuestions>(
      `${environment.apiBaseUrl}job-role/${id}/equipments`
    ).pipe(
      tap((response: any) => {
        this._equipments.next(response.data);
      })
    );
  }

  updateQuestion(id: string, assessor: IQuestions): Observable<IQuestions> {
    delete assessor.id;
    return this._httpClient.put<IQuestions>(`${environment.apiBaseUrl}questions/${id}`, assessor);
  }

  deleteEquipment(id: string): Observable<ApiResponse> {
    return this.equipments$.pipe(
      take(1),
      switchMap(accessors => this._httpClient.delete<ApiResponse>(`${environment.apiBaseUrl}equipments/${id}`).pipe(
        map((newRule) => {
          // Find the index of the deleted accessor
          const index = accessors.findIndex(item => item.id === id);

          // Delete the accessor
          accessors.splice(index, 1);

          // Update the accessors
          this._equipments.next(accessors);

          return newRule;
        })
      ))
    );
  }

  uploadEquipment(id, equipments) {
    return this._httpClient.post<IExcelQuestions>(`${environment.apiBaseUrl}job-role/${id}/equipments`, {equipments});
  }
}
