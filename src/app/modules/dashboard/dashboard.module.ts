import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { Route, RouterModule } from '@angular/router';
import { DashboardComponent } from 'app/modules/dashboard/dashboard.component';
import { ChartsModule } from 'ng2-charts';

const exampleRoutes: Route[] = [
    {
        path: '',
        component: DashboardComponent
    }
];

@NgModule({
    declarations: [DashboardComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(exampleRoutes),
        MatIconModule,
        MatMenuModule,
        MatTableModule,
        FormsModule,
        MatSelectModule,
        ChartsModule
    ],
})
export class DashboardModule { }
