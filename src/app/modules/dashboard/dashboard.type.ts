import { IBatchExam } from '../batch-exam/batch-exam.type';

export interface IDashboard{
    batchCount: number;
    batchList: IBatchExam[];
    candidateCount: number;
}
