import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ApiResponse } from '../scheme/scheme.types';
import { IDashboard } from './dashboard.type';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  private _dashboard: BehaviorSubject<IDashboard[] | null> = new BehaviorSubject(null);

  constructor(private _http: HttpClient) { }

  get dashboard$(): Observable<IDashboard[]> {
    return this._dashboard.asObservable();
  }

  getDashboard(): Observable<ApiResponse> {
    return this._http.get<ApiResponse>(`${environment.apiBaseUrl}dashboard`).pipe(
      tap(response => this._dashboard.next(response.data))
    );
  }
  getDashboardMonth(obj): Observable<ApiResponse> {
    return this._http.get<ApiResponse>(`${environment.apiBaseUrl}dashboard-month`, { params: obj }).pipe(
      tap(response => this._dashboard.next(response.data))
    );
  }
  getDashboardBatchTable(): Observable<ApiResponse>{
    return this._http.get<ApiResponse>(`${environment.apiBaseUrl}today/batches`).pipe(
      tap(response => this._dashboard.next(response.data))
    );

  }

  getCandidateAllotted(): Observable<ApiResponse> {
    return this._http.get<ApiResponse>(`${environment.apiBaseUrl}all-candidate`)
  }
  getAssessedCandidate(): Observable<ApiResponse> {
    return this._http.get<ApiResponse>(`${environment.apiBaseUrl}assessed/candidate`)
  }
  nosQuestionCount(): Observable<ApiResponse> {
    return this._http.get<ApiResponse>(`${environment.apiBaseUrl}nos/question-count`)
  }
  nosQuestionMarksCount(): Observable<ApiResponse> {
    return this._http.get<ApiResponse>(`${environment.apiBaseUrl}nos/question-mark-count`)
  }
  stateWiseData(obj?): Observable<ApiResponse> {
    return this._http.get<ApiResponse>(`${environment.apiBaseUrl}batch/stateWise` ,{params: obj})
  }

  schemeWiseData(scheme?): Observable<ApiResponse> {
    return this._http.get<ApiResponse>(`${environment.apiBaseUrl}all-candidate-batchwise` ,{params: scheme})
  }
  
  jobRoleWiseData(id: any, objJob?: any): Observable<any> {
    return this._http.post<ApiResponse>(`${environment.apiBaseUrl}job-roles/candidate`, id ,{params: objJob});
  }


}
