import { ChangeDetectorRef, Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { DashboardService } from './dashboard.service';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
    selector: 'dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class DashboardComponent implements OnInit {
    /**
     * Constructor
     */

    topBatchesDataSource: MatTableDataSource<any> = new MatTableDataSource();
    topBatchesTableColumns: string[] = ['batchName', 'date', 'status'];
    tableBatchesDataSource: MatTableDataSource<any> = new MatTableDataSource();
    tableBatchesColumns: string[] = ['batchName', 'count', 'theory_submit' , 'status'] ;

    dashboardData = {
        candidateCount: 0,
        batchCount: 0,
        batchList: [],
        updatedAt: ''
    };

    // Year wise chart data
    public yearWiseBarChartOptions: ChartOptions = {
        responsive: true,
    };
    public yearWiseBarChartLabels: Label[] = [];
    public yearWiseBarChartType: ChartType = 'bar';
    public yearWiseBarChartLegend = true;
    public yearWiseBarChartPlugins = [];
    public yearWiseBarChartData: ChartDataSets[] = [];

    // Month wise chart data
    filterWise: any = '2021-2022';
    public monthWiseBarChartOptions: any = {
        responsive: true,
    };
    public monthWiseBarChartLabels: Label[] = [];
    public monthWiseBarChartType: ChartType = 'bar';
    public monthWiseBarChartLegend = true;
    public monthWiseBarChartPlugins = [];
    public monthWiseBarChartData: ChartDataSets[] = [];

    // Asswssor wise pi-chart data
    public assessorWisePieChartOptions: ChartOptions = {
        responsive: true,
    };
    public assessorWisePieChartLabels: Label[] = [];
    public assessorWisePieChartType: ChartType = 'pie';
    public assessorWisePieChartLegend = true;
    public assessorWisePieChartPlugins = [];
    public assessorWisePieChartData: ChartDataSets[] = [];
    status :any
    dataTable: any;
    statuses: string[] = [];
    allotted: any;
    assessed: any;


    constructor(
        private _dashboardService: DashboardService,
        private check: ChangeDetectorRef,
    ) { }

    ngOnInit(): void {
        this.getDashboardData({ year: '2021-2022' });
        // this._dashboardService.getCandidateAllotted().subscribe(res=>{
        //     this.allotted=res?.data
        // })
        // this._dashboardService.getAssessedCandidate().subscribe(res=>{
        //     this.assessed=res?.data
        // })
    }
    getDashboardData(obj?) {
        this._dashboardService.getDashboard().subscribe((response: any) => {
            this.dashboardData.batchCount = response.data.number_of_batches;
            this.dashboardData.candidateCount = response.data.number_of_candidates;
            this.dashboardData.updatedAt = response.data.updatedAt;
            this.topBatchesDataSource = response.data.batch_list;
            this.yearWiseBarChartLabels = Object.keys(response.data.job_role_list);
            this.yearWiseBarChartData = this.filterObject(response.data, 'job_role_list', 'yearWiseBarChartLabels');
            this.assessorWisePieChartLabels = response.data.assessor.map(assessor => assessor.fullname);
            this.assessorWisePieChartData = [{ data: [], label: 'Assessors' }];
            this.assessorWisePieChartData[0].data = response.data.assessor.map(assessor => assessor.no_of_batches);
        });
        this._dashboardService.getDashboardMonth(obj).subscribe((response: any) => {
            this.monthWiseBarChartLabels = Object.keys(response.data.job_role_list);
            this.monthWiseBarChartData = this.filterObject(response.data, 'job_role_list', 'monthWiseBarChartLabels');
        });
        this._dashboardService.getDashboardBatchTable().subscribe((res:any)=>{
            this.tableBatchesDataSource=res.data;
            this.dataTable =this.tableBatchesDataSource
            for (let i = 0; i < this.dataTable.length; i++) {
                if (this.dataTable[i].theorySubmittedCount ==0) {
                  this.statuses.push("Not Submit");
                } else if (this.dataTable[i].candidateCount === this.dataTable[i].theorySubmittedCount && this.dataTable[i].candidateCount!=0) {
                  this.statuses.push("Completed");
                } else if (this.dataTable[i].candidateCount > this.dataTable[i].theorySubmittedCount) {
                  this.statuses.push("Partially Submitted");
                }
              }
            
        })
        this.check.markForCheck();
    }

    filterObject(objectArray, selectedObject, labels) {
        const groupRoles: any = {};
        for (const key in objectArray[selectedObject]) {
            if (key) {
                objectArray[selectedObject][key].map((ele) => {
                    if (groupRoles[ele.name]) {
                        groupRoles[ele.name][key] = ele.batch_count;
                    } else {
                        groupRoles[ele.name] = {
                            [key]: ele.batch_count
                        };
                    }
                });
            }
        };
        const filteredBarChartData = [];
        for (const key in groupRoles) {
            if (key) {
                const tempData = { data: [], label: key };
                this[labels].map((ele: any) => {
                    tempData.data.push(groupRoles[key][ele] || 0);
                });
                filteredBarChartData.push(tempData);
            }
        }
        return filteredBarChartData;
    }

    onSelectFilter(eventValue) {
        this._dashboardService.getDashboardMonth({ year: eventValue }).subscribe((response: any) => {
            this.monthWiseBarChartLabels = Object.keys(response.data.joRoleListForMonth);
            this.monthWiseBarChartData = this.filterObject(response.data, 'joRoleListForMonth', 'monthWiseBarChartLabels');
        });
    }
}
