import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { AuthService } from 'app/core/auth/auth.service';
import { ToastrService } from 'ngx-toastr';
import { LoaderService } from 'app/shared/loader.service';
import { HttpResponse } from 'aws-sdk';

@Injectable()
export class AuthInterceptor implements HttpInterceptor
{
    /**
     * Constructor
     */
    constructor(
        private _authService: AuthService,
        private _toastr: ToastrService,
        private _loading: LoaderService
    )
    {
    }

    /**
     * Intercept
     *
     * @param req
     * @param next
     */
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>
    {
        // Clone the request object
        this._loading.setLoading(true, req.url);
        let newReq = req.clone();

        // Request
        //
        // If the access token didn't expire, add the Authorization header.
        // We won't add the Authorization header if the access token expired.
        // This will force the server to return a "401 Unauthorized" response
        // for the protected API routes which our response interceptor will
        // catch and delete the access token from the local storage while logging
        // the user out from the app.
        if (this._authService.accessToken) {
            newReq = req.clone({
                headers: req.headers
                    .set('Authorization', 'Bearer ' + this._authService.accessToken)
                    .set('Content-Type', 'application/json')
            });
        } else {
            newReq = req.clone({
                headers: req.headers.set('Content-Type', 'application/x-www-form-urlencoded')
            });
        }

        // Response
        return next.handle(newReq).pipe(
            tap((res) => {
                if (['POST', 'PUT', 'DELETE'].includes(req.method)) {
                    if (res['body']) {
                        if (res['body'].message) {
                            this._toastr.success(res['body'].message);
                        }
                    }
                }
            }),
            catchError((error) => {
                // Catch "401 Unauthorized" responses
                this._loading.setLoading(false, req.url);
                if ( error instanceof HttpErrorResponse && error.status === 401 ) {
                    // Sign out
                    this._authService.signOut();

                    // Reload the app
                    location.reload();
                }

                if (error.status === 500) {
                    this._toastr.error('Please try again later!',error.error.message);
                }
                if (error.status === 400) {
                    if (typeof error.error.message === 'object') {
                        error.error.message.map((data) => {
                            this._toastr.error(data.message, data.instancePath, { timeOut: 10000 });
                        });
                    }
                    if (typeof error.error.message === 'string') {
                        this._toastr.error(error.error.message);
                    }
                }

                return throwError(error);
            }),
            map<HttpEvent<any>, any>((evt: HttpEvent<any>) => {
                if (evt instanceof HttpResponse) {
                    this._loading.setLoading(false, req.url);
                } else {
                    this._loading.setLoading(false, req.url);
                }
                return evt;
            })
        );
    }
}
