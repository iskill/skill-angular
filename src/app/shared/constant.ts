import { IPasswordType, IRoutesData } from './common.interface';

/* eslint-disable @typescript-eslint/naming-convention */
export const SECRET_ID = {
  CLIENT_ID: 'ISKILLCLIENT',
  CLIENT_SECRET: 'ISKILL123'
};

export const ADMIN = [
  'admin@gmail.com',
  'admin1@gmail.com',
  'admin2@gmail.com',
  'admin3 @gmail.com',
  'faizal@admin.com',
  'faisal@admin.com',
  'jayam@admin.com',
  'murtuza@admin.com',
  'burhan@gmail.com'
];

export const ROLES: IRoutesData[] = [
  { role_id: '0', svgIcon: 'heroicons_outline:chart-pie', title: 'Dashboard', routerLink: 'dashboard' },
  { role_id: '8', svgIcon: 'heroicons_outline:identification', title: 'Scheme', routerLink: 'scheme' },
  { role_id: '11', svgIcon: 'heroicons_outline:office-building', title: 'Sector', routerLink: 'sector' },
  { role_id: '9', svgIcon: 'heroicons_outline:bookmark-alt', title: 'Job Role', routerLink: 'job-role' },
  { role_id: '3', svgIcon: 'heroicons_outline:pencil-alt', title: 'Exam Rules', routerLink: 'exam-rules' },
  { role_id: '2', svgIcon: 'heroicons_outline:tag', title: 'Batch/Exam', routerLink: 'batch-exam' },
  { role_id: '14', svgIcon: 'heroicons_outline:document-text', title: 'Papers', routerLink: 'papers' },
  { role_id: '4', svgIcon: 'heroicons_outline:shield-check', title: 'Assessors', routerLink: 'assessor' },
  { role_id: '10', svgIcon: 'heroicons_outline:user-add', title: 'Sub Admin', routerLink: 'sub-admin' },
  { role_id: '1', svgIcon: 'heroicons_outline:question-mark-circle', title: 'Question Bank', routerLink: 'question-bank' },
  { role_id: '12', svgIcon: 'heroicons_outline:template', title: 'Equipments', routerLink: 'equipments' },
  { role_id: '13', svgIcon: 'heroicons_outline:book-open', title: 'Invoice', routerLink: 'invoice' },
  { role_id: '15', svgIcon: 'heroicons_outline:chart-bar', title: 'Reports', routerLink: 'reports' },
  // { role_id: '16', svgIcon: 'heroicons_outline:beaker', title: 'Practical', routerLink: 'practical' },
];


export const PasswordType: IPasswordType[] = [
  { value: '0', viewValue: 'Auto' },
  { value: '1', viewValue: 'Manual' },
];
