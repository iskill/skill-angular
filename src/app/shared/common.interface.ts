export interface IPasswordType{
    value: string;
    viewValue: string;
}
export interface ISignin{
    username: string;
    password: string;
    // eslint-disable-next-line @typescript-eslint/naming-convention
    client_id: string;
    // eslint-disable-next-line @typescript-eslint/naming-convention
    client_secret: string;
    // eslint-disable-next-line @typescript-eslint/naming-convention
    grant_type: string;
    scope: string;
}

export interface IRoutesData{
  // eslint-disable-next-line @typescript-eslint/naming-convention
  role_id: string;
  svgIcon: string;
  title: string;
  routerLink: string;
}
