import { Route } from '@angular/router';
import { AuthGuard } from 'app/core/auth/guards/auth.guard';
import { NoAuthGuard } from 'app/core/auth/guards/noAuth.guard';
import { LayoutComponent } from 'app/layout/layout.component';
import { InitialDataResolver } from 'app/app.resolvers';

// @formatter:off
// tslint:disable:max-line-length
export const appRoutes: Route[] = [
    // Redirect empty path to '/example'
    { path: '', pathMatch: 'full', redirectTo: 'dashboard' },

    // Redirect signed in user to the '/example'
    //
    // After the user signs in, the sign in page will redirect the user to the 'signed-in-redirect'
    // path. Below is another redirection for that path to redirect the user to the desired
    // location. This is a small convenience to keep all main routes together here on this file.
    { path: 'signed-in-redirect', pathMatch: 'full', redirectTo: 'dashboard' },

    // Auth routes for guests
    {
        path: '',
        canActivate: [NoAuthGuard],
        canActivateChild: [NoAuthGuard],
        component: LayoutComponent,
        data: {
            layout: 'empty',
        },
        children: [
            {
                path: 'forgot-password',
                loadChildren: () =>
                    import(
                        'app/modules/auth/forgot-password/forgot-password.module'
                    ).then(m => m.AuthForgotPasswordModule),
            },
            // {path: 'reset-password', loadChildren: () => import('app/modules/auth/reset-password/reset-password.module').then(m => m.AuthResetPasswordModule)},
            {
                path: 'sign-in',
                loadChildren: () =>
                    import('app/modules/auth/sign-in/sign-in.module').then(
                        m => m.AuthSignInModule
                    ),
            },
            // {path: 'sign-up', loadChildren: () => import('app/modules/auth/sign-up/sign-up.module').then(m => m.AuthSignUpModule)}
        ],
    },

    // Auth routes for authenticated users
    {
        path: '',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        component: LayoutComponent,
        data: {
            layout: 'empty',
        },
        children: [
            {
                path: 'sign-out',
                loadChildren: () =>
                    import('app/modules/auth/sign-out/sign-out.module').then(
                        m => m.AuthSignOutModule
                    ),
            },
            // {path: 'unlock-session', loadChildren: () => import('app/modules/auth/unlock-session/unlock-session.module').then(m => m.AuthUnlockSessionModule)}
        ],
    },

    // Landing routes
    // {
    //     path: '',
    //     component  : LayoutComponent,
    //     data: {
    //         layout: 'empty'
    //     },
    //     children   : [
    //         {path: 'home', loadChildren: () => import('app/modules/landing/home/home.module').then(m => m.LandingHomeModule)},
    //     ]
    // },

    // Admin routes
    {
        path: '',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        component: LayoutComponent,
        resolve: {
            initialData: InitialDataResolver,
        },
        children: [
            {
                path: 'assessor',
                loadChildren: () =>
                    import('app/modules/assessor/assessor.module').then(
                        m => m.AssessorModule
                    ),
            },
            {
                path: 'sub-admin',
                loadChildren: () =>
                    import('app/modules/sub-admin/sub-admin.module').then(
                        m => m.SubAdminModule
                    ),
            },
            {
                path: 'dashboard',
                loadChildren: () =>
                    import('app/modules/dashboard/dashboard.module').then(
                        m => m.DashboardModule
                    ),
            },
            {
                path: 'scheme',
                loadChildren: () =>
                    import('app/modules/scheme/scheme.module').then(
                        m => m.SchemeModule
                    ),
            },
            {
                path: 'sector',
                loadChildren: () =>
                    import('app/modules/sector/sector.module').then(
                        m => m.SectorModule
                    ),
            },
            {
                path: 'job-role',
                loadChildren: () =>
                    import('app/modules/job-role/job-role.module').then(
                        m => m.JobRoleModule
                    ),
            },
            {
                path: 'exam-rules',
                loadChildren: () =>
                    import('app/modules/exam-rules/exam-rules.module').then(
                        m => m.ExamRulesModule
                    ),
            },
            {
                path: 'batch-exam',
                loadChildren: () =>
                    import('app/modules/batch-exam/batch-exam.module').then(
                        m => m.BatchExamModule
                    ),
            },
            {
                path: 'question-bank',
                loadChildren: () =>
                    import(
                        'app/modules/question-bank/question-bank.module'
                    ).then(m => m.QuestionBankModule),
            },
            {
                path: 'equipments',
                loadChildren: () =>
                    import('app/modules/equipments/equipments.module').then(
                        m => m.EquipmentsModule
                    ),
            },
            {
                path: 'invoice',
                loadChildren: () =>
                    import('app/modules/invoice/invoice.module').then(
                        m => m.InvoiceModule
                    ),
            },
            {
                path: 'papers',
                loadChildren: () =>
                    import('app/modules/papers/papers.module').then(
                        m => m.PapersModule
                    ),
            },
            {
                path: 'practical',
                loadChildren: () =>
                    import('app/modules/practical-bank/practical-bank.module').then(
                        m => m.PracticalBankModule
                    ),
            },
            {
                path: 'reports',
                loadChildren: () =>
                    import('app/modules/reports/reports.module').then(m=>m.ReportsModule),
            }
        ],
    },
];
