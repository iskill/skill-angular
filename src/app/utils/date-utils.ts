
export function checkTimeDifference(startTime: string | Date | null): boolean {
    if (!startTime) {
      return false; 
    }
  
    const batchStart = new Date(startTime).getTime();
    const currentTime = new Date().getTime();         
    const timeDifference = currentTime - batchStart; 
    const hoursDifference = timeDifference / (1000 * 60 * 60);
  
    return hoursDifference >= 24; 
  }
  