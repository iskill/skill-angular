import { FuseNavigationItem } from '@fuse/components/navigation';

export const defaultNavigation: FuseNavigationItem[] = [
    {
        id: 'sub-admin',
        title: 'Sub Admin',
        type: 'basic',
        icon: 'heroicons_outline:user-add',
        link: '/sub-admin',
    },
    {
        id: 'dashboard',
        title: 'Dashboard',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/dashboard',
    },
    {
        id: 'scheme',
        title: 'Scheme',
        type: 'basic',
        icon: 'heroicons_outline:identification',
        link: '/scheme',
    },
    {
        id: 'sector',
        title: 'Sector',
        type: 'basic',
        icon: 'heroicons_outline:office-building',
        link: '/sector',
    },
    {
        id: 'jobRole',
        title: 'JobRole',
        type: 'basic',
        icon: 'heroicons_outline:bookmark-alt',
        link: '/job-role',
    },
    {
        id: 'examRules',
        title: 'ExamRules',
        type: 'basic',
        icon: 'heroicons_outline:pencil-alt',
        link: '/exam-rules',
    },
    {
        id: 'assessor',
        title: 'Assessors',
        type: 'basic',
        icon: 'heroicons_outline:shield-check',
        link: '/assessor',
    },
    {
        id: 'batch',
        title: 'Batch/Exam',
        type: 'basic',
        icon: 'heroicons_outline:tag',
        link: '/batch-exam',
    },
    {
        id: 'question-bank',
        title: 'Question Bank',
        type: 'basic',
        icon: 'heroicons_outline:tag',
        link: '/question-bank',
    },
    {
        id: 'equipments',
        title: 'Equipments',
        type: 'basic',
        icon: 'heroicons_outline:tag',
        link: '/equipments',
    },
    {
        id: 'practical',
        title: 'Practical',
        type: 'basic',
        icon: 'heroicons_outline:beaker',
        link: '/practical',
    },
    {
        id: 'invoice',
        title: 'Invoice',
        type: 'basic',
        icon: 'heroicons_outline:tag',
        link: '/invoice',
    },
    {
        id: 'reports',
        title: 'Reports',
        type: 'basic',
        icon: 'heroicons_outline:chart-bar-square',
        link: '/reports',
    },
];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
